package com.practice.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class SliderImagesDAOImpl implements SliderImagesDAO{


    @Autowired
    private EntityManager entityManager;
    /**
     * {@inheritDoc}
     */
    @Override
    public List<Object[]> getDefaultSliderImages(Integer statusId) {
        String sql = "select si.image, si.redirect_link, si.model_image, si.style, si.template_id , sit.content \n" +
                " from slider_images si left join slider_images_translation sit on sit.slider_image_id = si.id  \n" +
                " and sit.translation_id = 1 \n" +
                "  where si.id_country is NULL and si.id_state is NULL \n" +
                "  and si.user_type is NULL and si.device_type is NULL \n" +
                "  and si.status = ? order by si.sequence";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, statusId);
        return query.getResultList();
    }

    @Override
    public List<Object[]> getSliderImagesByFilter(Integer countryId, Integer stateId, String userType, Integer deviceId, Integer statusId, Integer languageId) {
      String sql = "select si.image, si.redirect_link, si.model_image, si.style, si.template_id, sit.content\n" +
              "from slider_images si\n" +
              "         left join slider_images_translation sit on sit.slider_image_id = si.id and sit.translation_id = ? \n" +
              "where (find_in_set(?, si.id_country) <> 0 or si.id_country is null)\n" +
              "  and (find_in_set(?, si.id_state) <> 0 or si.id_state is null)\n" +
              "  and (find_in_set(?, si.user_type) <> 0 or si.user_type is null)\n" +
              "  and (find_in_set(?, si.device_type) <> 0 or si.device_type is null)\n" +
              "  and si.status = ? \n" +
              "order by si.sequence;";
      Query query = entityManager.createNativeQuery(sql);
      query.setParameter(1, languageId);
      query.setParameter(2, countryId);
      query.setParameter(3, stateId);
      query.setParameter(4, userType);
      query.setParameter(5, deviceId);
      query.setParameter(6, statusId);
      return query.getResultList();
    }
}

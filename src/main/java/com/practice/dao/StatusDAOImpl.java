package com.practice.dao;

import com.practice.domain.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StatusDAOImpl implements StatusDAO {

    // Status Collection
    private static final Map<String, Status> STATUS = new HashMap<>();

    @Autowired
    private EntityManager entityManager;

    @Override
    public Status getStatusByName (final String statusName) {
        Status status;
        if (STATUS.containsKey(statusName)) {
            status =  STATUS.get(statusName);
        } else {
            synchronized (this) {
                String sql = "select * from status where name=:name and deleted=0 limit 1";
                Query query = entityManager.createNativeQuery(sql, Status.class);
                query.setParameter("name", statusName);


                try {
                    status = (Status) query.getSingleResult();
                } catch (NoResultException nre) {
                    status = null;
                }
                if (status != null) {
                    STATUS.put(statusName, status);
                }
            }
        }
        return status;
    }

    /**
     *
     * @return
     */
    @Override
    public List<Status> getProductStatus(){
        String sql = "select * from status where type='PRODUCT' and deleted='0'";
        Query query = entityManager.createNativeQuery(sql, Status.class);
        try {
            return query.getResultList();
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return null;
    }
}

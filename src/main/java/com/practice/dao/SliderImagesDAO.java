package com.practice.dao;

import java.util.List;

public interface SliderImagesDAO {

    /**
     * Get Default slider images that are visible for every one
     * @param statusId active status Id
     * @return list of object array or empty list.
     */
    List<Object[]> getDefaultSliderImages (Integer statusId);

    /**
     * Get slider images by given filters
     * @param countryId current user country id
     * @param stateId current state id
     * @param userType current user type
     * @param deviceId device type id
     * @param statusId id of status
     * @param languageId id of language
     * @return list of object array or empty list
     */
    List<Object[]> getSliderImagesByFilter (Integer countryId, Integer stateId,
                                            String userType, Integer deviceId, Integer statusId, Integer languageId);

}

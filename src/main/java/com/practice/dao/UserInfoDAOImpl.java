package com.practice.dao;

import com.practice.enums.StatusEnum;
import com.practice.log.EcommerceLogger;
import com.practice.service.SecurityService;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class UserInfoDAOImpl implements UserInfoDAO {

    private static final Logger LOGGER = Logger.getLogger(EcommerceLogger.class);

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private SecurityService securityService;


    @Override
    public boolean checkUserIsAdmin(String userName) {
        String sql = "select name from user_roles ur inner join roles r " +
                "on ur.role=r.id where r.module=2 and ur.user_name=?1";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, userName);
        if (query.getResultList().size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean updateUserStatus(String userName) {
        String sql = "update userinfo set is_pending_to_confirm=0, activation_token=NULL" +
                " where user_name='" + userName + "'";
        Query query = entityManager.createNativeQuery(sql);
        Integer result = (Integer) query.executeUpdate();
        if (result == 1) {
            return true;
        }
        return false;
    }


    @Override
    public boolean isDocCabVisible(final String userName) {
        String sql = "select exists(select * from userinfo_cabinet where user_name='" + userName + "')";
        Query query = entityManager.createNativeQuery(sql);
        BigInteger exist = (BigInteger) query.getSingleResult();
        if (exist != null && exist.intValue() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param userName
     * @param idBrand
     * @return
     */
    @Override
    public boolean isDocCabVisible(final String userName, final Integer idBrand) {
        String sql = "select exists(select * from userinfo_cabinet where user_name='" + userName + "' and id_brand = " +
                idBrand + ")";
        Query query = entityManager.createNativeQuery(sql);
        BigInteger exist = (BigInteger) query.getSingleResult();
        if (exist != null && exist.intValue() == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean checkAdminAccess(String userName) {

        if(isUserSuperAdmin(userName) || isUserAdmin(userName)){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    private boolean isUserAdmin(String userName ) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT exists(SELECT ra.action FROM user_roles ur INNER JOIN role_actions ra ON ur.role=ra.role ");
        sql.append(" INNER JOIN action a ON ra.action=a.id_action");
        sql.append(" WHERE ur.user_name=:userName and a.module = 2) ");
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter("userName",userName);
        Boolean isAdmin;
        try{
            isAdmin = ((BigInteger)  query.getSingleResult()).intValue()>0;
        }
        catch (NoResultException ee){
            isAdmin =Boolean.FALSE;
        }
        return isAdmin;
    }

    @Override
    public boolean isUserSuperAdmin(String userName) {
        StringBuilder sql = new StringBuilder();
        sql.append("select super_admin from userinfo where user_name=:userName");
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter("userName",userName);
        Boolean isSuperAdmin;
        try{
            isSuperAdmin = (Boolean) query.getSingleResult();
        }
        catch (NoResultException ee){
            isSuperAdmin =Boolean.FALSE;
        }
        return isSuperAdmin;
    }


    @Override
    public boolean isActionAllowed(final String action) {
        String sql = "select ra.action" +
                " from user_roles ur" +
                " inner join role_actions ra on ur.role=ra.role" +
                " inner join action ac on ra.action=ac.id_action" +
                " inner join roles r on r.id=ur.role" +
                " inner join module m on r.module=m.id_module" +
                " where ur.user_name='" + securityService.getCurrentUser() + "' and ac.name='" + action + "'";
        Query query = entityManager.createNativeQuery(sql);
        List<Object> id = query.getResultList();
        if (id != null && id.size() > 0) {
            return true;
        }
        return false;
    }

    public void deleteCreditCardInfo(final Integer id) {
        String sql = "UPDATE payment_details SET card_name = NULL , card_number = NULL, card_type = NULL , card_expiry_month = NULL, card_expiry_year = NULL WHERE id = '" + id + "'";
        Query query = entityManager.createNativeQuery(sql);
        query.executeUpdate();
    }

    @Override
    public Boolean isCreditCardExists(final Integer id) {
        String sql = "SELECT EXISTS ( SELECT * FROM payment_details WHERE id = '" + id + "' )";
        Query query = entityManager.createNativeQuery(sql);
        BigInteger isExists = (BigInteger) query.getSingleResult();
        if (isExists == BigInteger.ONE) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    @Override
    public String findCreditCard(Integer id) {
        String sql = "SELECT card_number FROM payment_details WHERE id = '" + id + "' ";
        Query query = entityManager.createNativeQuery(sql);
        return (String) query.getSingleResult();

    }

    @Override
    public boolean isQAUser(final String userName) {
        String sql = "select exists(" +
                "select * from user_roles ur" +
                " inner join roles r on ur.role=r.id" +
                " where ur.user_name=:userName and r.name='QA-USER-PROD')";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("userName", userName);
        BigInteger isExists = (BigInteger) query.getSingleResult();
        if (isExists == BigInteger.ONE) {
            return true;
        }
        return false;
    }


    /**
     * @param userName
     * @return user currency id
     */
    @Override
    public Integer getUserCurrencyId(final String userName) {
        String sql = "select currency_id from user_currency where user_name=:userName";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("userName", userName);
        try {
            Integer idCurrency = (Integer) query.getSingleResult();
            return idCurrency;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean checkIsVtbUser(String userName) {
        return false;
    }

    /**
     * @param idCountry
     * @return Country currency id
     */
    @Override
    public Integer getCountryCurrencyId(final Integer idCountry) {
        String sql = "select cu.id_currency from currency cu " +
                "left join country c on cu.id_currency = c.id_currency " +
                " where id_country =:idCountry";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("idCountry", idCountry);
        try {
            Integer idCurrency = (Integer) query.getSingleResult();
            return idCurrency;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * This method supposed to return user subsidiary
     * @param userName
     * @return user subsidiary
     */
    @Override
    public Integer getUserSubsidiary(String userName) {
        StringBuilder sql = new StringBuilder();
        sql.append("select subsidiary_id from userinfo_subsidiary where user_name=:userName limit 1");
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter("userName", userName);
        try {
            return (Integer) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public BigDecimal getUserTotalCredits(String userName) {
        return null;
    }

    public BigDecimal getNonWithdrawableCredit(String userName){
        StringBuilder queryString = new StringBuilder();
        queryString.append(" select sum(creditAmount)-sum(case when consumed != '' then consumed else 0 end) ");
        queryString.append(" from assigned_credit where username = :userName ");
        queryString.append(" and status = :status ");
        Query query = entityManager.createNativeQuery(queryString.toString());
        query.setParameter("userName", userName);
        query.setParameter("status", StatusEnum.APPROVED.getId());
        LOGGER.debug("Query that will be executed is : "+queryString.toString());
        try {
            BigDecimal vendorSideCredit = (BigDecimal) query.getSingleResult();
            return  vendorSideCredit != null ? vendorSideCredit : BigDecimal.ZERO;
        } catch (Exception e) {
            LOGGER.error(e);
            return BigDecimal.ZERO;
        }
    }

    /**
     * This function will return userName and userType of the current logged in user.
     *
     * <p>
     *   This method guarantees that {@code null} will never be returned in response. If any exception occurs or result
     *   is not found, this method will return {@code new Object[2]}
     * </p>
     *
     * @return A row with two columns (userName at index 0 and userType at index 1)
     */
    @Override
    public Object[] getUserInfo() {

        final String userName = securityService.getCurrentUser();

        StringBuilder sql = new StringBuilder();
        sql.append("select user_name, user_type from userinfo where user_name=:userName");
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter("userName", userName);
        try {
            // Should never be more than 1 user but since our data is very corrupted so just to be on safe side.
            List<Object[]> results = query.getResultList();
            if (results != null && results.size() > 0) {
                // return only first result
                return results.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Object[2];
    }

    @Override
    public List<Object> getAllowedModules(String userName) {
        return null;
    }


    /**
     * @param userName
     * @return logged in user is a sales rep or not
     */
    @Override
    public boolean isSalesRep(String userName) {
        LOGGER.debug("Entered in method to check user is sales rep or not. User is:"+userName);
        String sql = "select exists(select * from user_roles ur inner join roles r on ur.role=r.id where ur.user_name=:userName and r.id=48)";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("userName", userName);
        BigInteger isExists = (BigInteger) query.getSingleResult();
        if (isExists.compareTo(BigInteger.ONE) ==0) {
            LOGGER.debug("User is sales rep");
            return true;
        }
        LOGGER.debug("User is not a sales rep");
        return false;
    }

    @Override
    public Map<String, Boolean> getSaleRepActions(String userName) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isSalesManager(String userName) {
        LOGGER.debug("Entered in method to check user is sales manager or not. User is:"+userName);
        String sql = "select exists(select * from user_roles ur inner join roles r on ur.role=r.id where ur.user_name=:userName and r.id=52)";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("userName", userName);
        BigInteger isExists = (BigInteger) query.getSingleResult();
        if (isExists.compareTo(BigInteger.ONE) ==0) {
            LOGGER.debug("User is sales manager");
            return true;
        }
        LOGGER.debug("User is not a sales manager");
        return false;
    }

    @Override
    public BigDecimal getWithdrawableCredit(String userName) {
        return null;
    }

    @Override
    public boolean isUserEligibleForMobileApps () {
        Integer productCount = 0;
        Integer paymentMethods = 0;
        String sql = "select count(*) from product_price_per_country pp" +
                " inner join stock_product_size sps on pp.id_product=sps.id" +
                " where sps.status="+ StatusEnum.ACTIVE.getId();
        Query  query = entityManager.createNativeQuery(sql);
        productCount = ((BigInteger)query.getSingleResult()).intValue();
        sql = "select count(*) from payment_gateway_for_country_states pg" +
                " inner join payment_gateway_detail pgd on pg.gateway_id=pgd.gateway_id" +
                " where pgd.status="+StatusEnum.ACTIVE.getId();
        query = entityManager.createNativeQuery(sql);
        paymentMethods = ((BigInteger)query.getSingleResult()).intValue();
        if(paymentMethods == 0 || productCount == 0){
            return false;
        }else{
            return true;
        }
    }
}

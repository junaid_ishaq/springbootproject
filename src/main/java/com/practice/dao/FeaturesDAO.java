package com.practice.dao;

import com.practice.domain.Country;
import com.practice.domain.Module;
import com.practice.domain.Status;
import com.practice.domain.Subsidiary;

import java.util.List;

public interface FeaturesDAO {

    /**
     *
     * @param module
     * @param country
     * @param subsidiary
     * @param activeStatus
     * @return
     */
    List<Object[]> getAllowedFeatures (Module module, Country country, Subsidiary subsidiary,
                                       Status activeStatus);
}

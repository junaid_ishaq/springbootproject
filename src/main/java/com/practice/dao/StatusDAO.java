package com.practice.dao;

import com.practice.domain.Status;

import java.util.List;

public interface StatusDAO {

    /**
     * @param statusName
     *
     * @return
     */
    Status getStatusByName (String statusName);

    /**
     *
     * @return
     */
    List<Status> getProductStatus();

}

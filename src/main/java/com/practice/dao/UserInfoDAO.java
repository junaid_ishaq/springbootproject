package com.practice.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface UserInfoDAO {

    /**
     * @param userName
     * @return
     */
    boolean updateUserStatus(String userName);

    /**
     * @param userName
     * @return
     */
    boolean isDocCabVisible(String userName);

    /**
     * @param userName
     * @param idBrand
     * @return
     */
    boolean isDocCabVisible(String userName, Integer idBrand);

    /**
     * @param userName
     * @return
     */
    Boolean checkAdminAccess(String userName);

    /**
     * @param action
     * @return
     */
    boolean isActionAllowed(String action);

    /**
     * @param id
     */
    void deleteCreditCardInfo(Integer id);

    /**
     * @param userName
     * @return
     */
    public boolean checkUserIsAdmin(String userName);


    /**
     * @param id
     * @return
     */
    Boolean isCreditCardExists(Integer id);


    /**
     * @param id
     * @return
     */
    String findCreditCard(Integer id);


    /**
     * @param userName
     * @return
     */
    boolean isQAUser(String userName);

    /**
     * This method will return currency ID is there is specific currency associated with
     * any user, and if currency associated then all prices will be shown in that currency to user
     * It doesn't matter what currency is assigned to country
     *
     * @param userName
     * @return
     */
    Integer getUserCurrencyId(String userName);

    /**
     * @param userName
     * @return
     */
    boolean checkIsVtbUser(String userName);

    /**
     * @param userName
     * @return
     */
    Integer getUserSubsidiary(String userName);

    /**
     * This method will return user's total credits if credit are more than zero
     *
     * @param userName current logged in user name
     *
     * @return user's credits
     */
    BigDecimal getUserTotalCredits(String userName);

    /**
     * This function will return userName and userType of the current logged in user.
     *
     * @return A row with two columns (userName at index 0 and userType at index 1)
     */
    Object[] getUserInfo ();

    List<Object> getAllowedModules(String userName);

    /**
     * @param userName
     * @return logged in user is a sales rep or not
     */
    boolean isSalesRep(String userName);

    Map<String, Boolean> getSaleRepActions(String userName);

    /**
     * @param userName
     * @return logged in user is a sales manager
     */
    boolean isSalesManager(String userName);

    BigDecimal getWithdrawableCredit(String userName);

    BigDecimal getNonWithdrawableCredit(String userName);

    boolean isUserSuperAdmin(String userName);

    Integer getCountryCurrencyId(final Integer idCountry);

    boolean isUserEligibleForMobileApps ();
}

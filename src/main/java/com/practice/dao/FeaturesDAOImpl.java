package com.practice.dao;

import com.practice.domain.Country;
import com.practice.domain.Module;
import com.practice.domain.Status;
import com.practice.domain.Subsidiary;
import com.practice.service.SecurityService;
import com.practice.util.AppConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Objects;

@Service
public class FeaturesDAOImpl implements FeaturesDAO {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private SecurityService securityService;

    @Override
    public List<Object[]> getAllowedFeatures (Module module, Country country, Subsidiary subsidiary,
                                              Status activeStatus) {
        String sql;
        if (Objects.nonNull(module) && module.getIsSubsidiarySupported() && subsidiary != null) {
            sql =
                    "select a.name,(case when b.feature_name is not null or a.status!=1 then 0 else 1 end) as status, a.title as title  from" +
                            " features a left join (select distinct feature_name from restricted_features_by_subsidiary where " +
                            "subsidiary_id=" + subsidiary.getIdSubsidiary() + " and status=" + activeStatus.getId() +
                            " union select feature_name from restricted_features_by_users where " +
                            "user_name='" + securityService.getCurrentUser() + "') b on a.name=b.feature_name where module_id=" +
                            AppConstant.MODULE_ID;
        } else if (country != null && country.getStateLevelActions() && securityService.getStateId() != null) {
            sql = "select a.name,(case when b.feature_name is not null or a.status!=" + activeStatus.getId() +
                    " then 0 else 1 end) as status, a.title as title  from" +
                    " features a left join (select distinct feature_name from restricted_features_by_country_states " +
                    "where" +
                    " id_country_state=" + securityService.getStateId() + " and status=" + activeStatus.getId() +
                    " union select feature_name from " +
                    "restricted_features_by_users where user_name='" + securityService.getCurrentUser() +
                    "') b on a.name=b.feature_name where module_id=" + AppConstant.MODULE_ID;
        } else if (country != null) {
            sql = "select a.name,(case when b.feature_name is not null or a.status!=" + activeStatus.getId() +
                    " then 0 else 1 end) as status, a.title as title  from features a left join (select distinct feature_name from " +
                    "restricted_features_by_countries" +
                    " where country_id=" + country.getIdCountry() + " and status=" + activeStatus.getId() +
                    " union select feature_name from restricted_features_by_users where" +
                    " user_name='" + securityService.getCurrentUser() + "') b on a.name=b.feature_name where " +
                    "module_id=" + AppConstant.MODULE_ID;
        } else {
            sql =
                    "select a.name,(case when b.feature_name is not null or a.status!=1 then 0 else 1 end) as status, a.title as title from" +
                            " features a left join (select feature_name from restricted_features_by_users where " +
                            "user_name='" + securityService.getCurrentUser() + "') b on a.name=b.feature_name where module_id=" +
                            AppConstant.MODULE_ID;
        }
        Query query        = entityManager.createNativeQuery(sql);
        List<Object[]> featuresList = query.getResultList();
        return featuresList;
    }
}

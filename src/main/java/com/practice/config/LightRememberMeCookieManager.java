package com.practice.config;

import org.apache.shiro.web.mgt.CookieRememberMeManager;

/**
 *
 */
public class LightRememberMeCookieManager extends CookieRememberMeManager {

    public LightRememberMeCookieManager() {
        super();
        setSerializer(new SimplePrincipalSerializer());
    }
}


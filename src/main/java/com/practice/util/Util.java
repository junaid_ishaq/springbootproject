package com.practice.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.practice.util.AppConstant.NO_IMAGE_URL;

public class Util {

    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();


    public static String completeImageURL (final String inputImagePath, String baseImageURL){
        String imagePath = inputImagePath;
        if(StringUtils.isEmpty(imagePath)){
            imagePath = NO_IMAGE_URL;
        }
        if(imagePath.startsWith("http")){
            return imagePath;
        }
        if(!imagePath.startsWith("/")){
            imagePath = "/" + imagePath;
        }
        if(!imagePath.contains("images")){
            imagePath = "/images" + imagePath;
        }
        return baseImageURL + imagePath;
    }

    public static Map<String, String> jsonToMap(String jsonContent) {
        if(jsonContent == null)
            return null;
        TypeReference<HashMap<String, String>> typeRef
                = new TypeReference<HashMap<String, String>>() {
        };
        try{
            return JSON_MAPPER.readValue(jsonContent, typeRef);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    /**
     * This method verifies if given plain String sequence matches the given hash
     *
     * @param sequence plain String sequence
     * @param hash     generated hash for the sequence
     * @return true if matches otherwise false
     */
    public static boolean bCryptMatch(final String sequence, final String hash) {
        return (BCrypt.checkpw(sequence, hash));
    }
}

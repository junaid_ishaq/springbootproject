package com.practice.util;

public class FormConstants {

    public static final String SAVE_SHIPPING_ADDRESS_ONLY = "saveShippingAddressOnly";
    public final static String NEWSLETTER                 = "newsLetter";
    public final static String ONE_PAGE_MIX_FORM          = "one_page_all";
    public final static String FORM_CART_POPUP            = "cartPopup";
    public final static String FORM_PROFILE_POPUP         = "profilePopup";
    public final static String FORM_CONTACT_US            = "contact_us";
    public final static String ECOM_LISTING_FORM          = "ecomListing";
    public final static String PRODUCT_STATUS_LISTING_PAGE= "productStatusListingPage";
    public final static String CAREER_FORM                = "careerForm";
    public final static String GALLERY_FORM               = "galleryForm";
    public final static String MSDS_FORM                  = "msdsForm";
    public final static String VIDEOS_FORM                = "videosForm";
    public final static String PRIVACY_FORM               = "privacyPolicyForm";
    public final static String FAQ_FORM                   = "faqForm";
    public final static String PRESS_RELEASE_FORM         = "pressReleaseForm";
    public final static String MEDIA_MENTION_FORM         = "mediaMentionForm";
    public final static String DOCUMENT_CABINET_FORM      = "documentCabinetForm";
    public final static String DOCUMENT_LICENCE_FORM      = "document_licence_form";
    public final static String SALON_FORM                 = "salon_form";
    public final static String DISTRIBUTOR_FORM           = "distributor_form";
    public final static String PRODUCT_DETAIL_FORM        = "product_detail_form";
    public final static String PRODUCT_LISTING_FORM       = "product_listing_form";
    public final static String DASHBOARD_PAGE             = "dashboard_page";
    public final static String CREDIT_CARD_SAVING_FORM    = "credit_card_saving_form";
    public final static String HISTORY_DETAIL_PAGE        = "history_detail_page";
    public final static String EDIT_ADDRESS_PAGE          = "edit_address_page";
    public final static String CHANGE_PASWORD_PAGE        = "change_password_page";
    public final static String TAX_CERTIFICATE_PAGE       = "tax_certificate_page";
    public final static String ADMIN_ORDER_PAGE           = "admin_order_page";
    public final static String DISTRIBUTOR_ORDER_PAGE     = "distributor_order_page";
    public final static String APPROVED_ORDER_PAGE        = "approved_order_page";
    public final static String LOGIN_USER_PAGE            = "login_user_page";
    public final static String SIGNUP_USER_PAGE           = "signup_user_page";
    public final static String CHECKOUT_PAGE              = "checkout";
    public final static String SHOPPING_CART_PAGE         = "shoppingcart";
    public final static String BUSINESSRULESFORMNAME      = "businessRules";
    public final static String PROMOTIONSFORMNAME         = "promotions";
    public final static String FORM_BECOME_A_SALOON       = "becomeSaloon";
    public final static String CREDIT_CARD_PAGE           = "credit_card_page";
    public final static String CONTACT_INFORMATION_FORM   = "contact_information_page";
    public final static String LICENCE_INFORMATION_PAGE   = "licence_information_page";
    public final static String USER_BACK_ACCOUNT_INFORMATION_PAGE = "user_bank_account_information_page";
    public final static String WITHDRAW_INFORMATION_PAGE = "withdraw_information_page";
    public final static String WITHDRAWAL_REQUEST_PAGE       = "withdrawal_request_page";
    public final static String EMAIL_UPLOAD_FILE           = "email_uploadfile";
    public final static String FORGOT_PASSWORD_FORM = "forgotPassword";
    public final static String EMAIL_SIGNUP           = "email_signup";
    public final static String CHANGE_STATUS           = "email_changeStatus";
    public final static String EMAIL_REVIEW_COMMENT_REPLY           = "email_RepliedParentEcom";
    public static final String EMAIL_HEADING_REVIEW_REJECT = "emailHeadingReviewRejected";//Review/Comment Rejected
    public static final String EMAIL_HEADING_REVIEW_APPROVED = "emailHeadingReviewApproved";//Review/Comment Approved
    public static final String REASONS_QPB = "reasons";//Review/Comment Approved

    public static final String ORDER_STATUS = "email_orderStatusEcom" ; //order status mail
    public static final String YOUR_ORDER = "yourOrder" ; //Hello {customer}, your order
    public static final String IS = "is" ; //is
    public static final String TRACK_ORDER = "trackOrder" ; //For more details, you can also track your order
    public static final String HERE = "here" ; //here
    public static final String SUCCESSFULLY = "successfully";//successfully
    public static final String ORDER = "order";//Order
    public static final String BY_CUSTOMER = "byCustomer";//placed by the customer
    public static final String BY = "by";//by
    public static final String FOLLOWING_REASON = "followingReason";//because of the following reason
    public static final String ASSIGNED_ORDER_REJECTED = "assignedOrderRejected";//Assigned Order Rejected
    public static final String ORDER_STATUS_UPDATED = "orderStatusUpdated";//Order Status Update
    public static final String EMAIL_STATUSES = "email_staticStatuses";//statuses and other static values
    public static final String FOOTER_FORM = "footerForm";
    public static final String PICKUP_DEMO_FORM = "pickupDemoForm";      //Translation form for pickup demo





    public static final String PICKUP_ORDER_EMAIL = "pickupOrderEmail";
    public static final String PICKUP_ORDER_EMAIL_INSTOCK = "pickupOrderEmailInStock";
    public static final String PICKUP_LABLES = "pickupLables";
    public static final String ORDER_PLACEMENT_SUCCESS = "orderPlacementSuccessful";
    public static final String CHOOSE_SALON_BRIEF = "chooseSalonsBrief";
    public static final String CHOOSE_SALON_DETAILED = "chooseSalonsDetaled";
    public static final String PICKUP_BEFORE_EXPIRY = "pickupBeforeExpiry";
    public static final String MARK_ORDER = "markOrder";
    public static final String ORDER_REF_NO_TOCKEN = "{ORDER_REFERENCE_NUMBER}";
    public static final String ORDER_REF_NO_TOCKEN_SPLITTER = "@@";

}

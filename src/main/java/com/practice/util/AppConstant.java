package com.practice.util;

public class AppConstant {

    public static final String SUCCESS  = "SUCCESS";
    public static final String FAILURE = "FAILURE";
    public static final String SESSION_DATA           = "sessionData";
    public static final String DEFAULT_CARRIER_SERVICE = "";
    public static final Integer MODULE_ID = 1;

    public static String NO_IMAGE_URL = "/images/no_img.png";


    public static final String GET_SECTION_2        = "/getSection2";
    public static final String GET_STATES_LIST      = "/getStatesList";
    public static final String GET_HEADER_CONTENT = "/getHeaderContents";

    //Request Mapping
    public static final String SIGN_IN                                   = "/signIn";

    //default translation English US
    public static final Integer DEFAULT_TRANSLATION_ID = 1;


    public static final String STORE_NAME                           = "";
    public static final String GKHAIR_STORE_NAME                    = "GKhair";


    //GLOBAL_PROPERTIES_KEY_HARD_CODED
    public static final String GOOGLE_SITE_VERIFICATION_KEY_H = "zcp89ciwG1qekes20gd4O4ScxcTWqLFR3UcK16CwZDI";

    public static final String MINIMUM_CHARACTER_LIMIT_MESSAGE   = "minimum.character.limit.message";

    public static final String USERNAME_EMPTY  = "username.empty";;
    public static final String USERNAME_INVALID  = "username.invalid";
    public static final String USERNAME_ALPHANUMERIC_ONLY  =  "username.alphanumeric.only";
    public static final String MORE_THAN_ONE_ACCOUNT_FOUND  = "msg.more.than.one.account.found";


    public static final String LOGIN_SUCCESS                             = "msg.login.success";


}

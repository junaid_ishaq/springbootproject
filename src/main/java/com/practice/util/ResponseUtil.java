package com.practice.util;

import com.practice.dto.ResponseObject;

public class ResponseUtil {

    public static ResponseObject createResponseObject (String status,
                                                       String message,
                                                       Object object){
        ResponseObject responseObject = new ResponseObject();
        responseObject.setMessage(message);
        responseObject.setStatus(status);
        responseObject.setObject(object);
        return responseObject;
    }
}

package com.practice.log;

import org.apache.log4j.Logger;

public class EcommerceLogger {

    private static final Logger LOGGER = Logger.getLogger(EcommerceLogger.class.getName());


    public static void log(Exception ex, String methodName, String className) {
        String description = className + ":" + methodName + ":" + ex.getClass().getName() + ":" +
                ex.getMessage();

        LOGGER.info("EXCEPTION: " + description);
    }

    public static void log(Exception ex) {
        LOGGER.error("EXCEPTION: " + ex);
    }
}

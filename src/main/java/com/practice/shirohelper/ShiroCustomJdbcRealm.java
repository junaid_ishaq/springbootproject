package com.practice.shirohelper;

import org.apache.shiro.realm.jdbc.JdbcRealm;

public final class ShiroCustomJdbcRealm extends JdbcRealm {

    public static final String userAuthenticationQuery = "SELECT password from userinfo where user_name = ?";
    public static final String userRolesAuthenticationQuery = "SELECT r.name FROM userinfo ui join user_roles ur on ui.user_name=ur.user_name join" +
            " roles r on ur.role = r.id\n" +
            " where ui.user_name = ? ";
    public static final String rolePermissionAuthenticationQuery = "SELECT ra.action from userinfo ui inner join user_roles ur on ui.user_name =" +
            " ur.user_name inner join roles r on ur.role = r.id\n" +
            " inner join role_actions ra on r.id = ra.role where ui.user_name = ? ";

    public ShiroCustomJdbcRealm () {
        super();
        this.dataSource = MysqlDatabaseConnectionLoader.getDataSource();
    }
}

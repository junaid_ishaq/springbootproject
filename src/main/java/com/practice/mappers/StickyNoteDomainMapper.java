package com.practice.mappers;

import com.practice.domain.StickyNote;
import com.practice.dto.StickyNoteDTO;

public class StickyNoteDomainMapper {


    public static StickyNoteDTO createStickyMapperDTO(StickyNote stickyNote) {
        StickyNoteDTO stickyNoteDTO = new StickyNoteDTO();
        stickyNoteDTO.setDescription(stickyNote.getDescription());
        stickyNoteDTO.setEmojiName(stickyNote.getEmojiName());
        return stickyNoteDTO;
    }

}

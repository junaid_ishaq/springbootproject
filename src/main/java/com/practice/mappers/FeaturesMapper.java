package com.practice.mappers;

import com.practice.dto.FeaturesDTO;

import java.util.ArrayList;
import java.util.List;

public class FeaturesMapper {

    /**
     *
     * @param features
     * @return
     */
    public static List<FeaturesDTO> getFeatures(List<Object[]> features) {
        List<FeaturesDTO> featuresDTOs = new ArrayList<>();
        for (Object[] row : features) {
            FeaturesDTO dto = new FeaturesDTO();
            dto.setFeatureName(row[0]!=null?row[0].toString():"");
            dto.setStatus(Integer.parseInt(row[1].toString()));
            dto.setFeatureTitle(row[2]!=null?row[2].toString():"");
            featuresDTOs.add(dto);
        }
        return featuresDTOs;
    }
}

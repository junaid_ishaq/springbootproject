package com.practice.mappers;

import com.practice.domain.MenuInfo;
import com.practice.domain.TblFormField;
import com.practice.domain.Translation;
import com.practice.dto.HeaderElementsDTO;
import com.practice.dto.HomeElementDTO;
import com.practice.dto.LanguageDTO;
import com.practice.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.practice.util.Util.completeImageURL;

public class OnePageMapper {

    public static HomeElementDTO createHomeElementDTO(Object[] sliderImage, String baseImageURL){
        HomeElementDTO dto = new HomeElementDTO();
        dto.setImagePath(completeImageURL((String) sliderImage[0], baseImageURL));
        dto.setRedirectLink((String) sliderImage[1]);
        dto.setModelImage(completeImageURL((String) sliderImage[2], baseImageURL));
        dto.setStyle(Util.jsonToMap((String) sliderImage[3]));
        dto.setTemplateId((Integer) sliderImage[4]);
        dto.setTranslatedContent(Util.jsonToMap((String) sliderImage[5]));
        return dto;
    }


    public static HeaderElementsDTO createHeaderDTO (String logo, MenuInfo menuInfo, List<TblFormField> onePage,
                                                     List<TblFormField> profile, List<TblFormField> cart,
                                                     List<TblFormField> listing, List<Translation> translationList,
                                                     String baseImageURL) {
        HeaderElementsDTO dto = new HeaderElementsDTO();
        dto.setLogoPath(Util.completeImageURL(logo, baseImageURL));
        dto.setEmail(menuInfo != null ? menuInfo.getEmail() : "");
        dto.setPhone(menuInfo != null ? menuInfo.getPhone() : "");
        List<LanguageDTO> languageDTOList = new ArrayList<>();
        for (Translation lang : translationList) {
            LanguageDTO langDTO = new LanguageDTO();
            langDTO.setId(lang.getIdTranslation());
            langDTO.setName(lang.getName());
            langDTO.setIso(lang.getCode());
            languageDTOList.add(langDTO);
        }
        dto.setLanguagesList(languageDTOList);
        dto.setOnePage(FormMapper.createFormDTO(onePage));
        dto.setProfilePopup(FormMapper.createFormDTO(profile));
        dto.setCartPopup(FormMapper.createFormDTO(cart));
        dto.setListingForm(FormMapper.createFormDTO(listing));
        return dto;
    }
}

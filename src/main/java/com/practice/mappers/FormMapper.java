package com.practice.mappers;

import com.practice.domain.TblFormField;
import com.practice.dto.FormDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormMapper {

    private FormMapper () {

    }

    /**
     * @param formFields
     *
     * @return
     */
    public static FormDTO createFormDTO (List<TblFormField> formFields) {

        FormDTO formDTO = new FormDTO();
        if (formFields == null) {
            return formDTO;
        }
        Map<String, String> labelMap = new HashMap<>();
        for (TblFormField field : formFields) {
            labelMap.put(field.getFieldName(), field.getFieldValue());
        }
        formDTO.setLabelMap(labelMap);
        return formDTO;

    }
}

package com.practice.mappers;

import com.practice.domain.Brand;
import com.practice.dto.BrandsDTO;

public class BrandMapper {


    public static BrandsDTO createBrandsDTO(Brand brand){
        BrandsDTO dto = new BrandsDTO();
        dto.setBrandId(brand.getId());
        dto.setBrand_name(brand.getBrandName());
        dto.setSlugName(brand.getSlugName());
        dto.setMetaKeyWord(brand.getMetaKeywords());
        dto.setMetaDescription(brand.getMetaDescription());
        dto.setMetaTitle(brand.getMetaTitle());
        return dto;
    }
}

package com.practice.validation;

import javax.mail.internet.InternetAddress;
import java.util.regex.Pattern;

public class CustomerValidator {

    /**
     * @param str
     *
     * @return
     */
    public static boolean isNumeric5Digits (String str) {
        return str.matches("[0-9]{1,5}");  //decimal with max length of 5.
    }

    /**
     * @param str
     *
     * @return
     */
    public static boolean isNumeric4Digits (String str) // max 4 digits for credit card security code
    {
        return str.matches("[0-9]{1,4}");  //decimal with max length of 4.
    }

    /**
     * @param str
     *
     * @return
     */
    public static boolean isNumeric15Digits (String str) {
        return str.matches("[0-9]{1,15}");  //decimal with max length of 10.
    }

    /**
     * @param str
     *
     * @return
     */
    public static boolean isNumeric16Digits (String str) // max 16 digits for credit card number
    {
        return str.matches("[0-9]{1,16}");  //decimal with max length of 16.
    }

    /**
     * @param str
     *
     * @return
     */
    public static boolean isNumeric20Digits (String str) // max 16 digits for credit card number
    {
        return str.matches("^[0-9-+() ]*${1,20}");
    }

    /**
     * @param email
     *
     * @return
     */
    public static boolean isValidEmail (String email) {
        try {
            InternetAddress ia = new InternetAddress(email);
            ia.validate();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * @param prodName
     *
     * @return
     */
    public static boolean isValidProductName (String prodName) {
        Pattern numeric = Pattern.compile("^[a-zA-Z0-9 _-]*$");
        return numeric.matcher(prodName).matches();
    }

    /**
     * @param s
     *
     * @return
     */
    public static boolean isAlphaNumeric (String s) {
        String pattern = "^[a-zA-Z0-9-_.@() '&]*$";
        if (s.matches(pattern)) {
            return true;
        }
        return false;
    }

    /**
     * @param orderRefNo
     *
     * @return
     */
    public static boolean isValidOrderRefNo (String orderRefNo) {
        if (orderRefNo.length() < 4 || orderRefNo.length() > 18) {
            return false;
        }
        Pattern numeric = Pattern.compile("^[0-9-]*$");
        return numeric.matcher(orderRefNo).matches();
    }
}

package com.practice.validation;

import com.practice.dto.LoginUserDTO;
import com.practice.dto.ValidationDTO;
import com.practice.log.EcommerceLogger;
import com.practice.util.AppConstant;
import org.apache.log4j.Logger;

public class LoginUserDtoValidation {


    private static final Logger LOGGER = Logger.getLogger(EcommerceLogger.class);

    public static ValidationDTO validateLoginUser(LoginUserDTO dto) {
        ValidationDTO validationDTO = new ValidationDTO();
        if(dto.getUserName() == null || dto.getUserName().trim().isEmpty()){
            validationDTO.setErrorMessage(AppConstant.USERNAME_EMPTY);
            LOGGER.error("UserName cannot be Empty");
            validationDTO.setValidated(false);
        } else if( dto.getUserName() != null && dto.getUserName().length() > 45) {
            validationDTO.setErrorMessage(AppConstant.USERNAME_INVALID);
            LOGGER.error("Invalid UserName. Max Length is 45");
            validationDTO.setValidated(false);
        } else if(dto.getUserName() != null && !CustomerValidator.isAlphaNumeric(dto.getUserName())){
            validationDTO.setErrorMessage(AppConstant.USERNAME_ALPHANUMERIC_ONLY);
            LOGGER.error("Email can not be empty");
            validationDTO.setValidated(false);
        } else {
            validationDTO.setValidated(true);
        }
        return validationDTO;
    }
}

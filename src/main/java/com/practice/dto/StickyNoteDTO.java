package com.practice.dto;

import java.io.Serializable;

public class StickyNoteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String  description;
    private String emojiName;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmojiName() {
        return emojiName;
    }

    public void setEmojiName(String emojiName) {
        this.emojiName = emojiName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StickyNoteDTO that = (StickyNoteDTO) o;

        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return emojiName != null ? emojiName.equals(that.emojiName) : that.emojiName == null;
    }

    @Override
    public int hashCode() {
        int result = description != null ? description.hashCode() : 0;
        result = 31 * result + (emojiName != null ? emojiName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "StickyNoteDTO{" +
                "description='" + description + '\'' +
                ", emojiName='" + emojiName + '\'' +
                '}';
    }
}

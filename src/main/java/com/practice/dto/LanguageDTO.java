package com.practice.dto;

import java.io.Serializable;

public class LanguageDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private Integer id;

    private String iso;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }
}

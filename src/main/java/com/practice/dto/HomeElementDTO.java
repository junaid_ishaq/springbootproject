package com.practice.dto;

import java.io.Serializable;

public class HomeElementDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer imageId;
    private String  imagePath;
    private String  url;
    private Integer sequence;
    private String  redirectLink;
    private Object  style;
    private Object  translatedContent;
    private Integer templateId;
    private String  modelImage;

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getRedirectLink() {
        return redirectLink;
    }

    public void setRedirectLink(String redirectLink) {
        this.redirectLink = redirectLink;
    }

    public Object getStyle() {
        return style;
    }

    public void setStyle(Object style) {
        this.style = style;
    }

    public Object getTranslatedContent() {
        return translatedContent;
    }

    public void setTranslatedContent(Object translatedContent) {
        this.translatedContent = translatedContent;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getModelImage() {
        return modelImage;
    }

    public void setModelImage(String modelImage) {
        this.modelImage = modelImage;
    }

    @Override
    public String toString() {
        return "HomeElementDTO{" +
                "imageId=" + imageId +
                ", imagePath='" + imagePath + '\'' +
                ", url='" + url + '\'' +
                ", sequence=" + sequence +
                ", redirectLink='" + redirectLink + '\'' +
                ", style=" + style +
                ", translatedContent=" + translatedContent +
                ", templateId=" + templateId +
                ", modelImage='" + modelImage + '\'' +
                '}';
    }
}

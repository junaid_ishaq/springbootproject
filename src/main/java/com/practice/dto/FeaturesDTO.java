package com.practice.dto;

import java.io.Serializable;

public class FeaturesDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String  featureName;
    private Integer status;
    private String featureTitle;

    public String getFeatureName () {
        return featureName;
    }

    public void setFeatureName (final String featureName) {
        this.featureName = featureName;
    }

    public Integer getStatus () {
        return status;
    }

    public void setStatus (final Integer status) {
        this.status = status;
    }

    public String getFeatureTitle() {
        return featureTitle;
    }

    public void setFeatureTitle(String featureTitle) {
        this.featureTitle = featureTitle;
    }
}


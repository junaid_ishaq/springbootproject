package com.practice.dto;

import com.practice.domain.Action;

import java.io.Serializable;

public class ActionDTO implements Serializable {

    private static final long serialVersionUID = -3434934384093840342L;

    private Integer id;
    private String  actionName;
    private Action action;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}

package com.practice.dto;

import java.io.Serializable;
import java.util.Objects;

public class ValidationDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String errorMessage;
    private boolean validated;
    private Integer allowedQuantity;
    private Integer availed;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public Integer getAllowedQuantity() {
        return allowedQuantity;
    }

    public void setAllowedQuantity(Integer allowedQuantity) {
        this.allowedQuantity = allowedQuantity;
    }

    public Integer getAvailed() {
        return availed;
    }

    public void setAvailed(Integer availed) {
        this.availed = availed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidationDTO that = (ValidationDTO) o;
        return validated == that.validated &&
                errorMessage.equals(that.errorMessage) &&
                allowedQuantity.equals(that.allowedQuantity) &&
                availed.equals(that.availed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorMessage, validated, allowedQuantity, availed);
    }

    @Override
    public String toString() {
        return "ValidationDTO{" +
                "errorMessage='" + errorMessage + '\'' +
                ", validated=" + validated +
                ", allowedQuantity=" + allowedQuantity +
                ", availed=" + availed +
                '}';
    }
}

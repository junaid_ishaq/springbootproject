package com.practice.dto;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.Serializable;

public class PaymentResponseDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String transactionID;
    private String transactionCode;
    private String transactionDescription;
    private String approval_url;
    private String pareq;
    private String xid;
    private String returnUrl;
    private String md;

    public PaymentResponseDTO(){}

    public PaymentResponseDTO(String transactionID, String transactionCode){
        this.transactionID = transactionID;
        this.transactionCode = transactionCode;
    }

    public String getTransactionID () {
        return transactionID;
    }

    public void setTransactionID (String transactionID) {
        this.transactionID = transactionID;
    }

    public String getTransactionCode () {
        return transactionCode;
    }

    public void setTransactionCode (String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getTransactionDescription () {
        return transactionDescription;
    }

    public void setTransactionDescription (String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }

    public String getApproval_url () {
        return approval_url;
    }

    public void setApproval_url (String approval_url) {
        this.approval_url = approval_url;
    }

    public String getPareq () {
        return pareq;
    }

    public void setPareq (final String pareq) {
        this.pareq = pareq;
    }

    public String getXid () {
        return xid;
    }

    public void setXid (final String xid) {
        this.xid = xid;
    }

    public String getReturnUrl () {
        return returnUrl;
    }

    public void setReturnUrl (final String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getMd () {
        return md;
    }

    public void setMd (final String md) {
        this.md = md;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("transactionID", transactionID)
                .append("transactionCode", transactionCode)
                .append("transactionDescription", transactionDescription)
                .append("approval_url", approval_url)
                .append("pareq", pareq)
                .append("xid", xid)
                .append("returnUrl", returnUrl)
                .append("md", md)
                .toString();
    }
}

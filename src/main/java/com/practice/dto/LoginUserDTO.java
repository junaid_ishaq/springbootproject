package com.practice.dto;

import java.io.Serializable;

public class LoginUserDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userName;
    private String password;
    private boolean rememberMe;
    private boolean mobileRequest;
    private Integer languageId;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public boolean isMobileRequest() {
        return mobileRequest;
    }

    public void setMobileRequest(boolean mobileRequest) {
        this.mobileRequest = mobileRequest;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }
}

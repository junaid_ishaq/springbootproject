package com.practice.dto;

import java.io.Serializable;
import java.util.List;

public class HeaderElementsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String            logoPath;
    private String            email;
    private String            phone;
    private Integer           translationId;
    private FormDTO           onePage;
    private FormDTO           cartPopup;
    private FormDTO           profilePopup;
    private FormDTO           listingForm;
    private List<SectionDTO> sectionList;
    private List<LanguageDTO> languagesList;
    private List<BrandsDTO>   brandList;
    private List<StickyNoteDTO> stickyNotes;
    private String            characterLimitMessage;
    private String            whatsAppNumber;
    private List<MobileApplicationDTO> mobileApplicationLinks;


    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getTranslationId() {
        return translationId;
    }

    public void setTranslationId(Integer translationId) {
        this.translationId = translationId;
    }

    public FormDTO getOnePage() {
        return onePage;
    }

    public void setOnePage(FormDTO onePage) {
        this.onePage = onePage;
    }

    public FormDTO getCartPopup() {
        return cartPopup;
    }

    public void setCartPopup(FormDTO cartPopup) {
        this.cartPopup = cartPopup;
    }

    public FormDTO getProfilePopup() {
        return profilePopup;
    }

    public void setProfilePopup(FormDTO profilePopup) {
        this.profilePopup = profilePopup;
    }

    public FormDTO getListingForm() {
        return listingForm;
    }

    public void setListingForm(FormDTO listingForm) {
        this.listingForm = listingForm;
    }

    public List<SectionDTO> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<SectionDTO> sectionList) {
        this.sectionList = sectionList;
    }

    public List<LanguageDTO> getLanguagesList() {
        return languagesList;
    }

    public void setLanguagesList(List<LanguageDTO> languagesList) {
        this.languagesList = languagesList;
    }

    public List<BrandsDTO> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<BrandsDTO> brandList) {
        this.brandList = brandList;
    }

    public List<StickyNoteDTO> getStickyNotes() {
        return stickyNotes;
    }

    public void setStickyNotes(List<StickyNoteDTO> stickyNotes) {
        this.stickyNotes = stickyNotes;
    }

    public String getCharacterLimitMessage() {
        return characterLimitMessage;
    }

    public void setCharacterLimitMessage(String characterLimitMessage) {
        this.characterLimitMessage = characterLimitMessage;
    }

    public String getWhatsAppNumber() {
        return whatsAppNumber;
    }

    public void setWhatsAppNumber(String whatsAppNumber) {
        this.whatsAppNumber = whatsAppNumber;
    }

    public List<MobileApplicationDTO> getMobileApplicationLinks() {
        return mobileApplicationLinks;
    }

    public void setMobileApplicationLinks(List<MobileApplicationDTO> mobileApplicationLinks) {
        this.mobileApplicationLinks = mobileApplicationLinks;
    }


}

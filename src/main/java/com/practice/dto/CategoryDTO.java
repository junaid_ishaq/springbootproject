package com.practice.dto;

import java.io.Serializable;
import java.util.List;

public class CategoryDTO implements Serializable {


    private static final long serialVersionUID = 1L;

    private Integer              id;
    private String               category_name;
    private Integer              brandId;
    private Integer              productCount;
    private String               slugName;
    private List<SubCategoryDTO> subCategoryDTOList;
    private String               metaTitle;
    private String               metaDescription;
    private String               metaKeyWord;
    private String               image;
    private Integer              promotionCount;
}

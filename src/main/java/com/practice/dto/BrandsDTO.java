package com.practice.dto;

import java.util.List;

public class BrandsDTO {

    private static final long serialVersionUID = 1L;

    private Integer           brandId;
    private String            brand_name;
    private String            slugName;
    private String            metaTitle;
    private String            metaDescription;
    private String            metaKeyWord;
    private String            image;
    private List<CategoryDTO> categoryDTOList;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getSlugName() {
        return slugName;
    }

    public void setSlugName(String slugName) {
        this.slugName = slugName;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMetaKeyWord() {
        return metaKeyWord;
    }

    public void setMetaKeyWord(String metaKeyWord) {
        this.metaKeyWord = metaKeyWord;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<CategoryDTO> getCategoryDTOList() {
        return categoryDTOList;
    }

    public void setCategoryDTOList(List<CategoryDTO> categoryDTOList) {
        this.categoryDTOList = categoryDTOList;
    }
}

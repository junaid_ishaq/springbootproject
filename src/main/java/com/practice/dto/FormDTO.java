package com.practice.dto;

import java.io.Serializable;
import java.util.Map;

public class FormDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Map<String, String> labelMap;

    public Map<String, String> getLabelMap () {
        return labelMap;
    }

    public void setLabelMap (Map<String, String> labelMap) {
        this.labelMap = labelMap;
    }
}

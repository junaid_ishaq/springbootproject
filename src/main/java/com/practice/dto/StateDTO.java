package com.practice.dto;

import java.io.Serializable;
import java.util.Objects;

public class StateDTO implements Serializable {

    public static final long serialVersionUID = 3434343434343L;

    private String  stateName;
    private Integer stateId;


    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StateDTO stateDTO = (StateDTO) o;
        return Objects.equals(stateName, stateDTO.stateName) &&
                Objects.equals(stateId, stateDTO.stateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stateName, stateId);
    }

    @Override
    public String toString() {
        return "StateDTO{" +
                "stateName='" + stateName + '\'' +
                ", stateId=" + stateId +
                '}';
    }
}

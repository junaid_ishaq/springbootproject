package com.practice.service;


import com.practice.domain.SessionData;

public interface SecurityService {

    /**
     * @return
     */
    Integer getCurrentLanguageId();

    /**
     * @return
     */
    String getUserType();

    /**
     * @return
     */
    Integer getCartCountryId();

    /**
     * @return
     */
    SessionData getSessionData();

    /**
     * @return
     */
    String getCurrentUser();

    /**
     * @param sessionData
     */
    void setSessionData(SessionData sessionData);

    /**
     *
     * @return
     */
    String getStateId();
}

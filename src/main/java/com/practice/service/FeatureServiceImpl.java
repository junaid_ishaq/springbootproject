package com.practice.service;


import com.practice.dao.FeaturesDAO;
import com.practice.dao.StatusDAO;
import com.practice.domain.Country;
import com.practice.domain.Module;
import com.practice.domain.Status;
import com.practice.dto.FeaturesDTO;
import com.practice.enums.StatusEnum;
import com.practice.mappers.FeaturesMapper;
import com.practice.repository.ModuleRepository;
import com.practice.util.AppConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FeatureServiceImpl implements FeatureService {

    @Autowired
    private FeaturesDAO featuresDAO;

    @Autowired
    private ModuleRepository moduleRepository;

    @Autowired
    private CountryStateService countryStateService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private StatusDAO statusDAO;

    @Override
    public List<FeaturesDTO> getAllowedFeatures () {
        Module module  = moduleRepository.getOne(AppConstant.MODULE_ID);
        Country country = null;
        if (securityService.getCartCountryId() != null) {
            country = countryStateService.getCountryObject(securityService.getCartCountryId());
        }
        Status activeStatus = statusDAO.getStatusByName(StatusEnum.ACTIVE.getName());
        List<Object[]> featuresList = (module == null || country == null)?new ArrayList<>():featuresDAO.getAllowedFeatures(module, country, null, activeStatus);
        return FeaturesMapper.getFeatures(featuresList);
    }
}

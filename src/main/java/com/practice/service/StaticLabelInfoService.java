package com.practice.service;

public interface StaticLabelInfoService {

    /**
     *
     * @param name
     * @param idTranslation
     * @return
     */
    String getPropertyValue (String name, Integer idTranslation);

}

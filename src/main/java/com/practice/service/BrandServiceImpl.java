package com.practice.service;

import com.practice.domain.Brand;
import com.practice.dto.BrandsDTO;
import com.practice.enums.StatusEnum;
import com.practice.mappers.BrandMapper;
import com.practice.repository.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BrandServiceImpl implements BrandService {


    @Autowired
    private BrandRepository brandRepository;

    /**
     * @return
     */
    @Override
    @Cacheable(value = "brandCache", key = "#root.methodName")
    public List<BrandsDTO> getAllBrands () {
        List<Brand>     brands  = brandRepository.getAllActiveBrands(StatusEnum.ACTIVE.getId());
        List<BrandsDTO> dtoList = new ArrayList<>();
        dtoList.addAll(brands.stream().map(BrandMapper:: createBrandsDTO).collect(Collectors.toList()));
        return dtoList;
    }

    /**
     * @return
     */
    @Override
    @Cacheable (value = "brandCache", key = "#root.methodName")
    public List<Brand> getAllBrandObject () {
        return brandRepository.getAllActiveBrands(StatusEnum.ACTIVE.getId());
    }

    @Override
    @Cacheable (value = "brandCache", key = "#idBrand+#root.methodName")
    public Brand getBrandById(Integer idBrand) {
        return brandRepository.getBrandById(idBrand);
    }
}

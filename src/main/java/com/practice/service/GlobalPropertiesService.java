package com.practice.service;


public interface GlobalPropertiesService {

    /**
     *
     * @param key
     * @return
     */
    String getPropertyValue(String key);

    /** This method is used to get Google Site Verification Key
     *
     * @param key
     * @return
     */
    String getGoogleSiteVerificationKey(String key);
}

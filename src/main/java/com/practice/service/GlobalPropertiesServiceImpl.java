package com.practice.service;

import com.practice.repository.GlobalPropertiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static com.practice.util.AppConstant.*;

@Service
public class GlobalPropertiesServiceImpl implements GlobalPropertiesService {


    @Autowired
    private GlobalPropertiesRepository globalPropertiesRepository;

    @Override
    @Cacheable(value = "globalPropertiesCache",
            key = "#key+#root.methodName")
    public String getPropertyValue (final String key) {
        return globalPropertiesRepository.getPropertyValue(key);
    }

    @Override
    public String getGoogleSiteVerificationKey(String key) {
        String googleSiteVerificationKey = getPropertyValue(key);
        if (Objects.isNull(googleSiteVerificationKey) && (STORE_NAME.equals(GKHAIR_STORE_NAME))) {
            return GOOGLE_SITE_VERIFICATION_KEY_H;
        } else {
            return googleSiteVerificationKey;
        }
    }
}

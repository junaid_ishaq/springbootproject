package com.practice.service;


import com.practice.domain.UserInfo;
import com.practice.log.EcommerceLogger;
import com.practice.repository.UserInfoRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = Logger.getLogger(EcommerceLogger.class);

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public UserInfo getUserInfo (String userName) {
        UserInfo user = userInfoRepository.getUser(userName);
        return user;
    }

}

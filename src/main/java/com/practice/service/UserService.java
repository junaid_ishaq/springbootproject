package com.practice.service;

import com.practice.domain.UserInfo;

public interface UserService {

    /**
     *
     * @param userName
     * @return
     */
    UserInfo getUserInfo(String userName);


}

package com.practice.service;


import com.practice.dto.LoginUserDTO;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

public interface LoginRegistrationService {

    /**
     *
     * @param dto
     * @param request
     */
    void authenticate(LoginUserDTO dto, HttpServletRequest request);
}

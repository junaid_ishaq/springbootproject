package com.practice.service;


import com.practice.domain.StaticLabelInfo;
import com.practice.repository.StaticLabelInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.practice.util.AppConstant.DEFAULT_TRANSLATION_ID;

@Service
public class StaticLabelInfoServiceImpl implements StaticLabelInfoService {


    @Autowired
    private StaticLabelInfoRepository staticLabelInfoRepository;

    /**
     * @param name
     * @param idTranslation
     *
     * @return
     */
    @Override
    @Cacheable(value = "staticLabelCache", key = "#name+#idTranslation")
    public String getPropertyValue (final String name, final Integer idTranslation) {
        if (name != null && idTranslation != null) {
            List<StaticLabelInfo> labels = staticLabelInfoRepository.getStaticLabel(name, idTranslation);
            if (labels != null && !labels.isEmpty()) {
                return labels.get(0).getLabelText();
            } else {
                // if field is not in db retrun the name of the field we are looking instead of null
                labels = staticLabelInfoRepository.getStaticLabel(name, DEFAULT_TRANSLATION_ID);
                if (labels != null && !labels.isEmpty()) {
                    return labels.get(0).getLabelText();
                } else {
                    return name;
                }
            }
        }
        return name;
    }
}

package com.practice.service;

import com.practice.domain.TblFormField;
import com.practice.dto.FormDTO;
import com.practice.mappers.FormMapper;
import com.practice.repository.FormFieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.practice.util.AppConstant.DEFAULT_TRANSLATION_ID;

@Service
public class FormServiceImpl implements FormService {


    @Autowired
    private FormFieldRepository formFieldRepository;


    @Override
    @Cacheable(value = "formCache", key = "#formName+#idTranslation")
    public List<TblFormField> getFormFieldByFormAndTranslation (final String formName, final Integer idTranslation) {
        List<TblFormField> fieldList = formFieldRepository.findFormFieldsByFormName(formName, idTranslation);
        if (fieldList == null || fieldList.isEmpty()) {
            fieldList = formFieldRepository.findFormFieldsByFormName(formName, DEFAULT_TRANSLATION_ID);
        }
        return fieldList;
    }

    @Override
    public FormDTO getFormDTOByFormAndTranslation (final String formName, final Integer idTranslation) {
        List<TblFormField> fieldList =  getFormFieldByFormAndTranslation(formName, idTranslation);
        return FormMapper.createFormDTO(fieldList);
    }


}

package com.practice.service;

import com.practice.domain.Country;
import com.practice.domain.CountryState;
import com.practice.dto.StateDTO;
import com.practice.repository.CountryRepository;
import com.practice.repository.CountryStatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CountryStateServiceImpl implements CountryStateService {

    @Autowired
    private CountryStatesRepository countryStatesRepository;


    @Autowired
    private CountryRepository countryRepository;

    @Override
    public List<StateDTO> getStatesDTOListByCountry(Integer countryId) {
       List<CountryState> countryStatesList =
               countryStatesRepository.getStatesByCountryId(countryId);
               List<StateDTO> stateDTOList = new ArrayList<>();
        countryStatesList.forEach(countryStates -> {
            StateDTO stateDTO = new StateDTO();
            stateDTO.setStateName(countryStates.getName());
            stateDTO.setStateId(countryStates.getIdCountryState());
            stateDTOList.add(stateDTO);
        });
        return stateDTOList;
    }

    @Override
    public Country getCountryObject(Integer cartCountryId) {
        return countryRepository.getCountryByCountryId(cartCountryId);    }
}

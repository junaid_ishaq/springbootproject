package com.practice.service;


import com.practice.dto.FeaturesDTO;

import java.util.List;

public interface FeatureService {
    /**
     * @return
     */
    List<FeaturesDTO> getAllowedFeatures ();
}

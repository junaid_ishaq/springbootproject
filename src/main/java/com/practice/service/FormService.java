package com.practice.service;

import com.practice.domain.TblFormField;
import com.practice.dto.FormDTO;

import java.util.List;

public interface FormService {

    /**
     * @param formName
     * @param idTranslation
     *
     * @return
     */
    List<TblFormField> getFormFieldByFormAndTranslation (String formName, Integer idTranslation);

    /**
     * @param formName
     * @param idTranslation
     *
     * @return
     */
    FormDTO getFormDTOByFormAndTranslation (String formName, Integer idTranslation);
}

package com.practice.service;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.practice.dao.SliderImagesDAO;
import com.practice.dao.StatusDAO;
import com.practice.domain.*;
import com.practice.dto.*;
import com.practice.enums.StatusEnum;
import com.practice.enums.UserTypeEnum;
import com.practice.mappers.OnePageMapper;
import com.practice.mappers.StickyNoteDomainMapper;
import com.practice.repository.*;
import com.practice.util.FormConstants;
import org.hibernate.metadata.ClassMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.xml.soap.SOAPElement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static com.practice.util.AppConstant.DEFAULT_TRANSLATION_ID;
import static com.practice.util.AppConstant.MINIMUM_CHARACTER_LIMIT_MESSAGE;

@Service
//@Transaction Study In detail
public class OnePageServiceImpl implements OnePageService{

    //Logger study in detail

    @Autowired(required = true)
    private SliderImagesDAO sliderImagesDAO;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private StickyNoteRepository stickyNoteRepository;

    @Autowired
    private OnePageSectionsRepository onePageSectionsRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private MenuInfoRepository menuInfoRepository;

    @Autowired
    private MenuElementInfoRepository menuElementsInfoRepository;

    @Autowired
    private TranslationRepository translationRepository;

    @Autowired
    private FormService formService;

    @Autowired
    private GlobalPropertiesService globalPropertiesService;

    @Autowired
    private BrandService brandService;

    @Autowired
    private StaticLabelInfoService staticLabelInfoService;

    @Autowired
    private StatusDAO statusDAO;

    private Status activeStatus;

    @PostConstruct
    public void init(){
        activeStatus = statusDAO.getStatusByName(StatusEnum.ACTIVE.getName());
    }

    @Override
    public List<HomeElementDTO> getSliderImages(Integer countryId, Integer stateId, String userType, Integer deviceId, Integer languageId) {

        List<Object[]>  sliderImages = sliderImagesDAO.getSliderImagesByFilter(countryId,
                stateId,userType, deviceId, 1, languageId);
        if(CollectionUtils.isEmpty(sliderImages)){
            sliderImages = sliderImagesDAO.getDefaultSliderImages(1);
        }
        List<HomeElementDTO> imagesDTOList = new ArrayList<>(0);
        String  baseImageURL = "baseImageURL";
        sliderImages.forEach(sliderImage -> {
            imagesDTOList.add(OnePageMapper.createHomeElementDTO(sliderImage, baseImageURL));
        });
        return imagesDTOList;
    }

    @Override
    public HeaderElementsDTO getHeaderElements(Integer idTranslation, String userType) {
        List<SectionDTO>     sectionDTOList = new ArrayList<>();
        List<OnePageSection> sectionList    = onePageSectionsRepository.findAll();
        Menu menu = menuRepository.getHeader(activeStatus.getId());
        String               logo           = menu != null ? menu.getLogo() : null;
        MenuInfo menuInfo       = null;
        if (menu != null) {
            menuInfo = menuInfoRepository.getMenuInfo(menu.getId(), idTranslation);
            if (menuInfo == null) {
                menuInfo = menuInfoRepository.getMenuInfo(menu.getId(), DEFAULT_TRANSLATION_ID);
            }
        }
        for (OnePageSection section : sectionList) {
            SectionDTO dto = new SectionDTO();
            dto.setName(section.getSectionName());
            dto.setEnabled(section.getStatusBean().getName().equalsIgnoreCase(StatusEnum.ACTIVE.getName()));
            if (section.getMenuElement() != null) {
                dto.setShowInMenu(dto.isEnabled());
                String elementName = menuElementsInfoRepository.getElementName(section.getMenuElement().getId(),
                        idTranslation);
                if (elementName == null) {
                    elementName = menuElementsInfoRepository.getElementName(section.getMenuElement().getId(),
                            DEFAULT_TRANSLATION_ID);
                }
                dto.setMenuName(elementName);
            }
            sectionDTOList.add(dto);
        }

        List<Translation> translationList = translationRepository.getAllActiveDefaultTranslations(activeStatus.getId());
        List<TblFormField> cartPopup = formService.getFormFieldByFormAndTranslation(FormConstants.FORM_CART_POPUP,
                idTranslation);
        List<TblFormField> profilePopup = formService.getFormFieldByFormAndTranslation(FormConstants.FORM_PROFILE_POPUP,
                idTranslation);
        List<TblFormField> onePageForm = formService.getFormFieldByFormAndTranslation(FormConstants.ONE_PAGE_MIX_FORM,
                idTranslation);
        List<TblFormField> listingForm = formService.getFormFieldByFormAndTranslation(
                FormConstants.PRODUCT_LISTING_FORM, idTranslation);
        String baseImageURL = globalPropertiesService.getPropertyValue("images.base.url");
        HeaderElementsDTO headerDTO = OnePageMapper.createHeaderDTO(logo, menuInfo, onePageForm, cartPopup,
                profilePopup, listingForm, translationList,
                baseImageURL);
        headerDTO.setSectionList(sectionDTOList);
        headerDTO.setTranslationId(idTranslation);
        headerDTO.setBrandList(brandService.getAllBrands());
        headerDTO.setCharacterLimitMessage(staticLabelInfoService.getPropertyValue(MINIMUM_CHARACTER_LIMIT_MESSAGE,
                securityService.getCurrentLanguageId()));
        return headerDTO;
    }

    @Override
    public List<StickyNoteDTO> getStickyNotes() {
        String userType = securityService.getUserType();
        if (!userType.isEmpty() && userType.equals(UserTypeEnum.GUEST.getName())) {
            userType = UserTypeEnum.ENDUSER.getName();
        } else if (userType.isEmpty()) {
            userType = UserTypeEnum.ENDUSER.getName();
        }
        List<StickyNote> stickyNotes = stickyNoteRepository.getStickyNoteList(securityService.getCartCountryId(),
                Calendar.getInstance(), securityService.getCurrentLanguageId(), StatusEnum.DELETED.getId(),
                StatusEnum.DISABLE.getId(), userType);
        List<StickyNoteDTO> stickyNoteDTOS = new ArrayList<>();
        if (!CollectionUtils.isEmpty(stickyNotes)) {
            for (StickyNote stickyNote : stickyNotes) {
                stickyNoteDTOS.add(StickyNoteDomainMapper.createStickyMapperDTO(stickyNote));
            }
        } else {
            return Collections.emptyList();
        }
        return stickyNoteDTOS;
    }

    @Override
    public String getWhatsAppNumber() {
        return null;
    }

    @Override
    public List<MobileApplicationDTO> getMobileApplicationLinks() {
        return null;
    }


}

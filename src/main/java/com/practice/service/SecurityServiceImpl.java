package com.practice.service;


import com.practice.domain.SessionData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

import static com.practice.util.AppConstant.SESSION_DATA;

@Service
public class SecurityServiceImpl implements SecurityService {


    @Autowired
    private HttpSession httpSession;


    private SessionData getSessionDataFromSession () {
        return (SessionData) httpSession.getAttribute(SESSION_DATA);
    }

    @Override
    public Integer getCurrentLanguageId() {
        return 1;
    }

    @Override
    public String getUserType() {
        return "endUser";
    }

    @Override
    public Integer getCartCountryId() {
        return 1;
    }

    @Override
    public SessionData getSessionData() {
        return getSessionDataFromSession();
    }

    @Override
    public String getCurrentUser() {
        return "jenduser";
    }

    @Override
    public void setSessionData(SessionData sessionData) {
        httpSession.setAttribute(SESSION_DATA, sessionData);
    }

    @Override
    public String getStateId() {
        return "6";
    }
}

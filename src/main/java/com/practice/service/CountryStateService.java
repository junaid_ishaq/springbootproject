package com.practice.service;

import com.practice.domain.Country;
import com.practice.dto.StateDTO;

import java.util.List;

public interface CountryStateService {

    /** This method is supposed to return list of states with respect to country Id.
     *
     * @param countryId
     * @return list of states.
     */
    List<StateDTO> getStatesDTOListByCountry (final Integer countryId);

    Country getCountryObject(Integer cartCountryId);
}

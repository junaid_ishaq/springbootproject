package com.practice.service;

import com.practice.dao.StatusDAO;
import com.practice.dao.UserInfoDAO;
import com.practice.domain.*;
import com.practice.dto.LoginUserDTO;
import com.practice.enums.StatusEnum;
import com.practice.enums.UserTypeEnum;
import com.practice.exceptionHandling.RecordNotFoundException;
import com.practice.log.EcommerceLogger;
import com.practice.repository.CurrencyRepository;
import com.practice.repository.TempOrderRepository;
import com.practice.repository.UserInfoRepository;
import com.practice.util.AppConstant;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.practice.util.AppConstant.*;


@Service
public class LoginRegistrationServiceImpl implements LoginRegistrationService {

    private static final Logger LOGGER = Logger.getLogger(EcommerceLogger.class.getName());

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private StaticLabelInfoService staticLabelInfoService;

    @Autowired
    private UserService userService;

    @Autowired
    private StatusDAO statusDAO;

    @Autowired
    private TempOrderRepository tempOrderRepository;

    @Autowired
    private UserInfoDAO userInfoDAO;

    @Autowired
    private FeatureService featuresService;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private SecurityManager securityManager;

    public LoginRegistrationServiceImpl(SecurityManager securityManager) {
        SecurityUtils.setSecurityManager(securityManager);
    }

    @Override
    public void authenticate(LoginUserDTO dto, HttpServletRequest request) {
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.isAuthenticated() && securityService.getCurrentUser() != null) {
            return;
        }
        List<UserInfo> userinfoList = userInfoRepository.getNonGuestUserByUserNameOrEmail(dto.getUserName(),
                UserTypeEnum.GUEST.getType());

        if (CollectionUtils.isEmpty(userinfoList) && userinfoList.size() > 1) {
            throw new RecordNotFoundException(staticLabelInfoService.getPropertyValue(
                    MORE_THAN_ONE_ACCOUNT_FOUND, securityService.getCurrentLanguageId()
            ));
        }

        UserInfo userInfo = userinfoList.get(0);
        String userName = userInfo.getUserName();
        String password = new String(Base64.decode(dto.getPassword().getBytes()));
        UsernamePasswordToken token = new UsernamePasswordToken(userName, password);
        token.setRememberMe(dto.isRememberMe());
        currentUser.login(token);

        securityService.setSessionData(createSessionData(userName));
    }

    private SessionData createSessionData(String userName) {
        UserInfo userInfo = userService.getUserInfo(userName);
        userInfo.setLastLoginTime(new Date());
        userInfoRepository.saveAndFlush(userInfo);
        UserInfoAddress shippingAddress = getShippingAddress(userName);
        SessionData sessionData = securityService.getSessionData();
        if(sessionData == null){
            sessionData = new SessionData();
        }
        List<TempOrder> tempOrderList = tempOrderRepository.findAllByUserName(userName);
        if(!CollectionUtils.isEmpty(tempOrderList) && tempOrderList.get(0).getCarrierService() != null){
            sessionData.setSelectedShippingCarrier(tempOrderList.get(0).getCarrierService().getService().toUpperCase());
        }else {
            sessionData.setSelectedShippingCarrier(AppConstant.DEFAULT_CARRIER_SERVICE);
        }
        sessionData.setFullName(userInfo.getFullName());
        sessionData.setUserName(userName);
        sessionData.setEmail(userInfo.getEmail());
        sessionData.setUserType(userInfo.getUserTypeBean().getType());
        sessionData.setUserLevelPricing(userInfo.getUserLevelPrice() != null ? userInfo.getUserLevelPrice() : false);
        if(shippingAddress != null && shippingAddress.getCountry() != null){
            sessionData.setCartCountryId(shippingAddress.getCountry().getIdCountry());
            sessionData.setStateId(shippingAddress.getCountryState() != null? shippingAddress.getCountryState().getIdCountryState(): null);
            sessionData.setPricingCountry(sessionData.getCartCountryId());
        }
        sessionData.setAllowedAction(new HashMap<>());
        sessionData.setAdminUser(userInfoDAO.checkUserIsAdmin(userName));
        if (GKHAIR_STORE_NAME.equalsIgnoreCase(STORE_NAME)) {
            sessionData.setVtb(userInfoDAO.checkIsVtbUser(userName));
        }

        sessionData.setDocCabVisible(userInfoDAO.isDocCabVisible(userName));
        sessionData.setAllowedFeatures(featuresService.getAllowedFeatures());
        sessionData.setSaveCreditCardInfo(userInfo.getSaveCreditCardInfo() != null ? userInfo.getSaveCreditCardInfo() : false);
        sessionData.setQaUser(userInfoDAO.isQAUser(userName));
        sessionData.setSalesRep(userInfoDAO.isSalesRep(userName));
        //Sale Manager is only able to add user shipping cost, global discount % ,number of free Items and discount % on Item.
        sessionData.setSalesManager(userInfoDAO.isSalesManager(userName));
        sessionData.setSalesRepUserName(userName);
        sessionData.setAdditionalGlobalDiscount(BigDecimal.ZERO);
        Integer idCurrency;
        if (sessionData.getCartCountryId() != null){
            idCurrency = userInfoDAO.getCountryCurrencyId(sessionData.getCartCountryId());
            LOGGER.info("Country currency id : " +idCurrency);
            if(null != idCurrency && !idCurrency.equals(0)){
                Optional<Currency> currency = currencyRepository.findById(idCurrency);
                LOGGER.info("Country currency : " +currency.get());
                if(null != currency){
                    sessionData.setUserCurrencyISO(currency.get().getIso());
                    sessionData.setUserCurrencySymbol(currency.get().getSymbol());
                }
            }

        }
        idCurrency = userInfoDAO.getUserCurrencyId(userName);
        LOGGER.info("User currency id : " +idCurrency);
        if(null != idCurrency && !idCurrency.equals(0)){
            Optional<Currency> currency = currencyRepository.findById(idCurrency);
            LOGGER.info("User based currency : " +currency.get());
            if(null != currency){
                sessionData.setUserCurrencyISO(currency.get().getIso());
                sessionData.setUserCurrencySymbol(currency.get().getSymbol());
            }
        }
        return sessionData;
    }

    private UserInfoAddress getShippingAddress(String userName) {
        Status status = statusDAO.getStatusByName(StatusEnum.ACTIVE.getName());
        List<UserInfoAddress> shippingAddresses = userInfoRepository.getUserShippingAddress(
                userName, status.getId(), Boolean.TRUE);
        if(!CollectionUtils.isEmpty(shippingAddresses)){
            return shippingAddresses.get(0);
        }
        return null;
    }
}

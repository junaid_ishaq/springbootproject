package com.practice.service;

import com.practice.dto.HeaderElementsDTO;
import com.practice.dto.HomeElementDTO;
import com.practice.dto.MobileApplicationDTO;
import com.practice.dto.StickyNoteDTO;

import java.util.List;


public interface OnePageService {


    /**
     *
     * @param countryId
     * @param stateId
     * @param userType
     * @param deviceId
     * @param languageId
     * @return sliderImage list
     */
    List<HomeElementDTO> getSliderImages (Integer countryId, Integer stateId,
                                          String userType, Integer deviceId,
                                          Integer languageId);

    /**
     *
     * @param languageId
     * @param userType
     * @return
     */
    HeaderElementsDTO getHeaderElements(Integer languageId, String userType);

    /**
     * <p>
     *     Get all sticky notes against user country and global sticky note with in time interval
     * </p>
     * @return
     */
    List<StickyNoteDTO> getStickyNotes ();

    /**
     * <p>
     *     This method is used to get whatsapp number that is set for user country
     *     If user country whatsapp number not set than its returns default number that is set for all countries
     *     if both numbers are not set then its return null
     * </p>
     * @return
     */
    String getWhatsAppNumber ();

    /**
     *
     * @return
     */
    List<MobileApplicationDTO> getMobileApplicationLinks ();
}

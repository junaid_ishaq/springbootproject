package com.practice.service;

import com.practice.domain.Brand;
import com.practice.dto.BrandsDTO;

import java.util.List;

public interface BrandService {

    /**
     * @return
     */
    List<BrandsDTO> getAllBrands ();

    /**
     * @return
     */
    List<Brand> getAllBrandObject ();

    /**
     *
     * @param idBrand
     * @return
     */
    Brand getBrandById(Integer idBrand);
}

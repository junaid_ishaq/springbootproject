package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table (name = "product_group_visibility_per_state")
public class ProductGroupVisibilityPerState implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    //bi-directional many-to-one association to ProductGroup
    @ManyToOne
    @JoinColumn (name = "product_group_id", insertable = false, updatable = false)
    private ProductGroup productGroup;

    //bi-directional many-to-one association to CountryState
    @ManyToOne
    @JoinColumn (name = "id_state", insertable = false, updatable = false)
    private CountryState countryState;

    //bi-directional many-to-one association to UserType
    @ManyToOne
    @JoinColumn (name = "user_type", insertable = false, updatable = false)
    private UserType userTypeBean;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public ProductGroupVisibilityPerState () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public ProductGroup getProductGroup () {
        return this.productGroup;
    }

    public void setProductGroup (ProductGroup productGroup) {
        this.productGroup = productGroup;
    }

    public CountryState getCountryState () {
        return this.countryState;
    }

    public void setCountryState (CountryState countryState) {
        this.countryState = countryState;
    }

    public UserType getUserTypeBean () {
        return this.userTypeBean;
    }

    public void setUserTypeBean (UserType userTypeBean) {
        this.userTypeBean = userTypeBean;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

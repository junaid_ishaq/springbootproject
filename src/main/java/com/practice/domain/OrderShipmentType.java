package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "order_shipment_type")
public class OrderShipmentType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_order_shipment_type")
    private int idOrderShipmentType;

    private String description;

    private String name;

    //bi-directional many-to-one association to Order
    @OneToMany(mappedBy = "orderShipmentType")
    private List<Order> orders;

    public OrderShipmentType () {
    }

    public int getIdOrderShipmentType () {
        return this.idOrderShipmentType;
    }

    public void setIdOrderShipmentType (int idOrderShipmentType) {
        this.idOrderShipmentType = idOrderShipmentType;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public List<Order> getOrders () {
        return this.orders;
    }

    public void setOrders (List<Order> orders) {
        this.orders = orders;
    }

    public Order addOrder (Order order) {
        getOrders().add(order);
        order.setOrderShipmentType(this);

        return order;
    }

    public Order removeOrder (Order order) {
        getOrders().remove(order);
        order.setOrderShipmentType(null);

        return order;
    }

}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "carrier_service")
public class CarrierService implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_carrier_service")
    private int idCarrierService;

    @Column (name = "friendly_name")
    private String friendlyName;

    @Column (name = "group_name")
    private String groupName;

    private String service;

    //bi-directional many-to-one association to Carrier
    @ManyToOne
    @JoinColumn (name = "id_carrier")
    private Carrier carrier;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to Order
    @OneToMany (mappedBy = "carrierService")
    private List<Order> orders;

    public CarrierService () {
    }

    public int getIdCarrierService () {
        return this.idCarrierService;
    }

    public void setIdCarrierService (int idCarrierService) {
        this.idCarrierService = idCarrierService;
    }

    public String getFriendlyName () {
        return this.friendlyName;
    }

    public void setFriendlyName (String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getGroupName () {
        return this.groupName;
    }

    public void setGroupName (String groupName) {
        this.groupName = groupName;
    }

    public String getService () {
        return this.service;
    }

    public void setService (String service) {
        this.service = service;
    }

    public Carrier getCarrier () {
        return this.carrier;
    }

    public void setCarrier (Carrier carrier) {
        this.carrier = carrier;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<Order> getOrders () {
        return this.orders;
    }

    public void setOrders (List<Order> orders) {
        this.orders = orders;
    }

    public Order addOrder (Order order) {
        getOrders().add(order);
        order.setCarrierService(this);

        return order;
    }

    public Order removeOrder (Order order) {
        getOrders().remove(order);
        order.setCarrierService(null);

        return order;
    }

}

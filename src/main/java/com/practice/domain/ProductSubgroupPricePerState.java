package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "product_subgroup_price_per_state")
public class ProductSubgroupPricePerState implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    private BigDecimal price;

    //bi-directional many-to-one association to ProductSubgroup
    @ManyToOne
    @JoinColumn(name = "product_subgroup", insertable = false, updatable = false)
    private ProductSubgroup productSubgroupBean;

    //bi-directional many-to-one association to CountryState
    @ManyToOne
    @JoinColumn (name = "id_state", insertable = false, updatable = false)
    private CountryState countryState;

    //bi-directional many-to-one association to UserType
    @ManyToOne
    @JoinColumn (name = "user_type", insertable = false, updatable = false)
    private UserType userTypeBean;

    public ProductSubgroupPricePerState () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public BigDecimal getPrice () {
        return this.price;
    }

    public void setPrice (BigDecimal price) {
        this.price = price;
    }

    public ProductSubgroup getProductSubgroupBean () {
        return this.productSubgroupBean;
    }

    public void setProductSubgroupBean (ProductSubgroup productSubgroupBean) {
        this.productSubgroupBean = productSubgroupBean;
    }

    public CountryState getCountryState () {
        return this.countryState;
    }

    public void setCountryState (CountryState countryState) {
        this.countryState = countryState;
    }

    public UserType getUserTypeBean () {
        return this.userTypeBean;
    }

    public void setUserTypeBean (UserType userTypeBean) {
        this.userTypeBean = userTypeBean;
    }

}

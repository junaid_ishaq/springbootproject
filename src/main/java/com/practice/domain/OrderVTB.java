package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "orders_vtb")
public class OrderVTB implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn (name = "id_order")
    private Order order;

    //bi-directional many-to-one association to Userinfo
    @ManyToOne
    @JoinColumn (name = "id_distributor")
    private UserInfo distributor;

    @Column (name = "shipment_cost_paid_dist")
    private BigDecimal shipmentCostPaidDist;

    @Column (name = "shipment_cost_paid_company")
    private BigDecimal shipmentCostPaidCompany;


    //bi-directional many-to-one association to Class
    @ManyToOne
    @JoinColumn (name = "id_class")
    private Class class11;

    //bi-directional many-to-one association to Subsidiary
    @ManyToOne
    @JoinColumn (name = "id_subsidiary")
    private Subsidiary subsidiary;


    @Basic
    @Column (name = "sales_order_number")
    private String soNumber;

    @Column(name = "commission")
    private BigDecimal commission;

    @Column(name = "additional_global_discount")
    private BigDecimal additionalGlobalDiscount;

    @ManyToOne
    @JoinColumn(name = "id_warehouse")
    private WareHouse warehouse;


    @Column(name = "picking_ticket_generated")
    private Boolean pickingTicketGenerated;

    @Column(name = "tracking_number")
    private String trackingNumber;

    @Column(name = "credit_amount")
    private BigDecimal creditAmount;

    @Column(name = "generated_fedex_labels")
    private boolean fedexLabelGenerated;

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "approval_date")
    private Date approvalDate;


    public Integer getId () {
        return id;
    }

    public void setId (final Integer id) {
        this.id = id;
    }

    public Order getOrder () {
        return order;
    }

    public void setOrder (final Order order) {
        this.order = order;
    }

    public UserInfo getDistributor () {
        return distributor;
    }

    public void setDistributor (final UserInfo distributor) {
        this.distributor = distributor;
    }

    public BigDecimal getShipmentCostPaidDist () {
        return shipmentCostPaidDist;
    }

    public void setShipmentCostPaidDist (final BigDecimal shipmentCostPaidDist) {
        this.shipmentCostPaidDist = shipmentCostPaidDist;
    }

    public BigDecimal getShipmentCostPaidCompany () {
        return shipmentCostPaidCompany;
    }

    public void setShipmentCostPaidCompany (final BigDecimal shipmentCostPaidCompany) {
        this.shipmentCostPaidCompany = shipmentCostPaidCompany;
    }

    public Class getClass11 () {
        return class11;
    }

    public void setClass11 (final Class class11) {
        this.class11 = class11;
    }

    public Subsidiary getSubsidiary () {
        return subsidiary;
    }

    public void setSubsidiary (final Subsidiary subsidiary) {
        this.subsidiary = subsidiary;
    }


    public String getSoNumber () {
        return soNumber;
    }

    public void setSoNumber (final String soNumber) {
        this.soNumber = soNumber;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public BigDecimal getAdditionalGlobalDiscount() {
        this.additionalGlobalDiscount = this.additionalGlobalDiscount == null? BigDecimal.ZERO:this.additionalGlobalDiscount;
        return additionalGlobalDiscount;
    }

    public void setAdditionalGlobalDiscount(BigDecimal additionalGlobalDiscount) {
        this.additionalGlobalDiscount = additionalGlobalDiscount;
    }

    public WareHouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(WareHouse warehouse) {
        this.warehouse = warehouse;
    }


    public Boolean isPickingTicketGenerated() {
        return pickingTicketGenerated;
    }

    public void setPickingTicketGenerated(Boolean pickingTicketGenerated) {
        this.pickingTicketGenerated = pickingTicketGenerated;
    }
    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public boolean isFedexLabelGenerated() {
        return fedexLabelGenerated;
    }

    public void setFedexLabelGenerated(boolean fedexLabelGenerated) {
        this.fedexLabelGenerated = fedexLabelGenerated;
    }

    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }
}

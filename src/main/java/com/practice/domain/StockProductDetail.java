package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "stock_product_detail")
public class StockProductDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    @Lob
    private String description;

    @Lob
    @Column(name = "meta_description")
    private String metaDescription;

    @Lob
    @Column (name = "meta_keywords")
    private String metaKeywords;

    @Lob
    @Column(name="meta_title")
    private  String metaTitle;

    private String video;

    //bi-directional many-to-one association to StockProductSize
    @ManyToOne
    @JoinColumn (name = "id_product")
    private StockProductSize stockProductSize;

    //bi-directional many-to-one association to Translation
    @ManyToOne
    @JoinColumn (name = "id_translation")
    private Translation translation;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;


    public StockProductDetail () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getMetaDescription () {
        return this.metaDescription;
    }

    public void setMetaDescription (String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMetaKeywords () {
        return this.metaKeywords;
    }

    public void setMetaKeywords (String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getVideo () {
        return this.video;
    }

    public void setVideo (String video) {
        this.video = video;
    }

    public StockProductSize getStockProductSize () {
        return this.stockProductSize;
    }

    public void setStockProductSize (StockProductSize stockProductSize) {
        this.stockProductSize = stockProductSize;
    }

    public Translation getTranslation () {
        return this.translation;
    }

    public void setTranslation (Translation translation) {
        this.translation = translation;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public String getMetaTitle () {
        return metaTitle;
    }

    public void setMetaTitle (final String metaTitle) {
        this.metaTitle = metaTitle;
    }
}

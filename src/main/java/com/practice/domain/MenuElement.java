package com.practice.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "menu_element")
public class MenuElement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "sequence_order")
    private int sequenceOrder;

    //bi-directional many-to-one association to Menu
    @ManyToOne
    @JoinColumn (name = "id_menu")
    private Menu menu;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to MenuElementInfo
    @OneToMany (mappedBy = "menuElement")
    private List<MenuElementInfo> menuElementInfos;

    //bi-directional many-to-one association to OnePageSection
    @OneToMany (mappedBy = "menuElement")
    private List<OnePageSection> onePageSections;

    public MenuElement() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSequenceOrder() {
        return sequenceOrder;
    }

    public void setSequenceOrder(int sequenceOrder) {
        this.sequenceOrder = sequenceOrder;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<MenuElementInfo> getMenuElementInfos() {
        return menuElementInfos;
    }

    public void setMenuElementInfos(List<MenuElementInfo> menuElementInfos) {
        this.menuElementInfos = menuElementInfos;
    }

    public List<OnePageSection> getOnePageSections() {
        return onePageSections;
    }

    public void setOnePageSections(List<OnePageSection> onePageSections) {
        this.onePageSections = onePageSections;
    }
}

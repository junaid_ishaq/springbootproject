package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "carriers")
public class Carrier implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_carrier")
    private int idCarrier;

    private String carrier;

    @Column (name = "tracking_url")
    private String trackingUrl;

    //bi-directional many-to-one association to CarrierService
    @OneToMany(mappedBy = "carrier")
    private List<CarrierService> carrierServices;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to Order
    @OneToMany (mappedBy = "carrier")
    private List<Order> orders;

    public Carrier () {
    }

    public int getIdCarrier () {
        return this.idCarrier;
    }

    public void setIdCarrier (int idCarrier) {
        this.idCarrier = idCarrier;
    }

    public String getCarrier () {
        return this.carrier;
    }

    public void setCarrier (String carrier) {
        this.carrier = carrier;
    }

    public String getTrackingUrl () {
        return this.trackingUrl;
    }

    public void setTrackingUrl (String trackingUrl) {
        this.trackingUrl = trackingUrl;
    }

    public List<CarrierService> getCarrierServices () {
        return this.carrierServices;
    }

    public void setCarrierServices (List<CarrierService> carrierServices) {
        this.carrierServices = carrierServices;
    }

    public CarrierService addCarrierService (CarrierService carrierService) {
        getCarrierServices().add(carrierService);
        carrierService.setCarrier(this);

        return carrierService;
    }

    public CarrierService removeCarrierService (CarrierService carrierService) {
        getCarrierServices().remove(carrierService);
        carrierService.setCarrier(null);

        return carrierService;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<Order> getOrders () {
        return this.orders;
    }

    public void setOrders (List<Order> orders) {
        this.orders = orders;
    }

    public Order addOrder (Order order) {
        getOrders().add(order);
        order.setCarrier(this);

        return order;
    }

    public Order removeOrder (Order order) {
        getOrders().remove(order);
        order.setCarrier(null);

        return order;
    }

}

package com.practice.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "menu")
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id", nullable = false)
    @Basic (fetch = FetchType.EAGER)
    private int id;

    @Column (name = "logo")
    private String logo;

    @Column (name = "type")
    private String type;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to MenuElement
    @OneToMany (mappedBy = "menu")
    private List<MenuElement> menuElements;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<MenuElement> getMenuElements() {
        return menuElements;
    }

    public void setMenuElements(List<MenuElement> menuElements) {
        this.menuElements = menuElements;
    }
}

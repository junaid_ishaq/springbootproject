package com.practice.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "payment_type")
public class PaymentType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    private String name;


    public PaymentType () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

}

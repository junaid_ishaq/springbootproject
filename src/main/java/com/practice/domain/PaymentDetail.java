package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "payment_details")
public class PaymentDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer id;

    private BigDecimal amount;

    @Lob
    private String description;

    @Column (name = "is_testing")
    private Boolean isTesting;

    @Column (name = "transaction_id")
    private String transactionId;

    //bi-directional many-to-one association to PaymentGatewayForCountryState
    @ManyToOne
    @JoinColumn (name = "gateway_id")
    private PaymentGatewayForCountryState paymentGatewayForCountryState;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "payment_status")
    private Status status;

    //bi-directional many-to-one association to Order
    @ManyToOne
    @JoinColumn (name = "order_id")
    private Order order;

    //bi-directional many-to-one association to PaymentType
    @ManyToOne
    @JoinColumn (name = "payment_type")
    private PaymentType paymentTypeBean;

    //    //bi-directional many-to-one association to UserActivity
    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn (name = "activity_id")
    private UserActivity userActivity;

    @Basic
    @Column (name = "card_name")
    private String cardName;
    @Basic
    @Column (name = "card_number")
    private String cardNumber;
    @Basic
    @Column (name = "card_type")
    private String cardType;
    @Basic
    @Column (name = "card_expiry_month")
    private String cardExpiryMonth;
    @Basic
    @Column (name = "card_expiry_year")
    private String cardExpiryYear;

    @Column (name = "exchange_rate")
    private BigDecimal exchangeRate;

    @Column (name = "customer_exchange_rate")
    private BigDecimal customerExchangeRate;

    @Column (name = "subsidiary_exchange_rate")
    private BigDecimal subsidiaryExchangeRate;

    @Column (name = "inter_company_exchange_rate")
    private BigDecimal interCompanyExchangeRate;

    @Column(name = "sale_payment")
    private boolean salePayment;

    @Basic
    private boolean used;

    public PaymentDetail () {
    }

    public Integer getId () {
        return this.id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount () {
        return this.amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public Boolean getIsTesting () {
        return this.isTesting;
    }

    public void setIsTesting (Boolean isTesting) {
        this.isTesting = isTesting;
    }

    public String getTransactionId () {
        return this.transactionId;
    }

    public void setTransactionId (String transactionId) {
        this.transactionId = transactionId;
    }

    public PaymentGatewayForCountryState getPaymentGatewayForCountryState () {
        return this.paymentGatewayForCountryState;
    }

    public void setPaymentGatewayForCountryState (PaymentGatewayForCountryState paymentGatewayForCountryState) {
        this.paymentGatewayForCountryState = paymentGatewayForCountryState;
    }

    public Status getStatus () {
        return this.status;
    }

    public void setStatus (Status status) {
        this.status = status;
    }

    public Order getOrder () {
        return this.order;
    }

    public void setOrder (Order order) {
        this.order = order;
    }

    public PaymentType getPaymentTypeBean () {
        return this.paymentTypeBean;
    }

    public void setPaymentTypeBean (PaymentType paymentTypeBean) {
        this.paymentTypeBean = paymentTypeBean;
    }

    public UserActivity getUserActivity () {
        return this.userActivity;
    }

    public void setUserActivity (UserActivity userActivity) {
        this.userActivity = userActivity;
    }

    public String getCardName () {
        return cardName;
    }

    public void setCardName (final String cardName) {
        this.cardName = cardName;
    }

    public String getCardNumber () {
        return cardNumber;
    }

    public void setCardNumber (final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardType () {
        return cardType;
    }

    public void setCardType (final String cardType) {
        this.cardType = cardType;
    }

    public String getCardExpiryMonth () {
        return cardExpiryMonth;
    }

    public void setCardExpiryMonth (final String cardExpiryMonth) {
        this.cardExpiryMonth = cardExpiryMonth;
    }

    public String getCardExpiryYear () {
        return cardExpiryYear;
    }

    public void setCardExpiryYear (final String cardExpiryYear) {
        this.cardExpiryYear = cardExpiryYear;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public BigDecimal getCustomerExchangeRate() {
        return customerExchangeRate;
    }

    public void setCustomerExchangeRate(BigDecimal customerExchangeRate) {
        this.customerExchangeRate = customerExchangeRate;
    }

    public BigDecimal getSubsidiaryExchangeRate() {
        return subsidiaryExchangeRate;
    }

    public void setSubsidiaryExchangeRate(BigDecimal subsidiaryExchangeRate) {
        this.subsidiaryExchangeRate = subsidiaryExchangeRate;
    }

    public BigDecimal getInterCompanyExchangeRate() {
        return interCompanyExchangeRate;
    }

    public void setInterCompanyExchangeRate(BigDecimal interCompanyExchangeRate) {
        this.interCompanyExchangeRate = interCompanyExchangeRate;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public boolean isSalePayment() {
        return false;
    }

    public void setSalePayment(boolean salePayment) {
        this.salePayment = salePayment;
    }
}

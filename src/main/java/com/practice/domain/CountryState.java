package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "country_states")
public class CountryState implements Serializable {

    private static final long serialVersionUID = 9875343434343L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_country_state")
    private Integer IdCountryState;

    @ManyToOne
    @JoinColumn(name = "id_country")
    private Country country;

    @Column(name = "name")
    private String name;

    @Column(name = "iso_code")
    private String isoCode;

    @Column(name = "iso_code_full")
    private String isoCodeFull;

    @ManyToOne
    @JoinColumn (name = "time_zone")
    private TimeZone timeZoneBean;

    public Integer getIdCountryState() {
        return IdCountryState;
    }

    public void setIdCountryState(Integer idCountryState) {
        IdCountryState = idCountryState;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getIsoCodeFull() {
        return isoCodeFull;
    }

    public void setIsoCodeFull(String isoCodeFull) {
        this.isoCodeFull = isoCodeFull;
    }

    public TimeZone getTimeZoneBean() {
        return timeZoneBean;
    }

    public void setTimeZoneBean(TimeZone timeZoneBean) {
        this.timeZoneBean = timeZoneBean;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "order_shipment_terms")
public class OrderShipmentTerm implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_shipment_terms")
    private int idShipmentTerms;

    private String description;

    private String name;

    //bi-directional many-to-one association to Order
    @OneToMany(mappedBy = "orderShipmentTerm")
    private List<Order> orders;

    public OrderShipmentTerm () {
    }

    public int getIdShipmentTerms () {
        return this.idShipmentTerms;
    }

    public void setIdShipmentTerms (int idShipmentTerms) {
        this.idShipmentTerms = idShipmentTerms;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public List<Order> getOrders () {
        return this.orders;
    }

    public void setOrders (List<Order> orders) {
        this.orders = orders;
    }

    public Order addOrder (Order order) {
        getOrders().add(order);
        order.setOrderShipmentTerm(this);

        return order;
    }

    public Order removeOrder (Order order) {
        getOrders().remove(order);
        order.setOrderShipmentTerm(null);

        return order;
    }

}

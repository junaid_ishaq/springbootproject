package com.practice.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProductSubgroupPricePerUserPK implements Serializable {
    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "product_subgroup", insertable = false, updatable = false)
    private int productSubgroup;

    @Column (name = "user_name", insertable = false, updatable = false)
    private String userName;

    public ProductSubgroupPricePerUserPK () {
    }

    public int getProductSubgroup () {
        return this.productSubgroup;
    }

    public void setProductSubgroup (int productSubgroup) {
        this.productSubgroup = productSubgroup;
    }

    public String getUserName () {
        return this.userName;
    }

    public void setUserName (String userName) {
        this.userName = userName;
    }

    public boolean equals (Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ProductSubgroupPricePerUserPK)) {
            return false;
        }
        ProductSubgroupPricePerUserPK castOther = (ProductSubgroupPricePerUserPK) other;
        return (this.productSubgroup == castOther.productSubgroup) && this.userName.equals(castOther.userName);
    }

    public int hashCode () {
        final int prime = 31;
        int       hash  = 17;
        hash = hash * prime + this.productSubgroup;
        hash = hash * prime + this.userName.hashCode();

        return hash;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "product_weight")
public class ProductWeight implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private int id;

    private String description;

    private double value;

    //bi-directional many-to-one association to SizeWeightUnit
    @ManyToOne
    @JoinColumn (name = "unit_id")
    private SizeWeightUnit sizeWeightUnit;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to StockProductSize
    @OneToMany (mappedBy = "productWeight")
    private List<StockProductSize> stockProductSizes;

    public ProductWeight () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public double getValue () {
        return this.value;
    }

    public void setValue (double value) {
        this.value = value;
    }

    public SizeWeightUnit getSizeWeightUnit () {
        return this.sizeWeightUnit;
    }

    public void setSizeWeightUnit (SizeWeightUnit sizeWeightUnit) {
        this.sizeWeightUnit = sizeWeightUnit;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<StockProductSize> getStockProductSizes () {
        return this.stockProductSizes;
    }

    public void setStockProductSizes (List<StockProductSize> stockProductSizes) {
        this.stockProductSizes = stockProductSizes;
    }

    public StockProductSize addStockProductSize (StockProductSize stockProductSize) {
        getStockProductSizes().add(stockProductSize);
        stockProductSize.setProductWeight(this);

        return stockProductSize;
    }

    public StockProductSize removeStockProductSize (StockProductSize stockProductSize) {
        getStockProductSizes().remove(stockProductSize);
        stockProductSize.setProductWeight(null);

        return stockProductSize;
    }

}

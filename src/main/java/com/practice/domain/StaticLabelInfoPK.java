package com.practice.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class StaticLabelInfoPK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "id_static_label", insertable = false, updatable = false)
    private int idStaticLabel;

    @Column (name = "id_translation", insertable = false, updatable = false)
    private int idTranslation;

    public int getIdStaticLabel() {
        return idStaticLabel;
    }

    public void setIdStaticLabel(int idStaticLabel) {
        this.idStaticLabel = idStaticLabel;
    }

    public int getIdTranslation() {
        return idTranslation;
    }

    public void setIdTranslation(int idTranslation) {
        this.idTranslation = idTranslation;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "order_additional_information")
public class OrderAdditionalInformation implements Serializable {
    private static final long serialVersionUID = 8041186547478087954L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;

    @OneToOne
    @JoinColumn (name = "id_order")
    private Order order;

    @Column (name = "salon")
    private String salon;

    @Column (name = "salon_address")
    private String salonAddress;

    @Column (name = "salon_lat")
    private String salonLatitude;

    @Column (name = "salon_long")
    private String salonLongitude;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public String getSalonAddress() {
        return salonAddress;
    }

    public void setSalonAddress(String salonAddress) {
        this.salonAddress = salonAddress;
    }

    public String getSalonLatitude() {
        return salonLatitude;
    }

    public void setSalonLatitude(String salonLatitude) {
        this.salonLatitude = salonLatitude;
    }

    public String getSalonLongitude() {
        return salonLongitude;
    }

    public void setSalonLongitude(String salonLongitude) {
        this.salonLongitude = salonLongitude;
    }
}


package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "user_activity")
public class UserActivity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Basic
    @Column(name = "user_activity_id")
    private int userActivityId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "activity_date")
    private Date activityDate;

    @Lob
    private String description;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "process_id")
    private String processId;

    //bi-directional many-to-one association to UserInfo
    @ManyToOne
    @JoinColumn(name = "user_name")
    private UserInfo UserInfo;

    //bi-directional many-to-one association to Account
    @ManyToOne
    @JoinColumn(name = "action_id")
    private Action action;

    //bi-directional many-to-one association to SubAction
    @ManyToOne
    @JoinColumn(name = "sub_action_id")
    private SubAction subAction;

    public UserActivity() {
    }

    public int getUserActivityId() {
        return this.userActivityId;
    }

    public void setUserActivityId(int userActivityId) {
        this.userActivityId = userActivityId;
    }

    public Date getActivityDate() {
        return this.activityDate;
    }

    public void setActivityDate(Date activityDate) {
        this.activityDate = activityDate;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIpAddress() {
        return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public void setProcessId(final String processId) {
        this.processId = processId;
    }


    public UserInfo getUserInfo() {
        return this.UserInfo;
    }

    public void setUserInfo(UserInfo UserInfo) {
        this.UserInfo = UserInfo;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(final Action action) {
        this.action = action;
    }

    public SubAction getSubAction() {
        return this.subAction;
    }

    public void setSubAction(SubAction subAction) {
        this.subAction = subAction;
    }
}



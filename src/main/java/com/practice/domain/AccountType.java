package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "account_type")
public class AccountType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_account_type")
    private int idAccountType;

    private String type;

    //bi-directional many-to-one association to Account
    @OneToMany(mappedBy = "accountType")
    private List<Account> accounts;

    public AccountType () {
    }

    public int getIdAccountType () {
        return this.idAccountType;
    }

    public void setIdAccountType (int idAccountType) {
        this.idAccountType = idAccountType;
    }

    public String getType () {
        return this.type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public List<Account> getAccounts () {
        return this.accounts;
    }

    public void setAccounts (List<Account> accounts) {
        this.accounts = accounts;
    }

    public Account addAccount (Account account) {
        getAccounts().add(account);
        account.setAccountType(this);

        return account;
    }

    public Account removeAccount (Account account) {
        getAccounts().remove(account);
        account.setAccountType(null);

        return account;
    }

}

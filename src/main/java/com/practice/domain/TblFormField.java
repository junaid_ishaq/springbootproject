package com.practice.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_form_fields")
public class TblFormField implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tbl_form_fields")
    private int idTblFormFields;

    @Column (name = "field_name")
    private String fieldName;

    @Lob
    @Column (name = "field_value")
    private String fieldValue;

    //bi-directional many-to-one association to TblForm
    @ManyToOne
    @JoinColumn (name = "id_tblform")
    private TblForm tblForm;

    //bi-directional many-to-one association to Translation
    @ManyToOne
    @JoinColumn (name = "id_translation")
    private Translation translation;

    public int getIdTblFormFields() {
        return idTblFormFields;
    }

    public void setIdTblFormFields(int idTblFormFields) {
        this.idTblFormFields = idTblFormFields;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    public TblForm getTblForm() {
        return tblForm;
    }

    public void setTblForm(TblForm tblForm) {
        this.tblForm = tblForm;
    }

    public Translation getTranslation() {
        return translation;
    }

    public void setTranslation(Translation translation) {
        this.translation = translation;
    }
}

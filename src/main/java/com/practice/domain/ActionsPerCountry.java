package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "actions_per_country")
public class ActionsPerCountry implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ActionsPerCountryPK id;

    //bi-directional many-to-one association to Action
    @ManyToOne
    @JoinColumn(name = "id_action", insertable = false, updatable = false)
    private Action action;

    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "country_id", insertable = false, updatable = false)
    private Country country;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public ActionsPerCountry () {
    }

    public ActionsPerCountryPK getId () {
        return this.id;
    }

    public void setId (ActionsPerCountryPK id) {
        this.id = id;
    }

    public Action getAction () {
        return this.action;
    }

    public void setAction (Action action) {
        this.action = action;
    }

    public Country getCountry () {
        return this.country;
    }

    public void setCountry (Country country) {
        this.country = country;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

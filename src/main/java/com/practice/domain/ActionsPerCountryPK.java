package com.practice.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ActionsPerCountryPK implements Serializable {
    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "id_action", insertable = false, updatable = false)
    private int idAction;

    @Column (name = "country_id", insertable = false, updatable = false)
    private int countryId;

    public ActionsPerCountryPK () {
    }

    public int getIdAction () {
        return this.idAction;
    }

    public void setIdAction (int idAction) {
        this.idAction = idAction;
    }

    public int getCountryId () {
        return this.countryId;
    }

    public void setCountryId (int countryId) {
        this.countryId = countryId;
    }

    public boolean equals (Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ActionsPerCountryPK)) {
            return false;
        }
        ActionsPerCountryPK castOther = (ActionsPerCountryPK) other;
        return (this.idAction == castOther.idAction) && (this.countryId == castOther.countryId);
    }

    public int hashCode () {
        final int prime = 31;
        int       hash  = 17;
        hash = hash * prime + this.idAction;
        hash = hash * prime + this.countryId;

        return hash;
    }
}

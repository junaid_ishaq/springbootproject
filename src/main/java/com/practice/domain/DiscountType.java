package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "discount_type")
public class DiscountType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    @Column(name = "business_rule_table")
    private String businessRuleTable;

    //bi-directional many-to-one association to Discount
    @OneToMany(mappedBy = "discountType")
    private List<Discount> discounts;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public DiscountType () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getBusinessRuleTable () {
        return this.businessRuleTable;
    }

    public void setBusinessRuleTable (String businessRuleTable) {
        this.businessRuleTable = businessRuleTable;
    }

    public List<Discount> getDiscounts () {
        return this.discounts;
    }

    public void setDiscounts (List<Discount> discounts) {
        this.discounts = discounts;
    }

    public Discount addDiscount (Discount discount) {
        getDiscounts().add(discount);
        discount.setDiscountType(this);

        return discount;
    }

    public Discount removeDiscount (Discount discount) {
        getDiscounts().remove(discount);
        discount.setDiscountType(null);

        return discount;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "zone")
public class Zone implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_zone")
    private int idZone;

    private String description;

    private String name;

    //bi-directional many-to-one association to Warehouse
    @OneToMany(mappedBy = "zone")
    private List<WareHouse> warehouses;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public Zone () {
    }

    public int getIdZone () {
        return this.idZone;
    }

    public void setIdZone (int idZone) {
        this.idZone = idZone;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public List<WareHouse> getWarehouses () {
        return this.warehouses;
    }

    public void setWarehouses (List<WareHouse> warehouses) {
        this.warehouses = warehouses;
    }

    public WareHouse addWarehous (WareHouse warehous) {
        getWarehouses().add(warehous);
        warehous.setZone(this);

        return warehous;
    }

    public WareHouse removeWarehous (WareHouse warehous) {
        getWarehouses().remove(warehous);
        warehous.setZone(null);

        return warehous;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

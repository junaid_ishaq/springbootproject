package com.practice.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class UserInfoActionPK implements Serializable {
    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "action_id", insertable = false, updatable = false)
    private int actionId;

    @Column(name = "user_name", insertable = false, updatable = false)
    private String userName;

    public UserInfoActionPK() {
    }

    public int getActionId() {
        return this.actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof UserInfoActionPK)) {
            return false;
        }
        UserInfoActionPK castOther = (UserInfoActionPK) other;
        return (this.actionId == castOther.actionId) && this.userName.equals(castOther.userName);
    }

    public int hashCode() {
        final int prime = 31;
        int hash = 17;
        hash = hash * prime + this.actionId;
        hash = hash * prime + this.userName.hashCode();

        return hash;
    }
}


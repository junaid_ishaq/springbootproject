package com.practice.domain;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "static_label_info")
public class StaticLabelInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private StaticLabelInfoPK id;

    @Lob
    @Column(name = "label_text")
    private String labelText;

    //bi-directional many-to-one association to StaticLabel
    @ManyToOne
    @JoinColumn (name = "id_static_label", insertable = false, updatable = false)
    private StaticLabel staticLabel;

    //bi-directional many-to-one association to Translation
    @ManyToOne
    @JoinColumn (name = "id_translation", insertable = false, updatable = false)
    private Translation translation;

    public StaticLabelInfoPK getId() {
        return id;
    }

    public void setId(StaticLabelInfoPK id) {
        this.id = id;
    }

    public String getLabelText() {
        return labelText;
    }

    public void setLabelText(String labelText) {
        this.labelText = labelText;
    }

    public StaticLabel getStaticLabel() {
        return staticLabel;
    }

    public void setStaticLabel(StaticLabel staticLabel) {
        this.staticLabel = staticLabel;
    }

    public Translation getTranslation() {
        return translation;
    }

    public void setTranslation(Translation translation) {
        this.translation = translation;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "product_group_price_per_user")
public class ProductGroupPricePerUser implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ProductGroupPricePerUserPK id;

    private BigDecimal price;

    //bi-directional many-to-one association to ProductGroup
    @ManyToOne
    @JoinColumn(name = "product_group", insertable = false, updatable = false)
    private ProductGroup productGroupBean;

    //bi-directional many-to-one association to Userinfo
    @ManyToOne
    @JoinColumn (name = "user_name", insertable = false, updatable = false)
    private UserInfo userinfo;

    public ProductGroupPricePerUser () {
    }

    public ProductGroupPricePerUserPK getId () {
        return this.id;
    }

    public void setId (ProductGroupPricePerUserPK id) {
        this.id = id;
    }

    public BigDecimal getPrice () {
        return this.price;
    }

    public void setPrice (BigDecimal price) {
        this.price = price;
    }

    public ProductGroup getProductGroupBean () {
        return this.productGroupBean;
    }

    public void setProductGroupBean (ProductGroup productGroupBean) {
        this.productGroupBean = productGroupBean;
    }

    public UserInfo getUserinfo () {
        return this.userinfo;
    }

    public void setUserinfo (UserInfo userinfo) {
        this.userinfo = userinfo;
    }

}

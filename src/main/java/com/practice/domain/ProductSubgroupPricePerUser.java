package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "product_subgroup_price_per_user")
public class ProductSubgroupPricePerUser implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ProductSubgroupPricePerUserPK id;

    private BigDecimal price;

    //bi-directional many-to-one association to ProductSubgroup
    @ManyToOne
    @JoinColumn(name = "product_subgroup", insertable = false, updatable = false)
    private ProductSubgroup productSubgroupBean;

    //bi-directional many-to-one association to Userinfo
    @ManyToOne
    @JoinColumn (name = "user_name", insertable = false, updatable = false)
    private UserInfo userinfo;

    public ProductSubgroupPricePerUser () {
    }

    public ProductSubgroupPricePerUserPK getId () {
        return this.id;
    }

    public void setId (ProductSubgroupPricePerUserPK id) {
        this.id = id;
    }

    public BigDecimal getPrice () {
        return this.price;
    }

    public void setPrice (BigDecimal price) {
        this.price = price;
    }

    public ProductSubgroup getProductSubgroupBean () {
        return this.productSubgroupBean;
    }

    public void setProductSubgroupBean (ProductSubgroup productSubgroupBean) {
        this.productSubgroupBean = productSubgroupBean;
    }

    public UserInfo getUserinfo () {
        return this.userinfo;
    }

    public void setUserinfo (UserInfo userinfo) {
        this.userinfo = userinfo;
    }

}

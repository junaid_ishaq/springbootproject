package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "order_ship_via")
public class OrderShipVia implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_ship_via")
    private int idShipVia;

    private String name;

    //bi-directional many-to-one association to Order
    @OneToMany(mappedBy = "orderShipVia")
    private List<Order> orders;

    public OrderShipVia () {
    }

    public int getIdShipVia () {
        return this.idShipVia;
    }

    public void setIdShipVia (int idShipVia) {
        this.idShipVia = idShipVia;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public List<Order> getOrders () {
        return this.orders;
    }

    public void setOrders (List<Order> orders) {
        this.orders = orders;
    }

    public Order addOrder (Order order) {
        getOrders().add(order);
        order.setOrderShipVia(this);

        return order;
    }

    public Order removeOrder (Order order) {
        getOrders().remove(order);
        order.setOrderShipVia(null);

        return order;
    }

}

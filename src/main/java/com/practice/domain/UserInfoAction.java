package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "UserInfo_actions")
public class UserInfoAction implements Serializable {
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private UserInfoActionPK id;

    //bi-directional many-to-one association to Action
    @ManyToOne
    @JoinColumn(name = "action_id", insertable = false, updatable = false)
    private Action action;

    //bi-directional many-to-one association to UserInfo
    @ManyToOne
    @JoinColumn (name = "user_name", insertable = false, updatable = false)
    private UserInfo UserInfo;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public UserInfoAction () {
    }

    public UserInfoActionPK getId () {
        return this.id;
    }

    public void setId (UserInfoActionPK id) {
        this.id = id;
    }

    public Action getAction () {
        return this.action;
    }

    public void setAction (Action action) {
        this.action = action;
    }

    public UserInfo getUserInfo () {
        return this.UserInfo;
    }

    public void setUserInfo (UserInfo UserInfo) {
        this.UserInfo = UserInfo;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

package com.practice.domain;

import com.practice.dto.PaymentResponseDTO;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "order_id")
    private int orderId;

    //bi-directional many-to-one association to Warehouse
    @ManyToOne
    @JoinColumn (name = "id_warehouse")
    private WareHouse warehouse;

    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "id_country")
    private Country country;

    //bi-directional many-to-one association to UserInfo
    @ManyToOne
    @JoinColumn (name = "user_name")
    private UserInfo user;

    @Column (name = "id_tax_certificate")
    private Integer taxCertificate;

    @Column (name = "id_uploaded_files")
    private Integer taxCertificateFile;

    //bi-directional many-to-one association to UserInfo
    @ManyToOne
    @JoinColumn (name = "created_by")
    private UserInfo createdBy;

    //bi-directional many-to-one association to OrderShipmentType
    @ManyToOne
    @JoinColumn (name = "id_order_shipment_type")
    private OrderShipmentType orderShipmentType;

    //bi-directional many-to-one association to OrderShipmentTerm
    @ManyToOne
    @JoinColumn (name = "id_shipment_terms")
    private OrderShipmentTerm orderShipmentTerm;

    @Temporal (TemporalType.TIMESTAMP)
    @Column (name = "order_created_date")
    private Date orderCreatedDate;

    //bi-directional many-to-one association to Currency
    @ManyToOne
    @JoinColumn (name = "from_currency")
    private Currency fromCurrency;

    //bi-directional many-to-one association to Currency
    @ManyToOne
    @JoinColumn (name = "to_currency")
    private Currency toCurrency;

    @Column (name = "exchange_rate")
    private BigDecimal exchangeRate;

    @Lob
    @Column (name = "order_reference_number")
    private String orderReferenceNumber;

    @Temporal (TemporalType.TIMESTAMP)
    @Column (name = "recolection_date")
    private Date recollectionDate;

    @Column (name = "shipment_attachments")
    private String shipmentAttachments;

    //bi-directional many-to-one association to Carrier
    @ManyToOne
    @JoinColumn (name = "id_carrier")
    private Carrier carrier;

    //bi-directional many-to-one association to CarrierService
    @ManyToOne
    @JoinColumn (name = "id_carrier_service")
    private CarrierService carrierService;

    //bi-directional many-to-one association to OrderShipVia
    @ManyToOne
    @JoinColumn (name = "id_ship_via")
    private OrderShipVia orderShipVia;

    @Column (name = "sub_total")
    private BigDecimal subTotal;

    @Column (name = "grand_total")
    private BigDecimal grandTotal;

    @Lob
    private String memo;

    @Column (name = "shipment_cost")
    private BigDecimal shipmentCost;

    @Column (name = "invoice_number")
    private String invoiceNumber;

    @Column (name = "purchase_order")
    private String purchaseOrder;


    @Column (name = "handling_cost")
    private BigDecimal handlingCost;

    @Column (name = "invoice_date")
    private Timestamp invoiceDate;

    @Column (name = "invoice_due_date")
    private Timestamp invoiceDueDate;

    @Column (name = "order_on_hold")
    private Boolean orderOnHold;

    @ManyToOne
    @JoinColumn (name = "discount_id")
    private Discount discount;

    @Column (name = "tax_amount")
    private BigDecimal taxAmount;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "payment_status_id")
    private Status paymentStatusId;

    @Column (name = "is_taxable")
    private Boolean isTaxable;

    //bi-directional many-to-one association to TaxRate
    @ManyToOne
    @JoinColumn (name = "id_tax_rate")
    private TaxRate taxRate;

    @Column (name = "paid_amount")
    private BigDecimal paidAmount;

    @Column (name = "web_order")
    private Boolean webOrder;

    @Column (name = "shipment_tracking_id")
    private String shipmentTrackingId;

    @Column (name = "shipment_tracking_url")
    private String shipmentTrackingUrl;

    //bi-directional many-to-one association to UserInfo
    @ManyToOne
    @JoinColumn (name = "approved_by")
    private UserInfo approvedBy;

    @Column (name = "id_device_type")
    private String idDeviceType;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "order_status")
    private Status orderStatus;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "shipping_address")
    private UserInfoAddress shippmentAddress;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "billing_address")
    private UserInfoAddress billingAddress;

    //bi-directional many-to-one association to Order
    @OneToMany (fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "order")
    private List<PaymentDetail> paymentDetail;

    //bi-directional many-to-one association to Order
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany (cascade = CascadeType.ALL, mappedBy = "order")
    private List<OrderDetail> orderDetailList;

    //bi-directional many-to-one association to Order
    @OneToOne (mappedBy = "order", cascade = CascadeType.ALL)
    private OrderVTB orderVTB;

    @Column (name = "discounted_subtotal")
    private BigDecimal discountedSubTotal;

    @Column (name = "discounted_shipping")
    private BigDecimal discountedShippingCost;

    @Column (name = "discounted_grand_total")
    private BigDecimal discountedGrandTotal;

    @Column (name = "discounted_tax")
    private BigDecimal discountedTaxAmount;

    //This field added for Order PO number which is optional
    @Column(name="order_po_number")
    private String orderPoNumber;

    private String reason;

    private String salon;

    //bi-directional one-to-one association to OrderAdditionalInformation
    @OneToOne (mappedBy = "order", cascade = CascadeType.ALL)
    private OrderAdditionalInformation orderAdditionalInformation;

    @Transient
    private String carrierName;

    @Transient
    private String trackingNumber;


    @Column(name = "edit_date_time")
    private Calendar lastEditTime;

    @Column(name = "make_inventory_impact")
    private boolean makeInventoryImpact;

    @Column(name = "order_payment_status")
    private Integer orderPaymentStatus;

    @Column(name = "tax_rate")
    private BigDecimal appliedTaxRate;

    @Column(name = "fedex_signature_type")
    private String fedexSignatureType;

    @Column(name = "editing_user")
    private String editingUser;

    private String completedBy;

    private Timestamp initiatedAt;

    @Column(name = "back_order_number")
    private String backOrderNumber;

    @Transient
    private PaymentResponseDTO paymentResponseDTO;

    @Column(name = "sales_type_id")
    private Integer salesTypeId;

    public int getOrderId () {
        return orderId;
    }

    public void setOrderId (final int orderId) {
        this.orderId = orderId;
    }

    public WareHouse getWarehouse () {
        return warehouse;
    }

    public void setWarehouse (final WareHouse warehouse) {
        this.warehouse = warehouse;
    }

    public Country getCountry () {
        return country;
    }

    public void setCountry (final Country country) {
        this.country = country;
    }

    public UserInfo getUser () {
        return user;
    }

    public void setUser (final UserInfo user) {
        this.user = user;
    }

    public Integer getTaxCertificate() {
        return taxCertificate;
    }

    public void setTaxCertificate(Integer taxCertificate) {
        this.taxCertificate = taxCertificate;
    }

    public Integer getTaxCertificateFile() {
        return taxCertificateFile;
    }

    public void setTaxCertificateFile(Integer taxCertificateFile) {
        this.taxCertificateFile = taxCertificateFile;
    }

    public UserInfo getCreatedBy () {
        return createdBy;
    }

    public void setCreatedBy (final UserInfo createdBy) {
        this.createdBy = createdBy;
    }

    public OrderShipmentType getOrderShipmentType () {
        return orderShipmentType;
    }

    public void setOrderShipmentType (final OrderShipmentType orderShipmentType) {
        this.orderShipmentType = orderShipmentType;
    }

    public OrderShipmentTerm getOrderShipmentTerm () {
        return orderShipmentTerm;
    }

    public void setOrderShipmentTerm (final OrderShipmentTerm orderShipmentTerm) {
        this.orderShipmentTerm = orderShipmentTerm;
    }

    public Date getOrderCreatedDate () {
        return orderCreatedDate;
    }

    public void setOrderCreatedDate (final Date orderCreatedDate) {
        this.orderCreatedDate = orderCreatedDate;
    }

    public Currency getFromCurrency () {
        return fromCurrency;
    }

    public void setFromCurrency (final Currency fromCurrency) {
        this.fromCurrency = fromCurrency;
    }

    public Currency getToCurrency () {
        return toCurrency;
    }

    public void setToCurrency (final Currency toCurrency) {
        this.toCurrency = toCurrency;
    }

    public BigDecimal getExchangeRate () {
        return exchangeRate;
    }

    public void setExchangeRate (final BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getOrderReferenceNumber () {
        return orderReferenceNumber;
    }

    public void setOrderReferenceNumber (final String orderReferenceNumber) {
        this.orderReferenceNumber = orderReferenceNumber;
    }

    public Date getRecollectionDate () {
        return recollectionDate;
    }

    public void setRecollectionDate (final Date recollectionDate) {
        this.recollectionDate = recollectionDate;
    }

    public String getShipmentAttachments () {
        return shipmentAttachments;
    }

    public void setShipmentAttachments (final String shipmentAttachments) {
        this.shipmentAttachments = shipmentAttachments;
    }

    public Carrier getCarrier () {
        return carrier;
    }

    public void setCarrier (final Carrier carrier) {
        this.carrier = carrier;
    }

    public CarrierService getCarrierService () {
        return carrierService;
    }

    public void setCarrierService (final CarrierService carrierService) {
        this.carrierService = carrierService;
    }

    public OrderShipVia getOrderShipVia () {
        return orderShipVia;
    }

    public void setOrderShipVia (final OrderShipVia orderShipVia) {
        this.orderShipVia = orderShipVia;
    }

    public BigDecimal getSubTotal () {
        return subTotal;
    }

    public void setSubTotal (final BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getGrandTotal () {
        return grandTotal;
    }

    public void setGrandTotal (final BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getMemo () {
        return memo;
    }

    public void setMemo (final String memo) {
        this.memo = memo;
    }

    public BigDecimal getShipmentCost () {
        return shipmentCost;
    }

    public void setShipmentCost (final BigDecimal shipmentCost) {
        this.shipmentCost = shipmentCost;
    }

    public String getInvoiceNumber () {
        return invoiceNumber;
    }

    public void setInvoiceNumber (final String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getPurchaseOrder () {
        return purchaseOrder;
    }

    public void setPurchaseOrder (final String purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public BigDecimal getHandlingCost () {
        return handlingCost;
    }

    public void setHandlingCost (final BigDecimal handlingCost) {
        this.handlingCost = handlingCost;
    }

    public Timestamp getInvoiceDate () {
        return invoiceDate;
    }

    public void setInvoiceDate (final Timestamp invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Timestamp getInvoiceDueDate () {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate (final Timestamp invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public Boolean getOrderOnHold () {
        return orderOnHold;
    }

    public void setOrderOnHold (final Boolean orderOnHold) {
        this.orderOnHold = orderOnHold;
    }

    public Discount getDiscount () {
        return discount;
    }

    public void setDiscount (final Discount discount) {
        this.discount = discount;
    }

    public BigDecimal getTaxAmount () {
        return taxAmount;
    }

    public void setTaxAmount (final BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Status getPaymentStatusId () {
        return paymentStatusId;
    }

    public void setPaymentStatusId (final Status paymentStatusId) {
        this.paymentStatusId = paymentStatusId;
    }

    public Boolean getIsTaxable () {
        return isTaxable;
    }

    public void setIsTaxable (final Boolean isTaxable) {
        this.isTaxable = isTaxable;
    }

    public TaxRate getTaxRate () {
        return taxRate;
    }

    public void setTaxRate (final TaxRate taxRate) {
        this.taxRate = taxRate;
    }

    public BigDecimal getPaidAmount () {
        return paidAmount;
    }

    public void setPaidAmount (final BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public Boolean getWebOrder () {
        return webOrder;
    }

    public void setWebOrder (final Boolean webOrder) {
        this.webOrder = webOrder;
    }

    public String getShipmentTrackingId () {
        return shipmentTrackingId;
    }

    public void setShipmentTrackingId (final String shipmentTrackingId) {
        this.shipmentTrackingId = shipmentTrackingId;
    }

    public String getShipmentTrackingUrl () {
        return shipmentTrackingUrl;
    }

    public void setShipmentTrackingUrl (final String shipmentTrackingUrl) {
        this.shipmentTrackingUrl = shipmentTrackingUrl;
    }

    public UserInfo getApprovedBy () {
        return approvedBy;
    }

    public void setApprovedBy (final UserInfo approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getIdDeviceType () {
        return idDeviceType;
    }

    public void setIdDeviceType (final String idDeviceType) {
        this.idDeviceType = idDeviceType;
    }

    public Status getOrderStatus () {
        return orderStatus;
    }

    public void setOrderStatus (final Status orderStatus) {
        this.orderStatus = orderStatus;
    }

    public UserInfoAddress getShippmentAddress () {
        return shippmentAddress;
    }

    public void setShippmentAddress (final UserInfoAddress shippmentAddress) {
        this.shippmentAddress = shippmentAddress;
    }

    public UserInfoAddress getBillingAddress () {
        return billingAddress;
    }

    public void setBillingAddress (final UserInfoAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<PaymentDetail> getPaymentDetail () {
        return paymentDetail;
    }

    public void setPaymentDetail (final List<PaymentDetail> paymentDetail) {
        this.paymentDetail = paymentDetail;
    }

    public List<OrderDetail> getOrderDetailList () {
        return orderDetailList;
    }

    public void setOrderDetailList (final List<OrderDetail> orderDetailList) {
        this.orderDetailList = orderDetailList;
    }

    public OrderVTB getOrderVTB () {
        return orderVTB;
    }

    public void setOrderVTB (final OrderVTB orderVTB) {
        this.orderVTB = orderVTB;
    }

    public BigDecimal getDiscountedSubTotal () {
        return discountedSubTotal;
    }

    public void setDiscountedSubTotal (final BigDecimal discountedSubTotal) {
        this.discountedSubTotal = discountedSubTotal;
    }

    public BigDecimal getDiscountedShippingCost () {
        return discountedShippingCost;
    }

    public void setDiscountedShippingCost (final BigDecimal discountedShippingCost) {
        this.discountedShippingCost = discountedShippingCost;
    }

    public BigDecimal getDiscountedGrandTotal () {
        return discountedGrandTotal;
    }

    public void setDiscountedGrandTotal (final BigDecimal discountedGrandTotal) {
        this.discountedGrandTotal = discountedGrandTotal;
    }

    public BigDecimal getDiscountedTaxAmount () {
        return discountedTaxAmount;
    }

    public void setDiscountedTaxAmount (final BigDecimal discountedTaxAmount) {
        this.discountedTaxAmount = discountedTaxAmount;
    }

    public String getOrderPoNumber() {
        return orderPoNumber;
    }

    public void setOrderPoNumber(String orderPoNumber) {
        this.orderPoNumber = orderPoNumber;
    }

    public String getReason() { return reason; }

    public void setReason(String reason) { this.reason = reason; }

    public String getCarrierName() { return carrierName; }

    public void setCarrierName(String carrierName) { this.carrierName = carrierName; }

    public String getTrackingNumber() { return trackingNumber; }

    public void setTrackingNumber(String trackingNumber) { this.trackingNumber = trackingNumber; }


    public Calendar getLastEditTime() {
        return lastEditTime;
    }

    public void setLastEditTime(Calendar lastEditTime) {
        this.lastEditTime = lastEditTime;
    }

    public boolean isMakeInventoryImpact() {
        return makeInventoryImpact;
    }

    public void setMakeInventoryImpact(boolean makeInventoryImpact) {
        this.makeInventoryImpact = makeInventoryImpact;
    }

    public Integer getOrderPaymentStatus() {
        return orderPaymentStatus;
    }

    public void setOrderPaymentStatus(Integer orderPaymentStatus) {
        this.orderPaymentStatus = orderPaymentStatus;
    }

    public BigDecimal getAppliedTaxRate() {
        if(this.appliedTaxRate == null){
            return BigDecimal.ZERO;
        }else{
            return this.appliedTaxRate;
        }
    }

    public void setAppliedTaxRate(BigDecimal appliedTaxRate) {
        if(appliedTaxRate == null){
            this.appliedTaxRate = BigDecimal.ZERO;
        }else{
            this.appliedTaxRate = appliedTaxRate;
        }}

    public String getFedexSignatureType() {
        return fedexSignatureType;
    }

    public void setFedexSignatureType(String fedexSignatureType) {
        this.fedexSignatureType = fedexSignatureType;
    }

    public String getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    public Timestamp getInitiatedAt() {
        return initiatedAt;
    }

    public void setInitiatedAt(Timestamp initiatedAt) {
        this.initiatedAt = initiatedAt;
    }

    public String getEditingUser() {
        return editingUser;
    }

    public void setEditingUser(String editingUser) {
        this.editingUser = editingUser;
    }


    public String getBackOrderNumber() {
        return backOrderNumber;
    }

    public void setBackOrderNumber(String backOrder) {
        this.backOrderNumber = backOrder;
    }
    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;

    }

    public OrderAdditionalInformation getOrderAdditionalInformation() {
        return orderAdditionalInformation;
    }

    public void setOrderAdditionalInformation(OrderAdditionalInformation orderAdditionalInformation) {
        this.orderAdditionalInformation = orderAdditionalInformation;
    }


    public PaymentResponseDTO getPaymentResponseDTO() {
        return paymentResponseDTO;
    }

    public void setPaymentResponseDTO(PaymentResponseDTO paymentResponseDTO) {
        this.paymentResponseDTO = paymentResponseDTO;
    }

    public Integer getSalesTypeId() {
        return salesTypeId;
    }

    public void setSalesTypeId(Integer salesTypeId) {
        this.salesTypeId = salesTypeId;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", warehouse=" + warehouse +
                ", country=" + country +
                ", user=" + user +
                ", taxCertificate=" + taxCertificate +
                ", taxCertificateFile=" + taxCertificateFile +
                ", createdBy=" + createdBy +
                ", orderShipmentType=" + orderShipmentType +
                ", orderShipmentTerm=" + orderShipmentTerm +
                ", orderCreatedDate=" + orderCreatedDate +
                ", fromCurrency=" + fromCurrency +
                ", toCurrency=" + toCurrency +
                ", exchangeRate=" + exchangeRate +
                ", orderReferenceNumber='" + orderReferenceNumber + '\'' +
                ", recollectionDate=" + recollectionDate +
                ", shipmentAttachments='" + shipmentAttachments + '\'' +
                ", carrier=" + carrier +
                ", carrierService=" + carrierService +
                ", orderShipVia=" + orderShipVia +
                ", subTotal=" + subTotal +
                ", grandTotal=" + grandTotal +
                ", memo='" + memo + '\'' +
                ", shipmentCost=" + shipmentCost +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", purchaseOrder='" + purchaseOrder + '\'' +
                ", handlingCost=" + handlingCost +
                ", invoiceDate=" + invoiceDate +
                ", invoiceDueDate=" + invoiceDueDate +
                ", orderOnHold=" + orderOnHold +
                ", discount=" + discount +
                ", taxAmount=" + taxAmount +
                ", paymentStatusId=" + paymentStatusId +
                ", isTaxable=" + isTaxable +
                ", taxRate=" + taxRate +
                ", paidAmount=" + paidAmount +
                ", webOrder=" + webOrder +
                ", shipmentTrackingId='" + shipmentTrackingId + '\'' +
                ", shipmentTrackingUrl='" + shipmentTrackingUrl + '\'' +
                ", approvedBy=" + approvedBy +
                ", idDeviceType='" + idDeviceType + '\'' +
                ", orderStatus=" + orderStatus +
                ", shippmentAddress=" + shippmentAddress +
                ", billingAddress=" + billingAddress +
                ", paymentDetail=" + paymentDetail +
                ", orderDetailList=" + orderDetailList +
                ", orderVTB=" + orderVTB +
                ", discountedSubTotal=" + discountedSubTotal +
                ", discountedShippingCost=" + discountedShippingCost +
                ", discountedGrandTotal=" + discountedGrandTotal +
                ", discountedTaxAmount=" + discountedTaxAmount +
                ", orderPoNumber='" + orderPoNumber + '\'' +
                ", reason='" + reason + '\'' +
                ", salon='" + salon + '\'' +
                ", orderAdditionalInformation=" + orderAdditionalInformation +
                ", carrierName='" + carrierName + '\'' +
                ", trackingNumber='" + trackingNumber + '\'' +
                ", lastEditTime=" + lastEditTime +
                ", makeInventoryImpact=" + makeInventoryImpact +
                ", orderPaymentStatus=" + orderPaymentStatus +
                ", appliedTaxRate=" + appliedTaxRate +
                ", fedexSignatureType='" + fedexSignatureType + '\'' +
                ", editingUser='" + editingUser + '\'' +
                ", completedBy='" + completedBy + '\'' +
                ", initiatedAt=" + initiatedAt +
                ", backOrderNumber='" + backOrderNumber + '\'' +
                ", salesTypeId=" + salesTypeId +
                '}';
    }
}

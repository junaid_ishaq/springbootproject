package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "tax_rates")
public class TaxRate implements Serializable {


    private static final long serialVersionUID = 6746414128844338181L;


    @Id
    private int id;

    @Lob
    private String description;

    private String identifier;

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "last_update")
    private Date lastUpdate;

    private BigDecimal rate;

    @Column (name = "zip_code")
    private String zipCode;

    @Column (name = "zip_code_end")
    private String zipCodeEnd;

    @Column (name = "zip_code_start")
    private String zipCodeStart;


    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "id_country")
    private Country country;

    //bi-directional many-to-one association to CountryState
    @ManyToOne
    @JoinColumn (name = "id_country_state")
    private CountryState countryState;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "user_type")
    private UserType userType;


    public TaxRate () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getIdentifier () {
        return this.identifier;
    }

    public void setIdentifier (String identifier) {
        this.identifier = identifier;
    }

    public Date getLastUpdate () {
        return this.lastUpdate;
    }

    public void setLastUpdate (Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public BigDecimal getRate () {
        return this.rate;
    }

    public void setRate (BigDecimal rate) {
        this.rate = rate;
    }

    public String getZipCode () {
        return this.zipCode;
    }

    public void setZipCode (String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipCodeEnd () {
        return this.zipCodeEnd;
    }

    public void setZipCodeEnd (String zipCodeEnd) {
        this.zipCodeEnd = zipCodeEnd;
    }

    public String getZipCodeStart () {
        return this.zipCodeStart;
    }

    public void setZipCodeStart (String zipCodeStart) {
        this.zipCodeStart = zipCodeStart;
    }


    public Country getCountry () {
        return this.country;
    }

    public void setCountry (Country country) {
        this.country = country;
    }

    public CountryState getCountryState () {
        return this.countryState;
    }

    public void setCountryState (CountryState countryState) {
        this.countryState = countryState;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }
}

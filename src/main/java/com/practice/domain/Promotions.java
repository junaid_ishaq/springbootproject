package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

@Entity
@Table(name = "promotion")
public class Promotions implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic (optional = false)
    @Column (name = "id")
    private Integer id;

    @JoinColumn (name = "promotion_type_id", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.LAZY)
    private PromotionType promotionTypeId;

    @Column (name = "name")
    @Basic (optional = false)
    private String name;

    @Column (name = "image_link")
    private String imageLink;

    @Column (name = "description")
    private String description;

    @JoinColumn (name = "country_id", referencedColumnName = "id_country")
    @ManyToOne (fetch = FetchType.LAZY)
    private Country country;

    @JoinColumn (name = "user_type_id", referencedColumnName = "type")
    @ManyToOne (fetch = FetchType.LAZY)
    private UserType userType;

    @JoinColumn (name = "status", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.LAZY)
    private Status status;

    @Column (name = "start_date")
    private Calendar startDate;

    @Column (name = "end_date")
    private Calendar endDate;

    @Column (name = "buyChoiceOneMandatoryProducts")
    private Integer buyChoiceOneMandatoryProducts;

    @Column (name = "buyChoiceTwoMandatoryProducts")
    private Integer buyChoiceTwoMandatoryProducts;

    @Column (name = "buyChoiceThreeMandatoryProducts")
    private Integer buyChoiceThreeMandatoryProducts;

    @Column (name = "getChoiceOneMandatoryProducts")
    private Integer getChoiceOneMandatoryProducts;

    @Column (name = "getChoiceTwoMandatoryProducts")
    private Integer getChoiceTwoMandatoryProducts;

    @Column (name = "getChoiceThreeMandatoryProducts")
    private Integer getChoiceThreeMandatoryProducts;

    @Column (name = "onlyForVTB")
    private boolean onlyForSaleRep;

    @Column(name = "itemcode")
    private String itemCode;

    @OneToMany (mappedBy = "promotions")
    List<PromotionProduct> promotionProducts;

    @Column (name = "maxQuantityToOrder")
    private Integer maxQuantityToOrder;

    public Integer getId () {
        return id;
    }

    public void setId (final Integer id) {
        this.id = id;
    }

    public PromotionType getPromotionTypeId () {
        return promotionTypeId;
    }

    public void setPromotionTypeId (final PromotionType promotionTypeId) {
        this.promotionTypeId = promotionTypeId;
    }

    public String getName () {
        return name;
    }

    public void setName (final String name) {
        this.name = name;
    }

    public UserType getUserType () {
        return userType;
    }

    public void setUserType (final UserType userType) {
        this.userType = userType;
    }

    public Country getCountry () {
        return country;
    }

    public void setCountry (final Country country) {
        this.country = country;
    }

    public Status getStatus () {
        return status;
    }

    public void setStatus (final Status status) {
        this.status = status;
    }

    public String getImageLink () {
        return imageLink;
    }

    public void setImageLink (final String imageLink) {
        this.imageLink = imageLink;
    }

    public String getDescription () {
        return description;
    }

    public void setDescription (final String description) {
        this.description = description;
    }

    public Calendar getStartDate () {
        return startDate;
    }

    public void setStartDate (final Calendar startDate) {
        this.startDate = startDate;
    }

    public Calendar getEndDate () {
        return endDate;
    }

    public void setEndDate (final Calendar endDate) {
        this.endDate = endDate;
    }

    public Integer getBuyChoiceOneMandatoryProducts () {
        return buyChoiceOneMandatoryProducts;
    }

    public void setBuyChoiceOneMandatoryProducts (final Integer buyChoiceOneMandatoryProducts) {
        this.buyChoiceOneMandatoryProducts = buyChoiceOneMandatoryProducts;
    }

    public Integer getBuyChoiceTwoMandatoryProducts () {
        return buyChoiceTwoMandatoryProducts;
    }

    public void setBuyChoiceTwoMandatoryProducts (final Integer buyChoiceTwoMandatoryProducts) {
        this.buyChoiceTwoMandatoryProducts = buyChoiceTwoMandatoryProducts;
    }

    public Integer getBuyChoiceThreeMandatoryProducts () {
        return buyChoiceThreeMandatoryProducts;
    }

    public void setBuyChoiceThreeMandatoryProducts (final Integer buyChoiceThreeMandatoryProducts) {
        this.buyChoiceThreeMandatoryProducts = buyChoiceThreeMandatoryProducts;
    }

    public Integer getGetChoiceOneMandatoryProducts () {
        return getChoiceOneMandatoryProducts;
    }

    public void setGetChoiceOneMandatoryProducts (final Integer getChoiceOneMandatoryProducts) {
        this.getChoiceOneMandatoryProducts = getChoiceOneMandatoryProducts;
    }

    public Integer getGetChoiceTwoMandatoryProducts () {
        return getChoiceTwoMandatoryProducts;
    }

    public void setGetChoiceTwoMandatoryProducts (final Integer getChoiceTwoMandatoryProducts) {
        this.getChoiceTwoMandatoryProducts = getChoiceTwoMandatoryProducts;
    }

    public Integer getGetChoiceThreeMandatoryProducts () {
        return getChoiceThreeMandatoryProducts;
    }

    public void setGetChoiceThreeMandatoryProducts (final Integer getChoiceThreeMandatoryProducts) {
        this.getChoiceThreeMandatoryProducts = getChoiceThreeMandatoryProducts;
    }

    public List<PromotionProduct> getPromotionProducts () {
        return promotionProducts;
    }

    public void setPromotionProducts (final List<PromotionProduct> promotionProducts) {
        this.promotionProducts = promotionProducts;
    }

    public boolean isOnlyForSaleRep() {
        return onlyForSaleRep;
    }

    public void setOnlyForSaleRep(boolean onlyForSaleRep) {
        this.onlyForSaleRep = onlyForSaleRep;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Integer getMaxQuantityToOrder() {
        return maxQuantityToOrder;
    }

    public void setMaxQuantityToOrder(Integer maxQuantityToOrder) {
        this.maxQuantityToOrder = maxQuantityToOrder;
    }

}

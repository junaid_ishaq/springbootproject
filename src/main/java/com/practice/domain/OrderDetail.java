package com.practice.domain;

import com.practice.enums.ProductStatusEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import static java.math.BigDecimal.ZERO;

@Entity
@Table(name = "order_detail")
public class OrderDetail implements Serializable,Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic (optional = false)
    @Column (name = "id")
    private Integer id;

    @Column (name = "available_quantity_to_return")
    private Integer availableQuantityToReturn;

    @Column (name = "available_stock_products")
    private Integer availableStockProducts;

    @Column (name = "discount_percent")
    private BigDecimal discountPercent;

    @Column (name = "free_item")
    private Boolean    freeItem;

    @ManyToOne
    @JoinColumn (name = "id_promotion")
    private Promotions idPromotion;

    @ManyToOne
    @JoinColumn (name = "promotion_visibility_id", referencedColumnName = "id")
    private PromotionVisibility promotionVisibility;

    @Lob
    private String memo;

    private BigDecimal price;

    @Column (name = "price_level")
    private BigDecimal priceLevel;

    private Integer quantity;

    @Column (name = "quantity_damaged_products")
    private Integer quantityDamagedProducts;

    //bi-directional many-to-one association to Order
    @ManyToOne
    @JoinColumn (name = "id_order")
    private Order order;

    @OneToMany (mappedBy = "orderDetail", cascade = CascadeType.ALL)
    private List<OrderDetailPromotion> orderDetailPromotions;

    @Column (name = "discount_on_item")
    private BigDecimal discountOnItem;

    @Column (name = "is_discount_applied")
    @Basic (optional = false)
    private Boolean isDiscountApplied;

    @Column(name ="id_subpromotion")
    private Integer idSubPromotion;

    @Column(name = "free_quantity")
    private Integer freeItemsQuantity;

    @ManyToOne
    @JoinColumn(name = "product_status")
    private Status productStatus;

    private String addedBy;

    @Column(name = "is_marketing")
    private boolean isMarketing;

    private String reason;


    public OrderDetail () {
    }

    public Integer getId () {
        return this.id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public Integer getAvailableQuantityToReturn () {
        return this.availableQuantityToReturn;
    }

    public void setAvailableQuantityToReturn (Integer availableQuantityToReturn) {
        this.availableQuantityToReturn = availableQuantityToReturn;
    }

    public Integer getAvailableStockProducts () {
        return this.availableStockProducts;
    }

    public void setAvailableStockProducts (Integer availableStockProducts) {
        this.availableStockProducts = availableStockProducts;
    }

    public BigDecimal getDiscountPercent () {
        return this.discountPercent;
    }

    public void setDiscountPercent (BigDecimal discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Boolean getFreeItem () {
        return this.freeItem;
    }

    public void setFreeItem (Boolean freeItem) {
        this.freeItem = freeItem;
    }

    public Promotions getIdPromotion () {
        return idPromotion;
    }

    public void setIdPromotion (final Promotions idPromotion) {
        this.idPromotion = idPromotion;
    }

    public String getMemo () {
        return this.memo;
    }

    public void setMemo (String memo) {
        this.memo = memo;
    }

    public BigDecimal getPrice () {
        return this.price;
    }

    public void setPrice (BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceLevel () {
        return this.priceLevel;
    }

    public void setPriceLevel (BigDecimal priceLevel) {
        this.priceLevel = priceLevel;
    }

    public Integer getQuantity () {
        return this.quantity;
    }

    public void setQuantity (Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantityDamagedProducts () {
        return this.quantityDamagedProducts;
    }

    public void setQuantityDamagedProducts (Integer quantityDamagedProducts) {
        this.quantityDamagedProducts = quantityDamagedProducts;
    }

    public Order getOrder () {
        return this.order;
    }

    public void setOrder (Order order) {
        this.order = order;
    }

    public BigDecimal getDiscountOnItem () {
        return Objects.isNull(discountOnItem) ? ZERO : discountOnItem;
    }

    public void setDiscountOnItem (final BigDecimal discountOnItem) {
        this.discountOnItem = discountOnItem;
    }

    public Boolean isDiscountApplied () {
        return isDiscountApplied;
    }

    public void setIsDiscountApplied (final Boolean isDiscountApplied) {
        this.isDiscountApplied = isDiscountApplied;
    }

    public List<OrderDetailPromotion> getOrderDetailPromotions () {
        return orderDetailPromotions;
    }

    public void setOrderDetailPromotions (final List<OrderDetailPromotion> orderDetailPromotions) {
        this.orderDetailPromotions = orderDetailPromotions;
    }

    public Integer getIdSubPromotion () {
        return idSubPromotion;
    }

    public void setIdSubPromotion (final Integer idSubPromotion) {
        this.idSubPromotion = idSubPromotion;
    }

    public Integer getFreeItemsQuantity() {
        return freeItemsQuantity;
    }

    public void setFreeItemsQuantity(Integer freeItemsQuantity) {
        this.freeItemsQuantity = (freeItemsQuantity == null || freeItemsQuantity.intValue() < 0)? 0 : freeItemsQuantity;
    }

    public Status getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(Status productStatus) {
        this.productStatus = productStatus != null ? productStatus : new Status(ProductStatusEnum.REGULAR.getId());
    }

    public PromotionVisibility getPromotionVisibility() {
        return promotionVisibility;
    }

    public void setPromotionVisibility(PromotionVisibility promotionVisibility) {
        this.promotionVisibility = promotionVisibility;
    }


    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public boolean isMarketing() {
        return isMarketing;
    }

    public void setMarketing(boolean marketing) {
        isMarketing = marketing;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public OrderDetail clone() throws CloneNotSupportedException {
        try
        {
            OrderDetail clonedMyClass = (OrderDetail)super.clone();
            // if you have custom object, then you need create a new one in here
            return clonedMyClass ;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return new OrderDetail();
        }

    }

}

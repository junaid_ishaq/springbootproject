package com.practice.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ProductGroupPricePerUserPK implements Serializable {
    //default serial version id, required for serializable classes.
    private static final long serialVersionUID = 1L;

    @Column(name = "product_group", insertable = false, updatable = false)
    private int productGroup;

    @Column (name = "user_name", insertable = false, updatable = false)
    private String userName;

    public ProductGroupPricePerUserPK () {
    }

    public int getProductGroup () {
        return this.productGroup;
    }

    public void setProductGroup (int productGroup) {
        this.productGroup = productGroup;
    }

    public String getUserName () {
        return this.userName;
    }

    public void setUserName (String userName) {
        this.userName = userName;
    }

    public boolean equals (Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ProductGroupPricePerUserPK)) {
            return false;
        }
        ProductGroupPricePerUserPK castOther = (ProductGroupPricePerUserPK) other;
        return (this.productGroup == castOther.productGroup) && this.userName.equals(castOther.userName);
    }

    public int hashCode () {
        final int prime = 31;
        int       hash  = 17;
        hash = hash * prime + this.productGroup;
        hash = hash * prime + this.userName.hashCode();

        return hash;
    }
}

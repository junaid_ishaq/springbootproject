package com.practice.domain;


import javax.persistence.*;

@Entity
@Table(name = "userinfo_address")
public class UserInfoAddress {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_userinfo_address")
    private Integer idUserInfoAddress;

    @Lob
    private String address;

    @Lob
    private String address2;

    @Lob
    private String address3;


    private String city;

    @Column(name = "default_address")
    private boolean defaultAddress;

    private String email;

    @Column(name = "full_name")
    private String fullName;

    private String phone;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "state_province")
    private String stateProvince;

    @Column(name = "address_title")
    private String addressTitle;

    @ManyToOne
    @JoinColumn(name = "user_name")
    private UserInfo userInfo;

    @ManyToOne
    @JoinColumn(name = "address_type")
    private AddressType addressTypeBean;

    @ManyToOne
    @JoinColumn(name = "id_country")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "state")
    private CountryState countryState;

    @ManyToOne
    @JoinColumn(name = "status")
    private Status statusBean;

    public Integer getIdUserInfoAddress() {
        return idUserInfoAddress;
    }

    public void setIdUserInfoAddress(Integer idUserInfoAddress) {
        this.idUserInfoAddress = idUserInfoAddress;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean isDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStateProvince() {
        return stateProvince;
    }

    public void setStateProvince(String stateProvince) {
        this.stateProvince = stateProvince;
    }

    public String getAddressTitle() {
        return addressTitle;
    }

    public void setAddressTitle(String addressTitle) {
        this.addressTitle = addressTitle;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public AddressType getAddressTypeBean() {
        return addressTypeBean;
    }

    public void setAddressTypeBean(AddressType addressTypeBean) {
        this.addressTypeBean = addressTypeBean;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public CountryState getCountryState() {
        return countryState;
    }

    public void setCountryState(CountryState countryState) {
        this.countryState = countryState;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }
}

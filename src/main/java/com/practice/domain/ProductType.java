package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "product_type")
public class ProductType implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    private String name;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn(name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to StockProduct
    @OneToMany (mappedBy = "productTypeBean")
    private List<StockProduct> stockProducts;

    public ProductType () {
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<StockProduct> getStockProducts () {
        return this.stockProducts;
    }

    public void setStockProducts (List<StockProduct> stockProducts) {
        this.stockProducts = stockProducts;
    }

    public StockProduct addStockProduct (StockProduct stockProduct) {
        getStockProducts().add(stockProduct);
        stockProduct.setProductTypeBean(this);

        return stockProduct;
    }

    public StockProduct removeStockProduct (StockProduct stockProduct) {
        getStockProducts().remove(stockProduct);
        stockProduct.setProductTypeBean(null);

        return stockProduct;
    }

}

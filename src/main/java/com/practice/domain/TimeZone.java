package com.practice.domain;

import com.practice.domain.CountryState;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the time_zone database table.
 */
@Entity
@Table (name = "time_zone")
public class TimeZone implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column (name = "time_zone_id")
    private int timeZoneId;

    @Column (name = "country_code")
    private String countryCode;

    @Column (name = "time_zone_name")
    private String timeZoneName;

    //bi-directional many-to-one association to CountryState
    @OneToMany (mappedBy = "timeZoneBean")
    private List<CountryState> countryStates;

    public TimeZone () {
    }

    public int getTimeZoneId () {
        return this.timeZoneId;
    }

    public void setTimeZoneId (int timeZoneId) {
        this.timeZoneId = timeZoneId;
    }

    public String getCountryCode () {
        return this.countryCode;
    }

    public void setCountryCode (String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTimeZoneName () {
        return this.timeZoneName;
    }

    public void setTimeZoneName (String timeZoneName) {
        this.timeZoneName = timeZoneName;
    }

    public List<CountryState> getCountryStates () {
        return this.countryStates;
    }

    public void setCountryStates (List<CountryState> countryStates) {
        this.countryStates = countryStates;
    }

    public CountryState addCountryState (CountryState countryState) {
        getCountryStates().add(countryState);
        countryState.setTimeZoneBean(this);

        return countryState;
    }

    public CountryState removeCountryState (CountryState countryState) {
        getCountryStates().remove(countryState);
        countryState.setTimeZoneBean(null);

        return countryState;
    }

}

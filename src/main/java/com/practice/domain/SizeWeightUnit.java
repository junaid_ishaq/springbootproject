package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "size_weight_unit")
public class SizeWeightUnit implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private int id;

    private String description;

    private String value;

    //bi-directional many-to-one association to ProductSize
    @OneToMany(mappedBy = "sizeWeightUnit")
    private List<ProductSize> productSizes;

    //bi-directional many-to-one association to ProductWeight
    @OneToMany (mappedBy = "sizeWeightUnit")
    private List<ProductWeight> productWeights;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public SizeWeightUnit () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getValue () {
        return this.value;
    }

    public void setValue (String value) {
        this.value = value;
    }

    public List<ProductSize> getProductSizes () {
        return this.productSizes;
    }

    public void setProductSizes (List<ProductSize> productSizes) {
        this.productSizes = productSizes;
    }

    public ProductSize addProductSize (ProductSize productSize) {
        getProductSizes().add(productSize);
        productSize.setSizeWeightUnit(this);

        return productSize;
    }

    public ProductSize removeProductSize (ProductSize productSize) {
        getProductSizes().remove(productSize);
        productSize.setSizeWeightUnit(null);

        return productSize;
    }

    public List<ProductWeight> getProductWeights () {
        return this.productWeights;
    }

    public void setProductWeights (List<ProductWeight> productWeights) {
        this.productWeights = productWeights;
    }

    public ProductWeight addProductWeight (ProductWeight productWeight) {
        getProductWeights().add(productWeight);
        productWeight.setSizeWeightUnit(this);

        return productWeight;
    }

    public ProductWeight removeProductWeight (ProductWeight productWeight) {
        getProductWeights().remove(productWeight);
        productWeight.setSizeWeightUnit(null);

        return productWeight;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

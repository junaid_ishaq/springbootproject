package com.practice.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the stock_product_size database table.
 */
@Entity
@Table (name = "stock_product_size")
public class StockProductSize implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Integer id;

    private String hts;

    private String upc;

    private String model;

    @Column (name = "item_code")
    private String itemCode;

    @Column (name = "is_default")
    private Boolean isDefault;

    @Column (name = "items_per_box")
    private int itemsPerBox;

    @Lob
    @Column (name = "thumbnail_image")
    private String thumbnailImage;

    @Column (name = "splash_image")
    private String splashImage;

    @Column (name = "color")
    private String color;


    //bi-directional many-to-one association to StockProductComponent
    @OneToMany (mappedBy = "stockProductSize2")
    private List<StockProductComponent> stockProductComponents2;

    //bi-directional many-to-one association to StockProductDetail
    @OneToMany (mappedBy = "stockProductSize")
    private List<StockProductDetail> stockProductDetails;

    //bi-directional many-to-one association to StockProductDetail
    @OneToMany (mappedBy = "stockProductSize3")
    private List<TempOrder> tempOrderList;

    //bi-directional many-to-one association to StockProduct
    @ManyToOne
    @JoinColumn (name = "assembly")
    private StockProduct stockProduct;

    //bi-directional many-to-one association to ProductSize
    @ManyToOne
    @JoinColumn (name = "id_product_size")
    private ProductSize productSize;

    //bi-directional many-to-one association to ProductWeight
    @ManyToOne
    @JoinColumn (name = "weight")
    private ProductWeight productWeight;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public StockProductSize () {
    }

    public Integer getId () {
        return this.id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public Boolean getIsDefault () {
        return this.isDefault;
    }

    public void setIsDefault (Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public int getItemsPerBox () {
        return this.itemsPerBox;
    }

    public void setItemsPerBox (int itemsPerBox) {
        this.itemsPerBox = itemsPerBox;
    }

    public String getThumbnailImage () {
        return this.thumbnailImage;
    }

    public void setThumbnailImage (String thumbnailImage) {
        this.thumbnailImage = thumbnailImage;
    }

    public StockProduct getStockProduct () {
        return this.stockProduct;
    }

    public void setStockProduct (StockProduct stockProduct) {
        this.stockProduct = stockProduct;
    }

    public ProductSize getProductSize () {
        return this.productSize;
    }

    public void setProductSize (ProductSize productSize) {
        this.productSize = productSize;
    }

    public ProductWeight getProductWeight () {
        return this.productWeight;
    }

    public void setProductWeight (ProductWeight productWeight) {
        this.productWeight = productWeight;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public String getColor () {
        return color;
    }

    public void setColor (final String color) {
        this.color = color;
    }

    public String getHts () {
        return hts;
    }

    public void setHts (final String hts) {
        this.hts = hts;
    }

    public String getUpc () {
        return upc;
    }

    public String getModel () {
        return model;
    }

    public void setModel (final String model) {
        this.model = model;
    }

    public String getItemCode () {
        return itemCode;
    }

    public void setItemCode (final String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSplashImage() {
        return splashImage;
    }

    public void setSplashImage(String splashImage) {
        this.splashImage = splashImage;
    }
}

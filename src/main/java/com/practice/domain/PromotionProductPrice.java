package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "promotion_product_price")
public class PromotionProductPrice implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Integer id;

    @JoinColumn (name = "promotion_visibility_id", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.LAZY)
    private PromotionVisibility promotionVisibility;

    @JoinColumn (name = "promotion_product_id", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.LAZY)
    private PromotionProduct promotionProduct;

    @Column (name = "price")
    private BigDecimal price;

    @Column (name = "costPrice")
    private BigDecimal costPrice;

    public Integer getId () {
        return id;
    }

    public void setId (final Integer id) {
        this.id = id;
    }

    public BigDecimal getPrice () {
        return price;
    }

    public void setPrice (final BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getCostPrice () {
        return costPrice;
    }

    public void setCostPrice (final BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public PromotionVisibility getPromotionVisibility() {
        return promotionVisibility;
    }

    public void setPromotionVisibility(PromotionVisibility promotionVisibility) {
        this.promotionVisibility = promotionVisibility;
    }

    public PromotionProduct getPromotionProduct() {
        return promotionProduct;
    }

    public void setPromotionProduct(PromotionProduct promotionProduct) {
        this.promotionProduct = promotionProduct;
    }
}


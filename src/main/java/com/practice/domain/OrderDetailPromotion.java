package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "order_detail_promotion")
public class OrderDetailPromotion implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6902342615436478148L;


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn (name = "order_detail")
    private OrderDetail orderDetail;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "unit_price")
    private BigDecimal unitPrice;

    @Column(name = "cost_price")
    private BigDecimal costPrice;

    @Column(name = "product_category")
    private String productCategory;

    @Column(name = "promotion_detail_id")
    private Integer promotionDetailId;


    public Integer getId () {
        return id;
    }

    public void setId (final Integer id) {
        this.id = id;
    }

    public OrderDetail getOrderDetail () {
        return orderDetail;
    }

    public void setOrderDetail (final OrderDetail orderDetail) {
        this.orderDetail = orderDetail;
    }

    public Integer getQuantity () {
        return quantity;
    }

    public void setQuantity (final Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice () {
        return unitPrice;
    }

    public void setUnitPrice (final BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public BigDecimal getCostPrice () {
        return costPrice;
    }

    public void setCostPrice (final BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public String getProductCategory () {
        return productCategory;
    }

    public void setProductCategory (final String productCategory) {
        this.productCategory = productCategory;
    }

    public Integer getPromotionDetailId () {
        return promotionDetailId;
    }

    public void setPromotionDetailId (final Integer promotionDetailId) {
        this.promotionDetailId = promotionDetailId;
    }
}

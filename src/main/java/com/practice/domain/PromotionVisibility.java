package com.practice.domain;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "promotion_visibility")
public class PromotionVisibility implements Serializable {

    /**
     *
     */
    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic (optional = false)
    @Column (name = "id")
    private Integer id;

    @JoinColumn (name = "country_id", referencedColumnName = "id_country")
    @ManyToOne (fetch = FetchType.LAZY)
    private Country country;

    @JoinColumn (name = "user_type_id", referencedColumnName = "type")
    @ManyToOne (fetch = FetchType.LAZY)
    private UserType userType;

    @JoinColumn (name = "status", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.LAZY)
    private Status status;

    @Column (name = "promotion_price")
    private BigDecimal promotionPrice;

    @Column (name = "buy_price")
    private BigDecimal buyPrice;

    @JoinColumn (name = "promotion_id", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.LAZY)
    private Promotions promotions;

    @JoinColumn (name = "user_name", referencedColumnName = "user_name")
    @ManyToOne (fetch = FetchType.LAZY)
    private UserInfo userName;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany (mappedBy = "promotionVisibility", cascade = CascadeType.ALL)
    List<PromotionProductPrice> promotionProductPrice;

    public Integer getId () {
        return id;
    }

    public void setId (final Integer id) {
        this.id = id;
    }

    public UserType getUserType () {
        return userType;
    }

    public void setUserType (final UserType userType) {
        this.userType = userType;
    }

    public Country getCountry () {
        return country;
    }

    public void setCountry (final Country country) {
        this.country = country;
    }

    public Status getStatus () {
        return status;
    }

    public void setStatus (final Status status) {
        this.status = status;
    }

    public BigDecimal getPromotionPrice () {
        return promotionPrice;
    }

    public void setPromotionPrice (final BigDecimal promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public BigDecimal getBuyPrice () {
        return buyPrice;
    }

    public void setBuyPrice (final BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Promotions getPromotions () {
        return promotions;
    }

    public void setPromotions (final Promotions promotions) {
        this.promotions = promotions;
    }

    public UserInfo getUserName () {
        return userName;
    }

    public void setUserName (UserInfo userName) {
        this.userName = userName;
    }

    public List<PromotionProductPrice> getPromotionProductPrice() {
        return promotionProductPrice;
    }

    public void setPromotionProductPrice(List<PromotionProductPrice> promotionProductPrice) {
        this.promotionProductPrice = promotionProductPrice;
    }
}


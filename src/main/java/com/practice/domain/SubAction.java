package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sub_action")
public class SubAction implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_sub_action")
    private int idSubAction;

    private String name;


    //bi-directional many-to-one association to Action
    @ManyToOne
    @JoinColumn (name = "id_action")
    private Action action;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;


    public SubAction () {
    }

    public int getIdSubAction () {
        return this.idSubAction;
    }

    public void setIdSubAction (int idSubAction) {
        this.idSubAction = idSubAction;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }


    public Action getAction () {
        return this.action;
    }

    public void setAction (Action action) {
        this.action = action;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }


}

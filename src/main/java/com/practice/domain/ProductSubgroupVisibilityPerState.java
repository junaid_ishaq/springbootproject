package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product_subgroup_visibility_per_state")
public class ProductSubgroupVisibilityPerState implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    //bi-directional many-to-one association to ProductSubgroup
    @ManyToOne
    @JoinColumn(name = "product_subgroup_id", insertable = false, updatable = false)
    private ProductSubgroup productSubgroup;

    //bi-directional many-to-one association to CountryState
    @ManyToOne
    @JoinColumn (name = "id_state", insertable = false, updatable = false)
    private CountryState countryState;

    //bi-directional many-to-one association to UserType
    @ManyToOne
    @JoinColumn (name = "user_type", insertable = false, updatable = false)
    private UserType userTypeBean;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public ProductSubgroupVisibilityPerState () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public ProductSubgroup getProductSubgroup () {
        return this.productSubgroup;
    }

    public void setProductSubgroup (ProductSubgroup productSubgroup) {
        this.productSubgroup = productSubgroup;
    }

    public CountryState getCountryState () {
        return this.countryState;
    }

    public void setCountryState (CountryState countryState) {
        this.countryState = countryState;
    }

    public UserType getUserTypeBean () {
        return this.userTypeBean;
    }

    public void setUserTypeBean (UserType userTypeBean) {
        this.userTypeBean = userTypeBean;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

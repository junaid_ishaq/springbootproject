package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "payment_gateway_for_country_states")
public class PaymentGatewayForCountryState implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    //bi-directional many-to-one association to PaymentGatewayDetail
    @ManyToOne
    @JoinColumn(name = "gateway_id")
    private PaymentGatewayDetail paymentGateway;

    //bi-directional many-to-one association to Module
    @ManyToOne
    @JoinColumn (name = "module")
    private Module moduleBean;

    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "country")
    private Country countryBean;

    //bi-directional many-to-one association to CountryState
    @ManyToOne
    @JoinColumn (name = "state")
    private CountryState countryState;

    //bi-directional many-to-one association to Subsidiary
    @ManyToOne
    @JoinColumn (name = "subsidiary")
    private Subsidiary subsidiaryBean;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PaymentGatewayDetail getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(PaymentGatewayDetail paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public Module getModuleBean() {
        return moduleBean;
    }

    public void setModuleBean(Module moduleBean) {
        this.moduleBean = moduleBean;
    }

    public Country getCountryBean() {
        return countryBean;
    }

    public void setCountryBean(Country countryBean) {
        this.countryBean = countryBean;
    }

    public CountryState getCountryState() {
        return countryState;
    }

    public void setCountryState(CountryState countryState) {
        this.countryState = countryState;
    }

    public Subsidiary getSubsidiaryBean() {
        return subsidiaryBean;
    }

    public void setSubsidiaryBean(Subsidiary subsidiaryBean) {
        this.subsidiaryBean = subsidiaryBean;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PaymentGatewayForCountryState that = (PaymentGatewayForCountryState) o;

        if (id != that.id) return false;
        if (paymentGateway != null ? !paymentGateway.equals(that.paymentGateway) : that.paymentGateway != null)
            return false;
        if (moduleBean != null ? !moduleBean.equals(that.moduleBean) : that.moduleBean != null) return false;
        if (countryBean != null ? !countryBean.equals(that.countryBean) : that.countryBean != null) return false;
        if (countryState != null ? !countryState.equals(that.countryState) : that.countryState != null) return false;
        if (subsidiaryBean != null ? !subsidiaryBean.equals(that.subsidiaryBean) : that.subsidiaryBean != null)
            return false;
        return status != null ? status.equals(that.status) : that.status == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (paymentGateway != null ? paymentGateway.hashCode() : 0);
        result = 31 * result + (moduleBean != null ? moduleBean.hashCode() : 0);
        result = 31 * result + (countryBean != null ? countryBean.hashCode() : 0);
        result = 31 * result + (countryState != null ? countryState.hashCode() : 0);
        result = 31 * result + (subsidiaryBean != null ? subsidiaryBean.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PaymentGatewayForCountryState{" +
                "id=" + id +
                ", paymentGateway=" + paymentGateway +
                ", moduleBean=" + moduleBean +
                ", countryBean=" + countryBean +
                ", countryState=" + countryState +
                ", subsidiaryBean=" + subsidiaryBean +
                ", status=" + status +
                '}';
    }
}

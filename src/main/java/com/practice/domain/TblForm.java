package com.practice.domain;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbl_form")
public class TblForm {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tblform")
    private int idTblform;

    @Lob
    @Column (name = "form_description")
    private String formDescription;

    @Column (name = "form_name")
    private String formName;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to Module
    @ManyToOne
    @JoinColumn (name = "id_module")
    private Module module;

    //bi-directional many-to-one association to TblFormField
    @OneToMany (mappedBy = "tblForm")
    private List<TblFormField> tblFormFields;

    public int getIdTblform() {
        return idTblform;
    }

    public void setIdTblform(int idTblform) {
        this.idTblform = idTblform;
    }

    public String getFormDescription() {
        return formDescription;
    }

    public void setFormDescription(String formDescription) {
        this.formDescription = formDescription;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public List<TblFormField> getTblFormFields() {
        return tblFormFields;
    }

    public void setTblFormFields(List<TblFormField> tblFormFields) {
        this.tblFormFields = tblFormFields;
    }
}

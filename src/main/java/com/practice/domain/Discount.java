package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "discount")
public class Discount implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    @Lob
    private String description;

    //bi-directional many-to-one association to DiscountType
    @ManyToOne
    @JoinColumn (name = "type")
    private DiscountType discountType;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to Order
    @OneToMany (mappedBy = "discount")
    private List<Order> orders;

    public Discount () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public DiscountType getDiscountType () {
        return this.discountType;
    }

    public void setDiscountType (DiscountType discountType) {
        this.discountType = discountType;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<Order> getOrders () {
        return this.orders;
    }

    public void setOrders (List<Order> orders) {
        this.orders = orders;
    }

    public Order addOrder (Order order) {
        getOrders().add(order);
        order.setDiscount(this);

        return order;
    }

    public Order removeOrder (Order order) {
        getOrders().remove(order);
        order.setDiscount(null);

        return order;
    }

}

package com.practice.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.math.BigInteger;

@Entity
@Table(name = "subsidiary")
public class Subsidiary implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_subsidiary")
    private Integer idSubsidiary;

    @Lob
    private String address;

    @Lob
    private String address2;

    private String city;

    private String country;

    private BigInteger depth;

    private String description;

    @Column (name = "invoice_number_start")
    private Integer invoiceNumberStart;

    private BigInteger lft;

    private String name;

    @Column (name = "name_in_invoice")
    private String nameInInvoice;

    private String phone;

    @Column (name = "postal_code")
    private String postalCode;

    private String prefix;

    @Column (name = "prefix_invoice")
    private String prefixInvoice;

    private BigInteger rgt;

    @Column (name = "sales_number_start")
    private Integer salesNumberStart;

    private String state;

    @Column (name = "subsidiary_username")
    private String subsidiaryUsername;

    private BigInteger treeRoot;

    //bi-directional many-to-one association to Account
    @ManyToOne
    @JoinColumn (name = "id_default_account_intercompany")
    private Account account;

    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "id_country")
    private Country countryBean;

    //bi-directional many-to-one association to Currency
    @ManyToOne
    @JoinColumn (name = "id_default_currency")
    private Currency currency;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public Subsidiary () {
    }

    public Subsidiary (Integer idSubsidiary) {
        this.idSubsidiary = idSubsidiary;
    }

    public Integer getIdSubsidiary() {
        return idSubsidiary;
    }

    public void setIdSubsidiary(Integer idSubsidiary) {
        this.idSubsidiary = idSubsidiary;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public BigInteger getDepth() {
        return depth;
    }

    public void setDepth(BigInteger depth) {
        this.depth = depth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getInvoiceNumberStart() {
        return invoiceNumberStart;
    }

    public void setInvoiceNumberStart(Integer invoiceNumberStart) {
        this.invoiceNumberStart = invoiceNumberStart;
    }

    public BigInteger getLft() {
        return lft;
    }

    public void setLft(BigInteger lft) {
        this.lft = lft;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameInInvoice() {
        return nameInInvoice;
    }

    public void setNameInInvoice(String nameInInvoice) {
        this.nameInInvoice = nameInInvoice;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefixInvoice() {
        return prefixInvoice;
    }

    public void setPrefixInvoice(String prefixInvoice) {
        this.prefixInvoice = prefixInvoice;
    }

    public BigInteger getRgt() {
        return rgt;
    }

    public void setRgt(BigInteger rgt) {
        this.rgt = rgt;
    }

    public Integer getSalesNumberStart() {
        return salesNumberStart;
    }

    public void setSalesNumberStart(Integer salesNumberStart) {
        this.salesNumberStart = salesNumberStart;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSubsidiaryUsername() {
        return subsidiaryUsername;
    }

    public void setSubsidiaryUsername(String subsidiaryUsername) {
        this.subsidiaryUsername = subsidiaryUsername;
    }

    public BigInteger getTreeRoot() {
        return treeRoot;
    }

    public void setTreeRoot(BigInteger treeRoot) {
        this.treeRoot = treeRoot;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Country getCountryBean() {
        return countryBean;
    }

    public void setCountryBean(Country countryBean) {
        this.countryBean = countryBean;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }
}

package com.practice.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the temp_order database table.
 */
@Entity
@Table (name = "temp_order")
public class TempOrder implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column (name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn (name = "user_name")
    private UserInfo userinfo;

    @Column (name = "is_promotion")
    private Boolean isPromotion;

    @JoinColumn (name = "id_promotion", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.EAGER, optional = false)
    private Promotions promotions;


    @JoinColumn (name = "promotion_visibility_id", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.EAGER, optional = false)
    private PromotionVisibility promotionVisibility;

    @Column(name = "id_subpromotion")
    private Integer idSubPromotion;

    @ManyToOne
    @JoinColumn (name = "stock_product")
    private StockProductSize stockProductSize3;

    @Column (name = "create_date")
    private Date orderCreatedDate;

    @ManyToOne
    @JoinColumn (name = "id_carrier_service")
    private CarrierService carrierService;

    @Column (name = "device_id")
    private String deviceId;

    @Column (name = "is_available")
    private Boolean isAvailable;

    @Column (name = "product_status")
    private String productStatus;

    @Column (name = "unit_price")
    private BigDecimal unitPrice;

    @ManyToOne
    @JoinColumn (name = "currency", referencedColumnName = "id_currency")
    private Currency currency;

    @Column (name = "quantity")
    private Integer quantity;

    @Column (name = "promotion_free_product_group_id")
    private Integer promotionFreeProductGroup;

    @OneToMany (mappedBy = "idTempOrder", cascade = CascadeType.ALL)
    List<TempOrderDetail> tempOrderDetailList;

    @Column(name = "free_item_quantity")
    private Integer freeItemQuantity;

    @Column(name = "discount_percentage")
    private BigDecimal discountPercentage;

    private String createdBy;

    @Column
    private Timestamp createdAt;

    @Column(name = "freeze_for_payment")
    private Boolean freezeForPayment;

    private String reason;

    public TempOrder () {
    }

    public Integer getId () {
        return id;
    }

    public void setId (final Integer id) {
        this.id = id;
    }

    public UserInfo getUserinfo () {
        return userinfo;
    }

    public void setUserinfo (final UserInfo userinfo) {
        this.userinfo = userinfo;
    }

    public StockProductSize getStockProductSize3() {
        return stockProductSize3;
    }

    public void setStockProductSize3(StockProductSize stockProductSize3) {
        this.stockProductSize3 = stockProductSize3;
    }

    public int getQuantity () {
        return quantity;
    }

    public void setQuantity (final int quantity) {
        this.quantity = quantity;
    }

    public Date getOrderCreatedDate () {
        return orderCreatedDate;
    }

    public void setOrderCreatedDate (final Date orderCreatedDate) {
        this.orderCreatedDate = orderCreatedDate;
    }

    public CarrierService getCarrierService () {
        return carrierService;
    }

    public void setCarrierService (final CarrierService carrierService) {
        this.carrierService = carrierService;
    }

    public String getDeviceId () {
        return deviceId;
    }

    public void setDeviceId (final String deviceId) {
        this.deviceId = deviceId;
    }

    public Boolean getIsAvailable () {
        return isAvailable;
    }

    public void setIsAvailable (final Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getProductStatus () {
        return productStatus;
    }

    public void setProductStatus (final String productStatus) {
        this.productStatus = productStatus;
    }

    public BigDecimal getUnitPrice () {
        return unitPrice;
    }

    public void setUnitPrice (final BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Currency getCurrency () {
        return currency;
    }

    public void setCurrency (final Currency currency) {
        this.currency = currency;
    }

    public Boolean getIsPromotion () {
        return isPromotion;
    }

    public void setIsPromotion (final Boolean isPromotion) {
        this.isPromotion = isPromotion;
    }

    public Promotions getPromotions () {
        return promotions;
    }

    public void setPromotions (final Promotions promotions) {
        this.promotions = promotions;
    }

    public Integer getPromotionFreeProductGroup () {
        return promotionFreeProductGroup;
    }

    public void setPromotionFreeProductGroup (final Integer promotionFreeProductGroup) {
        this.promotionFreeProductGroup = promotionFreeProductGroup;
    }

    public void setQuantity (final Integer quantity) {
        this.quantity = quantity;
    }

    public List<TempOrderDetail> getTempOrderDetailList () {
        return tempOrderDetailList;
    }

    public void setTempOrderDetailList (final List<TempOrderDetail> tempOrderDetailList) {
        this.tempOrderDetailList = tempOrderDetailList;
    }

    public Integer getIdSubPromotion() {
        return idSubPromotion;
    }

    public void setIdSubPromotion(Integer idSubPromotion) {
        this.idSubPromotion = idSubPromotion;
    }

    public Integer getFreeItemQuantity() {
        return freeItemQuantity;
    }

    public void setFreeItemQuantity(Integer freeItemQuantity) {
        if(freeItemQuantity == null || freeItemQuantity.intValue()<0){
            this.freeItemQuantity = 0;
        }else{
            this.freeItemQuantity = freeItemQuantity;
        }
    }

    public BigDecimal getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(BigDecimal discountPercentage) {
        if(discountPercentage == null || discountPercentage.compareTo(BigDecimal.ZERO)<1){
            this.discountPercentage = BigDecimal.ZERO;
        }else{
            this.discountPercentage = discountPercentage;
        }
    }

    public PromotionVisibility getPromotionVisibility() {
        return promotionVisibility;
    }

    public void setPromotionVisibility(PromotionVisibility promotionVisibility) {
        this.promotionVisibility = promotionVisibility;
    }

    public Boolean getFreezeForPayment() {
        return freezeForPayment;
    }

    public void setFreezeForPayment(Boolean freezeForPayment) {
        this.freezeForPayment = this.freezeForPayment == null?false: freezeForPayment;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "TempOrder{" +
                "id=" + id +
                ", userinfo=" + userinfo +
                ", isPromotion=" + isPromotion +
                ", promotions=" + promotions +
                ", promotionVisibility=" + promotionVisibility +
                ", IdSubPromotion=" + idSubPromotion +
                ", stockProductSize=" + stockProductSize3 +
                ", orderCreatedDate=" + orderCreatedDate +
                ", carrierService=" + carrierService +
                ", deviceId='" + deviceId + '\'' +
                ", isAvailable=" + isAvailable +
                ", productStatus='" + productStatus + '\'' +
                ", unitPrice=" + unitPrice +
                ", currency=" + currency +
                ", quantity=" + quantity +
                ", promotionFreeProductGroup=" + promotionFreeProductGroup +
                ", tempOrderDetailList=" + tempOrderDetailList +
                ", freeItemQuantity=" + freeItemQuantity +
                ", discountPercentage=" + discountPercentage +
                ", freezeForPayment=" + freezeForPayment +
                '}';
    }

}

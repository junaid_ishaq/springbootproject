package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "payment_gateway")
public class PaymentGateway implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    private String name;

    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    @ManyToOne
    @JoinColumn (name = "payment_method_id")
    private PaymentType paymentMethod;

    @ManyToOne
    @JoinColumn  (name = "subsidiary_id")
    private Subsidiary subsidiaryId;


    public int getId () {
        return id;
    }

    public void setId (final int id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (final String name) {
        this.name = name;
    }

    public Status getStatusBean () {
        return statusBean;
    }

    public void setStatusBean (final Status statusBean) {
        this.statusBean = statusBean;
    }

    public PaymentType getPaymentMethod () {
        return paymentMethod;
    }

    public void setPaymentMethod (final PaymentType paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Subsidiary getSubsidiaryId() {
        return subsidiaryId;
    }

    public void setSubsidiaryId(Subsidiary subsidiaryId) {
        this.subsidiaryId = subsidiaryId;
    }
}


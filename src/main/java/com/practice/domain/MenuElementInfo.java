package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "menu_element_info")
public class MenuElementInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    @Column(name = "menu_name")
    private String menuName;

    //bi-directional many-to-one association to MenuElement
    @ManyToOne
    @JoinColumn (name = "id_menu_element")
    private MenuElement menuElement;

    //bi-directional many-to-one association to Translation
    @ManyToOne
    @JoinColumn (name = "id_translation")
    private Translation translation;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public MenuElement getMenuElement() {
        return menuElement;
    }

    public void setMenuElement(MenuElement menuElement) {
        this.menuElement = menuElement;
    }

    public Translation getTranslation() {
        return translation;
    }

    public void setTranslation(Translation translation) {
        this.translation = translation;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "classes")
public class Class implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_class")
    private int idClass;

    private String name;


    //bi-directional many-to-one association to Class
    @ManyToOne
    @JoinColumn (name = "id_parent_class")
    private Class class11;


    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public Class () {
    }

    public Class (int idClass) {
        this.idClass = idClass;
    }

    public int getIdClass () {
        return this.idClass;
    }

    public void setIdClass (int idClass) {
        this.idClass = idClass;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }


    public void setClass (Class class1) {
        this.class11 = class1;
    }


    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }


    public Class getClass11 () {
        return class11;
    }

    public void setClass11 (final Class class11) {
        this.class11 = class11;
    }

}

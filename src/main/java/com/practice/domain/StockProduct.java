package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "stock_product")
public class StockProduct implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String assembly;

    private String name;

    private Boolean assembled;

    @Column(name = "last_update")
    private Timestamp lastUpdate;

    //bi-directional many-to-one association to ProductType
    @ManyToOne
    @JoinColumn (name = "product_type")
    private ProductType productTypeBean;

    //bi-directional many-to-one association to ProductSubgroup
    @ManyToOne
    @JoinColumn (name = "id_product_group")
    private ProductGroup productGroup;


    @ManyToOne
    @JoinColumn (name = "id_product_subgroup")
    private ProductSubgroup productSubgroup;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "id_origin_country")
    private Country country;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "product_status")
    private Status productStatus;

    //bi-directional many-to-one association to StockProductSize
    @OneToMany (mappedBy = "stockProduct", fetch = FetchType.EAGER)
    private List<StockProductSize> stockProductSizes;

    @ManyToOne
    @JoinColumn (name = "id_brand")
    private Brand brandBean;

    @Column (name = "slug_name")
    private String slugName;

    public StockProduct () {
    }

    public String getAssembly () {
        return this.assembly;
    }

    public void setAssembly (String assembly) {
        this.assembly = assembly;
    }

    public Boolean getAssembled () {
        return this.assembled;
    }

    public void setAssembled (Boolean assembled) {
        this.assembled = assembled;
    }

    public Timestamp getLastUpdate () {
        return this.lastUpdate;
    }

    public void setLastUpdate (Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public ProductType getProductTypeBean () {
        return this.productTypeBean;
    }

    public void setProductTypeBean (ProductType productTypeBean) {
        this.productTypeBean = productTypeBean;
    }

    public ProductSubgroup getProductSubgroup () {
        return this.productSubgroup;
    }

    public void setProductSubgroup (ProductSubgroup productSubgroup) {
        this.productSubgroup = productSubgroup;
    }

    public Status getStatusBean () {
        return statusBean;
    }

    public void setStatusBean (final Status statusBean) {
        this.statusBean = statusBean;
    }

    public Country getCountry () {
        return country;
    }

    public void setCountry (final Country country) {
        this.country = country;
    }

    public Status getProductStatus () {
        return productStatus;
    }

    public void setProductStatus (final Status productStatus) {
        this.productStatus = productStatus;
    }

    public List<StockProductSize> getStockProductSizes () {
        return this.stockProductSizes;
    }

    public void setStockProductSizes (List<StockProductSize> stockProductSizes) {
        this.stockProductSizes = stockProductSizes;
    }

    public StockProductSize addStockProductSize (StockProductSize stockProductSize) {
        getStockProductSizes().add(stockProductSize);
        stockProductSize.setStockProduct(this);

        return stockProductSize;
    }

    public StockProductSize removeStockProductSize (StockProductSize stockProductSize) {
        getStockProductSizes().remove(stockProductSize);
        stockProductSize.setStockProduct(null);

        return stockProductSize;
    }


    public ProductGroup getProductGroup () {
        return productGroup;
    }

    public void setProductGroup (final ProductGroup productGroup) {
        this.productGroup = productGroup;
    }


    public Brand getBrandBean () {
        return brandBean;
    }

    public void setBrandBean (final Brand brandBean) {
        this.brandBean = brandBean;
    }

    public String getSlugName () {
        return slugName;
    }

    public void setSlugName (final String slugName) {
        this.slugName = slugName;
    }
}

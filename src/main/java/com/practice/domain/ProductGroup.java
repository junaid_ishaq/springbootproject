package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "product_group")
public class ProductGroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Integer id;

    @Column(name = "group_name")
    private String groupName;

    @Column (name = "slug_name")
    private String slugName;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to ProductGroupPricePerState
    @OneToMany (mappedBy = "productGroupBean")
    private List<ProductGroupPricePerState> productGroupPricePerStates;

    //bi-directional many-to-one association to ProductGroupPricePerUser
    @OneToMany (mappedBy = "productGroupBean")
    private List<ProductGroupPricePerUser> productGroupPricePerUsers;

    //bi-directional many-to-one association to ProductGroupVisibilityPerCountry
    @OneToMany (mappedBy = "productGroup")
    private List<ProductGroupVisibilityPerCountry> productGroupVisibilityPerCountries;

    //bi-directional many-to-one association to ProductGroupVisibilityPerState
    @OneToMany (mappedBy = "productGroup")
    private List<ProductGroupVisibilityPerState> productGroupVisibilityPerStates;

    //bi-directional many-to-one association to ProductSubgroup
    @OneToMany (mappedBy = "productGroupBean")
    private List<ProductSubgroup> productSubgroups;

    public ProductGroup () {
    }

    public Integer getId () {
        return this.id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getGroupName () {
        return this.groupName;
    }

    public void setGroupName (String groupName) {
        this.groupName = groupName;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<ProductGroupPricePerState> getProductGroupPricePerStates () {
        return this.productGroupPricePerStates;
    }

    public void setProductGroupPricePerStates (List<ProductGroupPricePerState> productGroupPricePerStates) {
        this.productGroupPricePerStates = productGroupPricePerStates;
    }

    public ProductGroupPricePerState addProductGroupPricePerState (
            ProductGroupPricePerState productGroupPricePerState) {
        getProductGroupPricePerStates().add(productGroupPricePerState);
        productGroupPricePerState.setProductGroupBean(this);

        return productGroupPricePerState;
    }

    public ProductGroupPricePerState removeProductGroupPricePerState (
            ProductGroupPricePerState productGroupPricePerState) {
        getProductGroupPricePerStates().remove(productGroupPricePerState);
        productGroupPricePerState.setProductGroupBean(null);

        return productGroupPricePerState;
    }

    public List<ProductGroupPricePerUser> getProductGroupPricePerUsers () {
        return this.productGroupPricePerUsers;
    }

    public void setProductGroupPricePerUsers (List<ProductGroupPricePerUser> productGroupPricePerUsers) {
        this.productGroupPricePerUsers = productGroupPricePerUsers;
    }

    public ProductGroupPricePerUser addProductGroupPricePerUser (ProductGroupPricePerUser productGroupPricePerUser) {
        getProductGroupPricePerUsers().add(productGroupPricePerUser);
        productGroupPricePerUser.setProductGroupBean(this);

        return productGroupPricePerUser;
    }

    public ProductGroupPricePerUser removeProductGroupPricePerUser (ProductGroupPricePerUser productGroupPricePerUser) {
        getProductGroupPricePerUsers().remove(productGroupPricePerUser);
        productGroupPricePerUser.setProductGroupBean(null);

        return productGroupPricePerUser;
    }

    public List<ProductGroupVisibilityPerCountry> getProductGroupVisibilityPerCountries () {
        return this.productGroupVisibilityPerCountries;
    }

    public void setProductGroupVisibilityPerCountries (
            List<ProductGroupVisibilityPerCountry> productGroupVisibilityPerCountries) {
        this.productGroupVisibilityPerCountries = productGroupVisibilityPerCountries;
    }

    public ProductGroupVisibilityPerCountry addProductGroupVisibilityPerCountry (
            ProductGroupVisibilityPerCountry productGroupVisibilityPerCountry) {
        getProductGroupVisibilityPerCountries().add(productGroupVisibilityPerCountry);
        productGroupVisibilityPerCountry.setProductGroup(this);

        return productGroupVisibilityPerCountry;
    }

    public ProductGroupVisibilityPerCountry removeProductGroupVisibilityPerCountry (
            ProductGroupVisibilityPerCountry productGroupVisibilityPerCountry) {
        getProductGroupVisibilityPerCountries().remove(productGroupVisibilityPerCountry);
        productGroupVisibilityPerCountry.setProductGroup(null);

        return productGroupVisibilityPerCountry;
    }

    public List<ProductGroupVisibilityPerState> getProductGroupVisibilityPerStates () {
        return this.productGroupVisibilityPerStates;
    }

    public void setProductGroupVisibilityPerStates (
            List<ProductGroupVisibilityPerState> productGroupVisibilityPerStates) {
        this.productGroupVisibilityPerStates = productGroupVisibilityPerStates;
    }

    public ProductGroupVisibilityPerState addProductGroupVisibilityPerState (
            ProductGroupVisibilityPerState productGroupVisibilityPerState) {
        getProductGroupVisibilityPerStates().add(productGroupVisibilityPerState);
        productGroupVisibilityPerState.setProductGroup(this);

        return productGroupVisibilityPerState;
    }

    public ProductGroupVisibilityPerState removeProductGroupVisibilityPerState (
            ProductGroupVisibilityPerState productGroupVisibilityPerState) {
        getProductGroupVisibilityPerStates().remove(productGroupVisibilityPerState);
        productGroupVisibilityPerState.setProductGroup(null);

        return productGroupVisibilityPerState;
    }

    public List<ProductSubgroup> getProductSubgroups () {
        return this.productSubgroups;
    }

    public void setProductSubgroups (List<ProductSubgroup> productSubgroups) {
        this.productSubgroups = productSubgroups;
    }

    public ProductSubgroup addProductSubgroup (ProductSubgroup productSubgroup) {
        getProductSubgroups().add(productSubgroup);
        productSubgroup.setProductGroupBean(this);

        return productSubgroup;
    }

    public ProductSubgroup removeProductSubgroup (ProductSubgroup productSubgroup) {
        getProductSubgroups().remove(productSubgroup);
        productSubgroup.setProductGroupBean(null);

        return productSubgroup;
    }

    public String getSlugName () {
        return slugName;
    }

    public void setSlugName (final String slugName) {
        this.slugName = slugName;
    }
}

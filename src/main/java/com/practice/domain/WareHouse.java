package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "warehouse")
public class WareHouse implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int idwarehouse;

    @Lob
    private String address;

    @Lob
    private String address2;

    @Lob
    private String address3;

    @Lob
    private String address4;

    @Lob
    private String address5;

    private String city;

    private String country;

    private String description;

    private String location;

    private String phone;

    @Column(name = "postal_code")
    private String postalCode;

    @Column (name = "print_barcode")
    private Boolean printBarcode;

    private String state;

    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "id_country")
    private Country countryBean;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to Subsidiary
    @ManyToOne
    @JoinColumn (name = "id_subsidiary")
    private Subsidiary subsidiary;

    //bi-directional many-to-one association to Zone
    @ManyToOne
    @JoinColumn (name = "id_zone")
    private Zone zone;

    @ManyToOne
    @JoinColumn(name="parent_id")
    private WareHouse parentWareHouse;

    public WareHouse () {
    }

    public WareHouse (int idwarehouse) {
        this.idwarehouse = idwarehouse;
    }

    public int getIdwarehouse () {
        return this.idwarehouse;
    }

    public void setIdwarehouse (int idwarehouse) {
        this.idwarehouse = idwarehouse;
    }

    public String getAddress () {
        return this.address;
    }

    public void setAddress (String address) {
        this.address = address;
    }

    public String getAddress2 () {
        return this.address2;
    }

    public void setAddress2 (String address2) {
        this.address2 = address2;
    }

    public String getAddress3 () {
        return this.address3;
    }

    public void setAddress3 (String address3) {
        this.address3 = address3;
    }

    public String getAddress4 () {
        return this.address4;
    }

    public void setAddress4 (String address4) {
        this.address4 = address4;
    }

    public String getAddress5 () {
        return this.address5;
    }

    public void setAddress5 (String address5) {
        this.address5 = address5;
    }

    public String getCity () {
        return this.city;
    }

    public void setCity (String city) {
        this.city = city;
    }

    public String getCountry () {
        return this.country;
    }

    public void setCountry (String country) {
        this.country = country;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getLocation () {
        return this.location;
    }

    public void setLocation (String location) {
        this.location = location;
    }

    public String getPhone () {
        return this.phone;
    }

    public void setPhone (String phone) {
        this.phone = phone;
    }

    public String getPostalCode () {
        return this.postalCode;
    }

    public void setPostalCode (String postalCode) {
        this.postalCode = postalCode;
    }

    public Boolean getPrintBarcode () {
        return this.printBarcode;
    }

    public void setPrintBarcode (Boolean printBarcode) {
        this.printBarcode = printBarcode;
    }

    public String getState () {
        return this.state;
    }

    public void setState (String state) {
        this.state = state;
    }

    public Country getCountryBean () {
        return this.countryBean;
    }

    public void setCountryBean (Country countryBean) {
        this.countryBean = countryBean;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public Subsidiary getSubsidiary () {
        return this.subsidiary;
    }

    public void setSubsidiary (Subsidiary subsidiary) {
        this.subsidiary = subsidiary;
    }

    public Zone getZone () {
        return this.zone;
    }

    public void setZone (Zone zone) {
        this.zone = zone;
    }

    public WareHouse getParentWareHouse() {
        return parentWareHouse;
    }

    public void setParentWareHouse(WareHouse parentWareHouse) {
        this.parentWareHouse = parentWareHouse;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "promotion_type")
public class PromotionType implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic (optional = false)
    @Column (name = "id")
    private Integer id;

    @Column (name = "name")
    @Basic (optional = false)
    private String name;

    @Column (name = "description")
    @Basic (optional = false)
    private String description;

    @JoinColumn (name = "status", referencedColumnName = "id")
    @ManyToOne (fetch = FetchType.LAZY)
    private Status status;

    public Integer getId () {
        return id;
    }

    public void setId (final Integer id) {
        this.id = id;
    }

    public String getName () {
        return name;
    }

    public void setName (final String name) {
        this.name = name;
    }

    public String getDescription () {
        return description;
    }

    public void setDescription (final String description) {
        this.description = description;
    }

    public Status getStatus () {
        return status;
    }

    public void setStatus (final Status status) {
        this.status = status;
    }
}


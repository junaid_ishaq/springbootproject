package com.practice.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "userinfo")
public class UserInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "user_name")
    private String userName;

    @Column(name = "activation_token")
    private String activationToken;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    private String email;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "is_pending_to_confirm")
    private Boolean isPendingToConfirm;

    @Column(name = "save_credit_card_info")
    private Boolean saveCreditCardInfo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_login_time")
    private Date lastLoginTime;

    private String password;

    @Column(name = "profile_img_url")
    private String profileImgUrl;

    @Column(name = "user_level_price")
    private Boolean userLevelPrice;

    @ManyToOne
    @JoinColumn (name = "id_translation")
    private Translation translation;

    @ManyToOne
    @JoinColumn(name = "status")
    private Status statusBean;

    @ManyToOne
    @JoinColumn(name = "user_type")
    private UserType userTypeBean;

    @ManyToOne
    @JoinColumn(name = "assigned_sales_rep")
    private UserInfo salesRep;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_default_warehouse")
    private WareHouse defaultWareHouse;

    @Column(name = "id_class")
    private Integer idClass;

    @Column(name = "commission")
    private BigDecimal commission;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public void setActivationToken(String activationToken) {
        this.activationToken = activationToken;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Boolean getPendingToConfirm() {
        return isPendingToConfirm;
    }

    public void setPendingToConfirm(Boolean pendingToConfirm) {
        isPendingToConfirm = pendingToConfirm;
    }

    public Boolean getSaveCreditCardInfo() {
        return saveCreditCardInfo;
    }

    public void setSaveCreditCardInfo(Boolean saveCreditCardInfo) {
        this.saveCreditCardInfo = saveCreditCardInfo;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfileImgUrl() {
        return profileImgUrl;
    }

    public void setProfileImgUrl(String profileImgUrl) {
        this.profileImgUrl = profileImgUrl;
    }

    public Boolean getUserLevelPrice() {
        return userLevelPrice;
    }

    public void setUserLevelPrice(Boolean userLevelPrice) {
        this.userLevelPrice = userLevelPrice;
    }

    public Translation getTranslation() {
        return translation;
    }

    public void setTranslation(Translation translation) {
        this.translation = translation;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }

    public UserType getUserTypeBean() {
        return userTypeBean;
    }

    public void setUserTypeBean(UserType userTypeBean) {
        this.userTypeBean = userTypeBean;
    }

    public UserInfo getSalesRep() {
        return salesRep;
    }

    public void setSalesRep(UserInfo salesRep) {
        this.salesRep = salesRep;
    }

    public WareHouse getDefaultWareHouse() {
        return defaultWareHouse;
    }

    public void setDefaultWareHouse(WareHouse defaultWareHouse) {
        this.defaultWareHouse = defaultWareHouse;
    }

    public Integer getIdClass() {
        return idClass;
    }

    public void setIdClass(Integer idClass) {
        this.idClass = idClass;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }
}

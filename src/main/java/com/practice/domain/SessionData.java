package com.practice.domain;


import com.practice.dto.ActionDTO;
import com.practice.dto.FeaturesDTO;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class SessionData implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer currentLanguageId;
    private String selectedShippingCarrier;
    private String userType;
    private String fullName;
    private String userName;
    private String email;
    private boolean userLevelPricing;
    private Integer cartCountryId;
    private Integer stateId;
    private Integer pricingCountry;
    private Map<String, ActionDTO> allowedAction;
    private boolean adminUser;
    private boolean vtb;
    private boolean docCabVisible;
    private List<FeaturesDTO> allowedFeatures;
    private boolean saveCreditCardInfo;
    private boolean qaUser;
    private boolean salesRep;
    private boolean salesManager;
    private String salesRepUserName;
    private String userCurrencyISO;
    private String userCurrencySymbol;


    public Integer getCurrentLanguageId() {
        return currentLanguageId;
    }

    public void setCurrentLanguageId(Integer currentLanguageId) {
        this.currentLanguageId = currentLanguageId;
    }

    public String getSelectedShippingCarrier() {
        return selectedShippingCarrier;
    }

    public void setSelectedShippingCarrier(String selectedShippingCarrier) {
        this.selectedShippingCarrier = selectedShippingCarrier;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isUserLevelPricing() {
        return userLevelPricing;
    }

    public void setUserLevelPricing(boolean userLevelPricing) {
        this.userLevelPricing = userLevelPricing;
    }

    public void setCartCountryId(Integer cartCountryId) {
        this.cartCountryId = cartCountryId;
    }

    public Integer getCartCountryId() {
        return cartCountryId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setPricingCountry(Integer pricingCountry) {
        this.pricingCountry = pricingCountry;
    }

    public Integer getPricingCountry() {
        return pricingCountry;
    }

    public Map<String, ActionDTO> getAllowedAction() {
        return allowedAction;
    }

    public void setAllowedAction(Map<String, ActionDTO> allowedAction) {
        this.allowedAction = allowedAction;
    }

    public void setAdminUser(boolean adminUser) {
        this.adminUser = adminUser;
    }

    public boolean getAdminUser() {
        return adminUser;
    }

    public void setVtb(boolean vtb) {
        this.vtb = vtb;
    }

    public boolean getVtb() {
        return vtb;
    }

    public void setDocCabVisible(boolean docCabVisible) {
        this.docCabVisible = docCabVisible;
    }

    public boolean getDocCabVisible() {
        return docCabVisible;
    }

    public void setAllowedFeatures(List<FeaturesDTO> allowedFeatures) {
        this.allowedFeatures = allowedFeatures;
    }

    public List<FeaturesDTO> getAllowedFeatures() {
        return allowedFeatures;
    }

    public void setSaveCreditCardInfo(boolean saveCreditCardInfo) {
        this.saveCreditCardInfo = saveCreditCardInfo;
    }

    public boolean getSaveCreditCardInfo() {
        return saveCreditCardInfo;
    }

    public void setQaUser(boolean qaUser) {
        this.qaUser = qaUser;
    }

    public boolean getQaUser() {
        return qaUser;
    }

    public void setSalesRep(boolean salesRep) {
        this.salesRep = salesRep;
    }

    public boolean getSalesRep() {
        return salesRep;
    }

    public void setSalesManager(boolean salesManager) { 
        this.salesManager = salesManager;
    }

    public boolean getSalesManager() {
        return salesManager;
    }

    public void setSalesRepUserName(String salesRepUserName) {
        this.salesRepUserName = salesRepUserName;
    }

    public String getSalesRepUserName() {
        return salesRepUserName;
    }

    public void setAdditionalGlobalDiscount(BigDecimal zero) {
    }

    public void setUserCurrencyISO(String userCurrencyISO) {
        this.userCurrencyISO = userCurrencyISO;
    }

    public String getUserCurrencyISO() {
        return userCurrencyISO;
    }

    public void setUserCurrencySymbol(String userCurrencySymbol) {
        this.userCurrencySymbol = userCurrencySymbol;
    }

    public String getUserCurrencySymbol() {
        return userCurrencySymbol;
    }
}

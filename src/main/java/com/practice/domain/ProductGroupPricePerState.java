package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "product_group_price_per_state")
public class ProductGroupPricePerState implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    private BigDecimal price;

    //bi-directional many-to-one association to ProductGroup
    @ManyToOne
    @JoinColumn(name = "product_group", insertable = false, updatable = false)
    private ProductGroup productGroupBean;

    //bi-directional many-to-one association to CountryState
    @ManyToOne
    @JoinColumn (name = "id_state", insertable = false, updatable = false)
    private CountryState countryState;

    //bi-directional many-to-one association to UserType
    @ManyToOne
    @JoinColumn (name = "user_type", insertable = false, updatable = false)
    private UserType userTypeBean;

    public ProductGroupPricePerState () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public BigDecimal getPrice () {
        return this.price;
    }

    public void setPrice (BigDecimal price) {
        this.price = price;
    }

    public ProductGroup getProductGroupBean () {
        return this.productGroupBean;
    }

    public void setProductGroupBean (ProductGroup productGroupBean) {
        this.productGroupBean = productGroupBean;
    }

    public CountryState getCountryState () {
        return this.countryState;
    }

    public void setCountryState (CountryState countryState) {
        this.countryState = countryState;
    }

    public UserType getUserTypeBean () {
        return this.userTypeBean;
    }

    public void setUserTypeBean (UserType userTypeBean) {
        this.userTypeBean = userTypeBean;
    }

}

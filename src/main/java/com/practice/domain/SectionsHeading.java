package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sections_heading")
public class SectionsHeading implements Serializable {


    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private int id;

    @Column(name = "button_title")
    private String buttonTitle;

    private String description;

    @Column (name = "detail_description")
    private String detailDescription;

    private String heading;

    //bi-directional many-to-one association to OnePageSection
    @ManyToOne
    @JoinColumn (name = "id_section")
    private OnePageSection onePageSection;

    //bi-directional many-to-one association to Translation
    @ManyToOne
    @JoinColumn (name = "id_translation")
    private Translation translation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getButtonTitle() {
        return buttonTitle;
    }

    public void setButtonTitle(String buttonTitle) {
        this.buttonTitle = buttonTitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetailDescription() {
        return detailDescription;
    }

    public void setDetailDescription(String detailDescription) {
        this.detailDescription = detailDescription;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public OnePageSection getOnePageSection() {
        return onePageSection;
    }

    public void setOnePageSection(OnePageSection onePageSection) {
        this.onePageSection = onePageSection;
    }

    public Translation getTranslation() {
        return translation;
    }

    public void setTranslation(Translation translation) {
        this.translation = translation;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table (name = "product_subgroup_visibility_per_country")
public class ProductSubgroupVisibilityPerCountry implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    //bi-directional many-to-one association to ProductSubgroup
    @ManyToOne
    @JoinColumn (name = "product_subgroup_id", insertable = false, updatable = false)
    private ProductSubgroup productSubgroup;

    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "id_country", insertable = false, updatable = false)
    private Country country;

    //bi-directional many-to-one association to UserType
    @ManyToOne
    @JoinColumn (name = "user_type", insertable = false, updatable = false)
    private UserType userTypeBean;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public ProductSubgroupVisibilityPerCountry () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public ProductSubgroup getProductSubgroup () {
        return this.productSubgroup;
    }

    public void setProductSubgroup (ProductSubgroup productSubgroup) {
        this.productSubgroup = productSubgroup;
    }

    public Country getCountry () {
        return this.country;
    }

    public void setCountry (Country country) {
        this.country = country;
    }

    public UserType getUserTypeBean () {
        return this.userTypeBean;
    }

    public void setUserTypeBean (UserType userTypeBean) {
        this.userTypeBean = userTypeBean;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

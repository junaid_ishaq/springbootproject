package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "product_subgroup")
public class ProductSubgroup implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Integer id;

    @Column(name = "subgroup_name")
    private String subgroupName;

    //bi-directional many-to-one association to ProductGroupPricePerCountry
    @OneToMany(mappedBy = "productSubgroup")
    private List<ProductGroupPricePerCountry> productGroupPricePerCountries;

    @Column (name = "slug_name")
    private String slugName;

    //bi-directional many-to-one association to ProductGroup
    @ManyToOne
    @JoinColumn (name = "product_group")
    private ProductGroup productGroupBean;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to ProductSubgroupPricePerCountry
    @OneToMany (mappedBy = "productSubgroupBean")
    private List<ProductSubgroupPricePerCountry> productSubgroupPricePerCountries;

    //bi-directional many-to-one association to ProductSubgroupPricePerState
    @OneToMany (mappedBy = "productSubgroupBean")
    private List<ProductSubgroupPricePerState> productSubgroupPricePerStates;

    //bi-directional many-to-one association to ProductSubgroupPricePerUser
    @OneToMany (mappedBy = "productSubgroupBean")
    private List<ProductSubgroupPricePerUser> productSubgroupPricePerUsers;

    //bi-directional many-to-one association to ProductSubgroupVisibilityPerCountry
    @OneToMany (mappedBy = "productSubgroup")
    private List<ProductSubgroupVisibilityPerCountry> productSubgroupVisibilityPerCountries;

    //bi-directional many-to-one association to ProductSubgroupVisibilityPerState
    @OneToMany (mappedBy = "productSubgroup")
    private List<ProductSubgroupVisibilityPerState> productSubgroupVisibilityPerStates;

    //bi-directional many-to-one association to StockProduct
    @OneToMany (mappedBy = "productSubgroup")
    private List<StockProduct> stockProducts;

    public ProductSubgroup () {
    }

    public Integer getId () {
        return this.id;
    }

    public void setId (Integer id) {
        this.id = id;
    }

    public String getSubgroupName () {
        return this.subgroupName;
    }

    public void setSubgroupName (String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public List<ProductGroupPricePerCountry> getProductGroupPricePerCountries () {
        return this.productGroupPricePerCountries;
    }

    public void setProductGroupPricePerCountries (List<ProductGroupPricePerCountry> productGroupPricePerCountries) {
        this.productGroupPricePerCountries = productGroupPricePerCountries;
    }

    public ProductGroupPricePerCountry addProductGroupPricePerCountry (
            ProductGroupPricePerCountry productGroupPricePerCountry) {
        getProductGroupPricePerCountries().add(productGroupPricePerCountry);
        productGroupPricePerCountry.setProductSubgroup(this);

        return productGroupPricePerCountry;
    }

    public ProductGroupPricePerCountry removeProductGroupPricePerCountry (
            ProductGroupPricePerCountry productGroupPricePerCountry) {
        getProductGroupPricePerCountries().remove(productGroupPricePerCountry);
        productGroupPricePerCountry.setProductSubgroup(null);

        return productGroupPricePerCountry;
    }

    public ProductGroup getProductGroupBean () {
        return this.productGroupBean;
    }

    public void setProductGroupBean (ProductGroup productGroupBean) {
        this.productGroupBean = productGroupBean;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<ProductSubgroupPricePerCountry> getProductSubgroupPricePerCountries () {
        return this.productSubgroupPricePerCountries;
    }

    public void setProductSubgroupPricePerCountries (
            List<ProductSubgroupPricePerCountry> productSubgroupPricePerCountries) {
        this.productSubgroupPricePerCountries = productSubgroupPricePerCountries;
    }

    public ProductSubgroupPricePerCountry addProductSubgroupPricePerCountry (
            ProductSubgroupPricePerCountry productSubgroupPricePerCountry) {
        getProductSubgroupPricePerCountries().add(productSubgroupPricePerCountry);
        productSubgroupPricePerCountry.setProductSubgroupBean(this);

        return productSubgroupPricePerCountry;
    }

    public ProductSubgroupPricePerCountry removeProductSubgroupPricePerCountry (
            ProductSubgroupPricePerCountry productSubgroupPricePerCountry) {
        getProductSubgroupPricePerCountries().remove(productSubgroupPricePerCountry);
        productSubgroupPricePerCountry.setProductSubgroupBean(null);

        return productSubgroupPricePerCountry;
    }

    public List<ProductSubgroupPricePerState> getProductSubgroupPricePerStates () {
        return this.productSubgroupPricePerStates;
    }

    public void setProductSubgroupPricePerStates (List<ProductSubgroupPricePerState> productSubgroupPricePerStates) {
        this.productSubgroupPricePerStates = productSubgroupPricePerStates;
    }

    public ProductSubgroupPricePerState addProductSubgroupPricePerState (
            ProductSubgroupPricePerState productSubgroupPricePerState) {
        getProductSubgroupPricePerStates().add(productSubgroupPricePerState);
        productSubgroupPricePerState.setProductSubgroupBean(this);

        return productSubgroupPricePerState;
    }

    public ProductSubgroupPricePerState removeProductSubgroupPricePerState (
            ProductSubgroupPricePerState productSubgroupPricePerState) {
        getProductSubgroupPricePerStates().remove(productSubgroupPricePerState);
        productSubgroupPricePerState.setProductSubgroupBean(null);

        return productSubgroupPricePerState;
    }

    public List<ProductSubgroupPricePerUser> getProductSubgroupPricePerUsers () {
        return this.productSubgroupPricePerUsers;
    }

    public void setProductSubgroupPricePerUsers (List<ProductSubgroupPricePerUser> productSubgroupPricePerUsers) {
        this.productSubgroupPricePerUsers = productSubgroupPricePerUsers;
    }

    public ProductSubgroupPricePerUser addProductSubgroupPricePerUser (
            ProductSubgroupPricePerUser productSubgroupPricePerUser) {
        getProductSubgroupPricePerUsers().add(productSubgroupPricePerUser);
        productSubgroupPricePerUser.setProductSubgroupBean(this);

        return productSubgroupPricePerUser;
    }

    public ProductSubgroupPricePerUser removeProductSubgroupPricePerUser (
            ProductSubgroupPricePerUser productSubgroupPricePerUser) {
        getProductSubgroupPricePerUsers().remove(productSubgroupPricePerUser);
        productSubgroupPricePerUser.setProductSubgroupBean(null);

        return productSubgroupPricePerUser;
    }

    public List<ProductSubgroupVisibilityPerCountry> getProductSubgroupVisibilityPerCountries () {
        return this.productSubgroupVisibilityPerCountries;
    }

    public void setProductSubgroupVisibilityPerCountries (
            List<ProductSubgroupVisibilityPerCountry> productSubgroupVisibilityPerCountries) {
        this.productSubgroupVisibilityPerCountries = productSubgroupVisibilityPerCountries;
    }

    public ProductSubgroupVisibilityPerCountry addProductSubgroupVisibilityPerCountry (
            ProductSubgroupVisibilityPerCountry productSubgroupVisibilityPerCountry) {
        getProductSubgroupVisibilityPerCountries().add(productSubgroupVisibilityPerCountry);
        productSubgroupVisibilityPerCountry.setProductSubgroup(this);

        return productSubgroupVisibilityPerCountry;
    }

    public ProductSubgroupVisibilityPerCountry removeProductSubgroupVisibilityPerCountry (
            ProductSubgroupVisibilityPerCountry productSubgroupVisibilityPerCountry) {
        getProductSubgroupVisibilityPerCountries().remove(productSubgroupVisibilityPerCountry);
        productSubgroupVisibilityPerCountry.setProductSubgroup(null);

        return productSubgroupVisibilityPerCountry;
    }

    public List<ProductSubgroupVisibilityPerState> getProductSubgroupVisibilityPerStates () {
        return this.productSubgroupVisibilityPerStates;
    }

    public void setProductSubgroupVisibilityPerStates (
            List<ProductSubgroupVisibilityPerState> productSubgroupVisibilityPerStates) {
        this.productSubgroupVisibilityPerStates = productSubgroupVisibilityPerStates;
    }

    public ProductSubgroupVisibilityPerState addProductSubgroupVisibilityPerState (
            ProductSubgroupVisibilityPerState productSubgroupVisibilityPerState) {
        getProductSubgroupVisibilityPerStates().add(productSubgroupVisibilityPerState);
        productSubgroupVisibilityPerState.setProductSubgroup(this);

        return productSubgroupVisibilityPerState;
    }

    public ProductSubgroupVisibilityPerState removeProductSubgroupVisibilityPerState (
            ProductSubgroupVisibilityPerState productSubgroupVisibilityPerState) {
        getProductSubgroupVisibilityPerStates().remove(productSubgroupVisibilityPerState);
        productSubgroupVisibilityPerState.setProductSubgroup(null);

        return productSubgroupVisibilityPerState;
    }

    public List<StockProduct> getStockProducts () {
        return this.stockProducts;
    }

    public void setStockProducts (List<StockProduct> stockProducts) {
        this.stockProducts = stockProducts;
    }

    public StockProduct addStockProduct (StockProduct stockProduct) {
        getStockProducts().add(stockProduct);
        stockProduct.setProductSubgroup(this);

        return stockProduct;
    }

    public StockProduct removeStockProduct (StockProduct stockProduct) {
        getStockProducts().remove(stockProduct);
        stockProduct.setProductSubgroup(null);

        return stockProduct;
    }



    public String getSlugName () {
        return slugName;
    }

    public void setSlugName (final String slugName) {
        this.slugName = slugName;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "payment_gateway_detail")
public class PaymentGatewayDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "gateway_id")
    private int gatewayId;

    private String email;

    @Column (name = "is_testing_account")
    private Boolean isTestingAccount;

    @Column (name = "merchant_id")
    private String merchantId;

    @Column (name = "host")
    private String host;

    @ManyToOne
    @JoinColumn (name = "payment_method_id",referencedColumnName = "id")
    private PaymentGateway paymentGateway;

    private String password;

    @Column (name = "registered_email")
    private String registeredEmail;

    @Column (name = "publisher_key")
    private String publisherKey;

    @Column (name = "private_key")
    private String privateKey;

    private String url;

    private String user;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to PaymentGatewayForCountryState
    @OneToMany (mappedBy = "paymentGateway")
    private List<PaymentGatewayForCountryState> paymentGatewayForCountryStates;

    public PaymentGatewayDetail () {
    }

    public int getGatewayId () {
        return this.gatewayId;
    }

    public void setGatewayId (int gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getEmail () {
        return this.email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public Boolean getIsTestingAccount () {
        return this.isTestingAccount;
    }

    public void setIsTestingAccount (Boolean isTestingAccount) {
        this.isTestingAccount = isTestingAccount;
    }

    public String getMerchantId () {
        return this.merchantId;
    }

    public void setMerchantId (String merchantId) {
        this.merchantId = merchantId;
    }

    public PaymentGateway getPaymentGateway () {
        return paymentGateway;
    }

    public void setPaymentGateway (final PaymentGateway paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getPassword () {
        return this.password;
    }

    public void setPassword (String password) {
        this.password = password;
    }

    public String getRegisteredEmail () {
        return this.registeredEmail;
    }

    public void setRegisteredEmail (String registeredEmail) {
        this.registeredEmail = registeredEmail;
    }

    public String getUrl () {
        return this.url;
    }

    public void setUrl (String url) {
        this.url = url;
    }

    public String getUser () {
        return this.user;
    }

    public void setUser (String user) {
        this.user = user;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<PaymentGatewayForCountryState> getPaymentGatewayForCountryStates () {
        return this.paymentGatewayForCountryStates;
    }

    public void setPaymentGatewayForCountryStates (List<PaymentGatewayForCountryState> paymentGatewayForCountryStates) {
        this.paymentGatewayForCountryStates = paymentGatewayForCountryStates;
    }

    public PaymentGatewayForCountryState addPaymentGatewayForCountryState (
            PaymentGatewayForCountryState paymentGatewayForCountryState) {
        getPaymentGatewayForCountryStates().add(paymentGatewayForCountryState);
        paymentGatewayForCountryState.setPaymentGateway(this);

        return paymentGatewayForCountryState;
    }

    public PaymentGatewayForCountryState removePaymentGatewayForCountryState (
            PaymentGatewayForCountryState paymentGatewayForCountryState) {
        getPaymentGatewayForCountryStates().remove(paymentGatewayForCountryState);
        paymentGatewayForCountryState.setPaymentGateway(null);

        return paymentGatewayForCountryState;
    }

    public String getHost () {
        return host;
    }

    public void setHost (final String host) {
        this.host = host;
    }

    public String getPublisherKey () {
        return publisherKey;
    }

    public void setPublisherKey (final String publisherKey) {
        this.publisherKey = publisherKey;
    }

    public String getPrivateKey () {
        return privateKey;
    }

    public void setPrivateKey (final String privateKey) {
        this.privateKey = privateKey;
    }
}

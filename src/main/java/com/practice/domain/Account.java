package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "account")
public class Account implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Column(name = "id_account")
    private int idAccount;


    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to AccountType
    @ManyToOne
    @JoinColumn (name = "id_account_type")
    private AccountType accountType;

    public int getIdAccount () {
        return idAccount;
    }

    public void setIdAccount (final int idAccount) {
        this.idAccount = idAccount;
    }


    public Status getStatusBean () {
        return statusBean;
    }

    public void setStatusBean (final Status statusBean) {
        this.statusBean = statusBean;
    }

    public AccountType getAccountType () {
        return accountType;
    }

    public void setAccountType (final AccountType accountType) {
        this.accountType = accountType;
    }
}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "module")
public class Module implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id_module")
    private int idModule;

    @Column (name = "is_subsidiary_supported")
    private Boolean isSubsidiarySupported;

    private String name;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to TblForm
    @OneToMany (mappedBy = "module")
    private List<TblForm> tblForms;

    public int getIdModule() {
        return idModule;
    }

    public void setIdModule(int idModule) {
        this.idModule = idModule;
    }

    public Boolean getSubsidiarySupported() {
        return isSubsidiarySupported;
    }

    public void setSubsidiarySupported(Boolean subsidiarySupported) {
        isSubsidiarySupported = subsidiarySupported;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<TblForm> getTblForms() {
        return tblForms;
    }

    public void setTblForms(List<TblForm> tblForms) {
        this.tblForms = tblForms;
    }

    public boolean getIsSubsidiarySupported() {
        return isSubsidiarySupported;
    }

    public void setIsSubsidiarySupported(boolean isSubsidiarySupported) {
        this.isSubsidiarySupported = isSubsidiarySupported;
    }
}

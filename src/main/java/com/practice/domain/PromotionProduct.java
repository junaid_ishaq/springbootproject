package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "promotion_product")
public class PromotionProduct implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic (optional = false)
    @Column (name = "id")
    private Integer id;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn (name = "promotion", referencedColumnName = "id")
    private Promotions promotions;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "brand", referencedColumnName = "id")
    private Brand brand;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "category", referencedColumnName = "id")
    private ProductGroup group;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "subCategory", referencedColumnName = "id")
    private ProductSubgroup subGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn (name = "product", referencedColumnName = "id")
    private StockProductSize product;

    @Column (name = "quantity")
    private Integer quantity;

    @Column (name = "productCategory")
    private String productCategory;

    public Integer getId () {
        return id;
    }

    public void setId (final Integer id) {
        this.id = id;
    }

    public Promotions getPromotions () {
        return promotions;
    }

    public void setPromotions (final Promotions promotions) {
        this.promotions = promotions;
    }

    public StockProductSize getProduct () {
        return product;
    }

    public void setProduct (final StockProductSize product) {
        this.product = product;
    }

    public Brand getBrand () {
        return brand;
    }

    public void setBrand (final Brand brand) {
        this.brand = brand;
    }

    public ProductGroup getGroup () {
        return group;
    }

    public void setGroup (final ProductGroup group) {
        this.group = group;
    }

    public ProductSubgroup getSubGroup () {
        return subGroup;
    }

    public void setSubGroup (final ProductSubgroup subGroup) {
        this.subGroup = subGroup;
    }

    public Integer getQuantity () {
        return quantity;
    }

    public void setQuantity (final Integer quantity) {
        this.quantity = quantity;
    }

    public String getProductCategory () {
        return productCategory;
    }

    public void setProductCategory (final String productCategory) {
        this.productCategory = productCategory;
    }
}


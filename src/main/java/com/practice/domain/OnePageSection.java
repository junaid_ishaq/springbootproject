package com.practice.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "one_page_sections")
public class OnePageSection implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "background_image")
    private String backgroundImage;

    @Column (name = "section_name")
    private String sectionName;

    //bi-directional many-to-one association to MenuElement
    @ManyToOne
    @JoinColumn (name = "id_menu_element")
    private MenuElement menuElement;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to SectionsHeading
    @OneToMany (mappedBy = "onePageSection")
    private List<SectionsHeading> sectionsHeadings;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public MenuElement getMenuElement() {
        return menuElement;
    }

    public void setMenuElement(MenuElement menuElement) {
        this.menuElement = menuElement;
    }

    public Status getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<SectionsHeading> getSectionsHeadings() {
        return sectionsHeadings;
    }

    public void setSectionsHeadings(List<SectionsHeading> sectionsHeadings) {
        this.sectionsHeadings = sectionsHeadings;
    }
}

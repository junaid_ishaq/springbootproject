package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_type")
public class UserType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String type;

    private String description;


    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn(name = "status")
    private Status statusBean;

    public UserType () {
    }

    public UserType (String type) {
        this.type = type;
    }

    public String getType () {
        return this.type;
    }

    public void setType (String type) {
        this.type = type;
    }

    public String getDescription () {
        return this.description;
    }

    public void setDescription (String description) {
        this.description = description;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

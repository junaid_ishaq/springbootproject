package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "stock_product_components")
public class StockProductComponent implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    @Column(name = "component_quantity")
    private BigDecimal componentQuantity;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to StockProductSize
    @ManyToOne
    @JoinColumn (name = "component_id")
    private StockProductSize stockProductSize1;

    //bi-directional many-to-one association to StockProductSize
    @ManyToOne
    @JoinColumn (name = "result_product")
    private StockProductSize stockProductSize2;

    public StockProductComponent () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public BigDecimal getComponentQuantity () {
        return this.componentQuantity;
    }

    public void setComponentQuantity (BigDecimal componentQuantity) {
        this.componentQuantity = componentQuantity;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public StockProductSize getStockProductSize1 () {
        return this.stockProductSize1;
    }

    public void setStockProductSize1 (StockProductSize stockProductSize1) {
        this.stockProductSize1 = stockProductSize1;
    }

    public StockProductSize getStockProductSize2 () {
        return this.stockProductSize2;
    }

    public void setStockProductSize2 (StockProductSize stockProductSize2) {
        this.stockProductSize2 = stockProductSize2;
    }

}

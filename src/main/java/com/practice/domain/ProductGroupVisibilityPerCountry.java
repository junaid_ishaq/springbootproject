package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "product_group_visibility_per_country")
public class ProductGroupVisibilityPerCountry implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    //bi-directional many-to-one association to ProductGroup
    @ManyToOne
    @JoinColumn(name = "product_group_id", insertable = false, updatable = false)
    private ProductGroup productGroup;

    //bi-directional many-to-one association to Country
    @ManyToOne
    @JoinColumn (name = "id_country", insertable = false, updatable = false)
    private Country country;

    //bi-directional many-to-one association to UserType
    @ManyToOne
    @JoinColumn (name = "user_type", insertable = false, updatable = false)
    private UserType userTypeBean;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    public ProductGroupVisibilityPerCountry () {
    }

    public int getId () {
        return this.id;
    }

    public void setId (int id) {
        this.id = id;
    }

    public ProductGroup getProductGroup () {
        return this.productGroup;
    }

    public void setProductGroup (ProductGroup productGroup) {
        this.productGroup = productGroup;
    }

    public Country getCountry () {
        return this.country;
    }

    public void setCountry (Country country) {
        this.country = country;
    }

    public UserType getUserTypeBean () {
        return this.userTypeBean;
    }

    public void setUserTypeBean (UserType userTypeBean) {
        this.userTypeBean = userTypeBean;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

}

package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "action")
public class Action implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_action")
    private int idAction;

    private String name;

    private String description;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to ActionsPerCountry
    @OneToMany (mappedBy = "action")
    private List<ActionsPerCountry> actionsPerCountries;

    //bi-directional many-to-one association to SubAction
    @OneToMany (mappedBy = "action")
    private List<SubAction> subActions;

    //bi-directional many-to-one association to UserInfoAction
    @OneToMany (mappedBy = "action")
    private List<UserInfoAction> UserInfoActions;


    public Action () {
    }

    public int getIdAction () {
        return this.idAction;
    }

    public void setIdAction (int idAction) {
        this.idAction = idAction;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public List<ActionsPerCountry> getActionsPerCountries () {
        return this.actionsPerCountries;
    }

    public void setActionsPerCountries (List<ActionsPerCountry> actionsPerCountries) {
        this.actionsPerCountries = actionsPerCountries;
    }

    public ActionsPerCountry addActionsPerCountry (ActionsPerCountry actionsPerCountry) {
        getActionsPerCountries().add(actionsPerCountry);
        actionsPerCountry.setAction(this);

        return actionsPerCountry;
    }

    public ActionsPerCountry removeActionsPerCountry (ActionsPerCountry actionsPerCountry) {
        getActionsPerCountries().remove(actionsPerCountry);
        actionsPerCountry.setAction(null);

        return actionsPerCountry;
    }

    public List<SubAction> getSubActions () {
        return this.subActions;
    }

    public void setSubActions (List<SubAction> subActions) {
        this.subActions = subActions;
    }

    public SubAction addSubAction (SubAction subAction) {
        getSubActions().add(subAction);
        subAction.setAction(this);

        return subAction;
    }

    public SubAction removeSubAction (SubAction subAction) {
        getSubActions().remove(subAction);
        subAction.setAction(null);

        return subAction;
    }


    public List<UserInfoAction> getUserInfoActions () {
        return this.UserInfoActions;
    }

    public void setUserInfoActions (List<UserInfoAction> UserInfoActions) {
        this.UserInfoActions = UserInfoActions;
    }

    public UserInfoAction addUserInfoAction (UserInfoAction UserInfoAction) {
        getUserInfoActions().add(UserInfoAction);
        UserInfoAction.setAction(this);

        return UserInfoAction;
    }

    public UserInfoAction removeUserInfoAction (UserInfoAction UserInfoAction) {
        getUserInfoActions().remove(UserInfoAction);
        UserInfoAction.setAction(null);

        return UserInfoAction;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

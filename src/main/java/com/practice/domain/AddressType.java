package com.practice.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "address_type")
public class AddressType implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String name;

    private int status;

    //bi-directional many-to-one association to UserInfoAddress
    @OneToMany(mappedBy = "addressTypeBean")
    private List<UserInfoAddress> UserInfoAddresses;

    public AddressType () {
    }

    public AddressType (String name) {
        this.name = name;
    }

    public String getName () {
        return this.name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public int getStatus () {
        return this.status;
    }

    public void setStatus (int status) {
        this.status = status;
    }

    public List<UserInfoAddress> getUserInfoAddresses () {
        return this.UserInfoAddresses;
    }

    public void setUserInfoAddresses (List<UserInfoAddress> UserInfoAddresses) {
        this.UserInfoAddresses = UserInfoAddresses;
    }

    public UserInfoAddress addUserInfoAddress (UserInfoAddress UserInfoAddress) {
        getUserInfoAddresses().add(UserInfoAddress);
        UserInfoAddress.setAddressTypeBean(this);

        return UserInfoAddress;
    }

    public UserInfoAddress removeUserInfoAddress (UserInfoAddress UserInfoAddress) {
        getUserInfoAddresses().remove(UserInfoAddress);
        UserInfoAddress.setAddressTypeBean(null);

        return UserInfoAddress;
    }

}

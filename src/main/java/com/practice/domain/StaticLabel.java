package com.practice.domain;

import com.practice.domain.Module;
import com.practice.domain.StaticLabelInfo;
import com.practice.domain.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the static_label database table.
 */
@Entity
@Table (name = "static_label")
public class StaticLabel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column (name = "id_static_label")
    private int idStaticLabel;

    private String property;

    //bi-directional many-to-one association to Status
    @ManyToOne
    @JoinColumn (name = "status")
    private Status statusBean;

    //bi-directional many-to-one association to Module
    @ManyToOne
    @JoinColumn (name = "id_module")
    private Module module;

    //bi-directional many-to-one association to StaticLabelInfo
    @OneToMany (mappedBy = "staticLabel")
    private List<StaticLabelInfo> staticLabelInfos;

    public StaticLabel () {
    }

    public int getIdStaticLabel () {
        return this.idStaticLabel;
    }

    public void setIdStaticLabel (int idStaticLabel) {
        this.idStaticLabel = idStaticLabel;
    }

    public String getProperty () {
        return this.property;
    }

    public void setProperty (String property) {
        this.property = property;
    }

    public Status getStatusBean () {
        return this.statusBean;
    }

    public void setStatusBean (Status statusBean) {
        this.statusBean = statusBean;
    }

    public Module getModule () {
        return this.module;
    }

    public void setModule (Module module) {
        this.module = module;
    }

    public List<StaticLabelInfo> getStaticLabelInfos () {
        return this.staticLabelInfos;
    }

    public void setStaticLabelInfos (List<StaticLabelInfo> staticLabelInfos) {
        this.staticLabelInfos = staticLabelInfos;
    }

    public StaticLabelInfo addStaticLabelInfo (StaticLabelInfo staticLabelInfo) {
        getStaticLabelInfos().add(staticLabelInfo);
        staticLabelInfo.setStaticLabel(this);

        return staticLabelInfo;
    }

    public StaticLabelInfo removeStaticLabelInfo (StaticLabelInfo staticLabelInfo) {
        getStaticLabelInfos().remove(staticLabelInfo);
        staticLabelInfo.setStaticLabel(null);

        return staticLabelInfo;
    }

}

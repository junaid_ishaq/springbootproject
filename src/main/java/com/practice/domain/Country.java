package com.practice.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "country")
public class Country implements Serializable {

    private static final long serialVersionUID = 983748734834L;

    @Id
    @Column (name = "id_country")
    private Integer idCountry;

    @Column (name = "cookie_policy")
    private Boolean cookiePolicy;

    @Column (name = "ISO")
    private String iso;

    @Column (name = "manufacturer_country")
    private Boolean manufacturerCountry;

    @Lob
    private String name;

    @Lob
    @Column (name = "native_name")
    private String nativeName;

    @Column (name = "state_level_actions")
    private Boolean stateLevelActions;

    @Column (name = "state_level_price")
    private Boolean stateLevelPrice;

    @Column (name = "state_level_product_visibility")
    private Boolean stateLevelProductVisibility;

    @Column (name = "zipcode_mandatory")
    private Boolean zipcodeMandatory;

    @Column(name = "time_zone")
    private String timeZone;

    //bi-directional many-to-one association to Currency
    @ManyToOne
    @JoinColumn (name = "id_currency")
    private Currency currency;

    public Integer getIdCountry() {
        return idCountry;
    }

    public void setIdCountry(Integer idCountry) {
        this.idCountry = idCountry;
    }

    public Boolean getCookiePolicy() {
        return cookiePolicy;
    }

    public void setCookiePolicy(Boolean cookiePolicy) {
        this.cookiePolicy = cookiePolicy;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public Boolean getManufacturerCountry() {
        return manufacturerCountry;
    }

    public void setManufacturerCountry(Boolean manufacturerCountry) {
        this.manufacturerCountry = manufacturerCountry;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public Boolean getStateLevelActions() {
        return stateLevelActions;
    }

    public void setStateLevelActions(Boolean stateLevelActions) {
        this.stateLevelActions = stateLevelActions;
    }

    public Boolean getStateLevelPrice() {
        return stateLevelPrice;
    }

    public void setStateLevelPrice(Boolean stateLevelPrice) {
        this.stateLevelPrice = stateLevelPrice;
    }

    public Boolean getStateLevelProductVisibility() {
        return stateLevelProductVisibility;
    }

    public void setStateLevelProductVisibility(Boolean stateLevelProductVisibility) {
        this.stateLevelProductVisibility = stateLevelProductVisibility;
    }

    public Boolean getZipcodeMandatory() {
        return zipcodeMandatory;
    }

    public void setZipcodeMandatory(Boolean zipcodeMandatory) {
        this.zipcodeMandatory = zipcodeMandatory;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}

package com.practice.enums;

public enum  ProductStatusEnum {

    FREE(7, "FREE_PRODUCT"),
    REGULAR(5, "REGULAR_PRODUCT"),
    COMMING_SOON(6, "COMING_SOON_PRODUCT"),
    MARKETING_PRODUCT(28, "MARKETING_PRODUCT"),
    NEW(4, "NEW_PRODUCT"),
    OUT_OF_STOCK(62, "OUT_OF_STOCK");

    ProductStatusEnum (final Integer id, final String name) {
        this.id = id;
        this.name = name;
    }

    private Integer id;
    private String  name;

    public Integer getId () {
        return id;
    }

    public String getName () {
        return name;
    }
}

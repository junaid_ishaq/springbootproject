package com.practice.enums;

public enum UserTypeEnum {

    HAIR_PROFESSIONAL(1, "Hair Professional"),
    SALON_OWNER(2, "Salon Owner"),
    INDIVIDUAL(3, "Home User"),
    DISTRIBUTOR("distributor", "DISTRIBUTOR"),
    SALON("saloonUser", "SALON"),
    ADMIN("admin", "admin"),
    QA("qa", "qa"),
    GUEST("GUEST", "GUEST"),
    ENDUSER("endUser", "ENDUSER"),
    HAIRPREFESSIONAL("hairProfessional","HairProfessional"),
    SALES_CONSULTANT(4, "Sales Consultant"),
    INFLUENCER(5, "Influencer"),
    SALESCONSULTANT("salesConsultant", "Sales Consultant"),
    INFLUENCERMARKETING("influencer", "Influencer"),
    DIRECTSALESALON("directsalessalon", "directsalessalon"),
    HAIR_CONTEST_PARTICIPANT(7, "HairContestParticipant");

    private String type;
    private String name;
    private Integer id;

    public Integer getId() {
        return id;
    }


    UserTypeEnum (final Integer id, final String name) {
        this.id = id;
        this.name = name;
    }

    UserTypeEnum (final String type, final  String name) {
        this.type = type;
        this.name = name;
    }

    public String getType () {
        return type;
    }

    public String getName () {
        return name;
    }

    public static UserTypeEnum getById (final int id) {
        switch (id) {
            case 1:
                return HAIR_PROFESSIONAL;
            case 2:
                return SALON_OWNER;
            case 3:
                return INDIVIDUAL;
            case 4:
                return SALES_CONSULTANT;
            case 5:
                return INFLUENCER;
            case 6:
                return HAIR_CONTEST_PARTICIPANT;
            default:
                return null;
        }
    }
}

package com.practice.enums;

public enum StatusEnum {

    ACTIVE(1, "ACTIVE"),
    DELETED(3, "DELETED"),
    PENDING_APPROVAL(8, "PENDING_APPROVAL"),
    PENDING_FULFILLMENT(47,"PENDING_FULFILLMENT"),
    PACKED(11,"PACKED"),
    SHIPPED(12,"SHIPPED"),
    RECEIVED(0,"RECEIVED"),
    SHIPMENT_APPROVED(56,"SHIPMENT_APPROVED"),
    SHIPMENT_BOOKED(57,"SHIPMENT_BOOKED"),
    SHIPMENT_BILLED(61,"SHIPMENT/BILLED"),
    CLOSED(30,"CLOSED"),
    REQUEST_RETURN(58,"REQUEST_RETURN"),
    CANCELED(59,"CANCELED"),
    APPROVED(9, "APPROVED"),
    REJECTED(10, "REJECTED"),
    DISABLE(2, "DISABLED"),
    Promotion_Free_Product(22, "Promotion_Free_Product"),
    Promotion_Buy_Product(23, "Promotion_Buy_Product"),
    Promotion_Free_Product_Group_1(25, "Promotion Free Product Group 1"),
    Promotion_Free_Product_Group_2(26, "Promotion Free Product Group 2"),
    OUT_OF_STOCK(62, "OUT_OF_STOCK"),
    PENDING_PAYMENT(75, "PENDING_PAYMENT"),



    // QR Statuses
    QR_CREATED(37,"QR_CREATED"),
    QR_VERIFIED(38,"QR_VERIFIED"),
    QR_EXPIRED(39,"QR_EXPIRED"),

    // In case Customer marks order as Get from GK
    PARTIAL_PAID (40, "PARTIAL_PAID"),

    // Pickup Salon statuses
    PENDING_PICKUP (42, "PENDING_PICKUP"),
    SALONS_AVAILABLE (43, "SALONS_AVAILABLE"),
    ACCEPTED_SALON (44, "ACCEPTED_SALON"),
    DELIVERED (45, "DELIVERED"),
    DELIVERED_AND_REVIEWED (46, "DELIVERED_AND_REVIEWED"),
    BACK_ORDER (71, "BACK_ORDER");

    private Integer id;
    private String  name;

    StatusEnum (final Integer id, final String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId () {
        return id;
    }

    public String getName () {
        return name;
    }
}

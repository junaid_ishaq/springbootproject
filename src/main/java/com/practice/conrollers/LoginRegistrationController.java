package com.practice.conrollers;

import com.practice.dto.LoginUserDTO;
import com.practice.dto.ResponseObject;
import com.practice.dto.StateDTO;
import com.practice.dto.ValidationDTO;
import com.practice.service.CountryStateService;
import com.practice.service.LoginRegistrationService;
import com.practice.service.SecurityService;
import com.practice.service.StaticLabelInfoService;
import com.practice.util.AppConstant;
import com.practice.util.ResponseUtil;
import com.practice.validation.LoginUserDtoValidation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.mgt.SecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static com.practice.util.AppConstant.*;

@RestController
public class LoginRegistrationController {


    @Autowired
    CountryStateService countryStateService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private LoginRegistrationService loginRegistrationService;

    @Autowired
    private StaticLabelInfoService staticLabelInfoService;

    @RequestMapping(value = AppConstant.GET_STATES_LIST+"/{idCountry}")
    @ResponseBody
    public ResponseObject getStatesList(@PathVariable(name = "idCountry") final
                                            Integer idCountry){
        List<StateDTO> dtoList = new ArrayList<>();
        if (idCountry != null) {
            dtoList = countryStateService.getStatesDTOListByCountry(idCountry);
        }
        return ResponseUtil.createResponseObject(SUCCESS, null, dtoList);
    }

    @PostMapping(value = SIGN_IN)
    public ResponseObject login(@RequestBody LoginUserDTO dto,
                                HttpServletRequest request,
                                HttpServletResponse response){
        ValidationDTO validationDTO = LoginUserDtoValidation.validateLoginUser(dto);
        if(!validationDTO.isValidated()){
            if(dto.getLanguageId() != null && dto.getLanguageId() > 0){
                 if(securityService.getSessionData() != null){
                     securityService.getSessionData().setCurrentLanguageId(dto.getLanguageId());
                 }
            }
            return ResponseUtil.createResponseObject(FAILURE, staticLabelInfoService.getPropertyValue(
                    validationDTO.getErrorMessage(), securityService.getCurrentLanguageId()), null);
        }

        loginRegistrationService.authenticate(dto,request);

        return ResponseUtil.createResponseObject(SUCCESS, staticLabelInfoService.getPropertyValue(LOGIN_SUCCESS,
                securityService
                        .getCurrentLanguageId()),
                null);
    }
}

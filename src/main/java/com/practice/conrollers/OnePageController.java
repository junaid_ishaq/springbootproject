package com.practice.conrollers;

import com.practice.dto.HeaderElementsDTO;
import com.practice.dto.HomeElementDTO;
import com.practice.dto.ResponseObject;
import com.practice.service.OnePageService;
import com.practice.service.SecurityService;
import com.practice.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.practice.util.AppConstant.*;

@Controller
public class OnePageController {

    @Autowired
    private OnePageService onePageService;

    @Autowired
    private SecurityService securityService;


    @RequestMapping(value = GET_SECTION_2, method = RequestMethod.GET)
    @ResponseBody
    public ResponseObject getSection2(){
     Integer countryId = 1;
     Integer stateId = 6;
     String  userType = "endUser";
     Integer languageId = 1;
     Integer deviceTypeId = 0;
        List<HomeElementDTO> slideImageList = onePageService.getSliderImages(
                countryId, stateId, userType, deviceTypeId, languageId);
        return  ResponseUtil.createResponseObject(SUCCESS, null, slideImageList);
    }


    @GetMapping(value = GET_HEADER_CONTENT)
    @ResponseBody
    public ResponseObject getHeaderContent(){
        HeaderElementsDTO headerElementsDTO = onePageService.getHeaderElements(securityService.getCurrentLanguageId(),
              securityService.getUserType());
        headerElementsDTO.setStickyNotes(onePageService.getStickyNotes());
        headerElementsDTO.setWhatsAppNumber(onePageService.getWhatsAppNumber());
        headerElementsDTO.setMobileApplicationLinks(onePageService.getMobileApplicationLinks());
        return ResponseUtil.createResponseObject(SUCCESS, null, headerElementsDTO);
    }
}

package com.practice.repository;

import com.practice.domain.Country;
import com.practice.domain.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CountryRepository extends JpaRepository<Country,Integer> {


    /**
     * @param countryName
     *
     * @return
     */
    @Query("select country.id from Country country where country.name=?1")
    Integer getCountryId (String countryName);

    /**
     * @param id
     *
     * @return
     */
    @Query ("select country from Country country where country.idCountry=?1")
    Country getCountryByCountryId (Integer id);

    /**
     * @return
     */
    @Query ("select country from Country country order by country.name ASC")
    List<Country> getEcomCountries ();


    /**
     *
     * @param idCountry
     * @return
     */
    @Query ("select country.currency.iso from Country country where country.idCountry = ?1")
    String getCurrencyIsoById (Integer idCountry);


    /**
     *
     * @param idCountry
     * @return
     */
    @Query ("select country.currency from Country country where country.idCountry = ?1")
    Currency getCountryCurrency (Integer idCountry);

    /**
     *
     * @param iso
     * @return
     */
    @Query (value = "select id_country from country where country.iso=?1 limit 1", nativeQuery = true)
    Integer getCountryIdByISO (String iso);

    /**
     *
     * @return
     */
    @Query(value = "select id_country from country where status = 1", nativeQuery = true)
    Integer[] getCountriesId();
}

package com.practice.repository;


import com.practice.domain.Translation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TranslationRepository extends JpaRepository<Translation,Integer> {

    /**
     * @param translationId
     *
     * @return
     */
    @Query("select trans from Translation trans where trans.idTranslation =?1")
    Translation getTraslationByID (Integer translationId);

    /**
     * @param iso
     *
     * @return
     */
    @Query (value = "select id_translation from translation where code = ?1 and status = 1 limit 1", nativeQuery = true)
    Integer getTranslationIdByISO (String iso);

    /**
     * @return
     */
    @Query ("select trans from Translation trans where trans.statusBean.id=?1 and trans.defaultTranslation=1")
    List<Translation> getAllActiveDefaultTranslations(Integer status);

    /**
     * @param translationId
     *
     * @return
     */
    @Query ("select trans from Translation trans where trans.language.idLanguage=?1 and trans.statusBean.id=1")
    List<Translation> getLanguageTranslationStatus (Integer translationId);

    /**
     *
     * @param idCountry
     * @return
     */
    @Query (value = "select id_translation from translation where id_country=?1 and status=1 limit 1",
            nativeQuery = true)
    Integer getTranslationIdByCountry (Integer idCountry);

    /**
     *
     * @param translationId
     * @return
     */
    @Query (value = "select code from translation where id_translation=?1 and status=1 limit 1",
            nativeQuery = true)
    String getTranslationCodeById(Integer translationId);
}


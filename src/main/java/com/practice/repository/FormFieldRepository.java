package com.practice.repository;

import com.practice.domain.TblFormField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FormFieldRepository extends JpaRepository<TblFormField,Integer> {

    /**
     * @param form
     * @param idTranslation
     *
     * @return
     */
    @Query("select formfield from TblFormField formfield where formfield.tblForm.formName=?1" +
            " and formfield.translation.idTranslation=?2")
    List<TblFormField> findFormFieldsByFormName (String form, Integer idTranslation);

    /**
     * @param label
     * @param idTranslation
     *
     * @return
     */
    @Query ("select formfield.fieldValue from TblFormField formfield where formfield.fieldName = ?1 and formfield" +
            ".translation.idTranslation = ?2 group by formfield.fieldName")
    String getFieldLabel (String label, Integer idTranslation);


    /**
     *
     * @param label
     * @param form
     * @param idTranslation
     * @return
     */
    @Query ("select formfield.fieldValue from TblFormField formfield where formfield.fieldName = ?1 and formfield" +
            ".translation.idTranslation = ?3 and formfield.tblForm.formName=?2")
    String getFieldValueByForm (String label, String form, Integer idTranslation);

    /**
     * @param idTranslation
     *
     * @return
     */
    @Query (value = "select * " +
            "from tbl_form_fields where id_tblform =1 and id_translation =?1 and field_name in ('meta_description', 'meta_page_title')", nativeQuery = true)
    List<TblFormField> findMetaInformation (Integer idTranslation);
}

package com.practice.repository;

import com.practice.domain.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyRepository extends JpaRepository<Currency,Integer> {
    /**
     * @param iso
     *
     * @return
     */
    @Query("select currency from Currency currency where iso=?1")
    Currency getCurrencyByISO (String iso);
}

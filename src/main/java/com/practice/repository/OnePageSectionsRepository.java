package com.practice.repository;

import com.practice.domain.OnePageSection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OnePageSectionsRepository extends JpaRepository<OnePageSection, Integer> {

    /**
     * @param sectionName
     *
     * @return
     */
    @Query("select section from OnePageSection section where section.sectionName=?1")
    OnePageSection getSectionByName (String sectionName);
}

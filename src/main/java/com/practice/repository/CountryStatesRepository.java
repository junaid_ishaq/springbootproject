package com.practice.repository;


import com.practice.domain.CountryState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryStatesRepository extends JpaRepository<CountryState , Integer> {
    /**
     *
     * @param countryId
     * @return
     */
    @Query ("select countryStates from  CountryState countryStates where countryStates.country.idCountry = ?1 and " +
            "countryStates.name <> 'All States'")
    List<CountryState> getStatesByCountryId (Integer countryId);

}

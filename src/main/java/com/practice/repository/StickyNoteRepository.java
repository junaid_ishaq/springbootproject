package com.practice.repository;

import com.practice.domain.StickyNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.List;

@Repository
public interface StickyNoteRepository extends JpaRepository<StickyNote, Integer> {

    @Query("select sn from StickyNote sn where (sn.idCountry=?1 or sn.idCountry=0) " +
            " and sn.startDate <=?2 and sn.endDate>=?2 and sn.idTranslation=?3 and sn.status.id<>?4 and sn.status.id<>?5" +
            " and (sn.userType is null or sn.userType like concat('%', ?6, '%')) order by sn.userType")
    List<StickyNote> getStickyNoteList(Integer countryId, Calendar now, Integer translationId, Integer DeleteStatus,
                                       Integer disableStatus, String userType);
}

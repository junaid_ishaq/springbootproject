package com.practice.repository;

import com.practice.domain.UserInfo;
import com.practice.domain.UserInfoAddress;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo, String> {

    /**
     * Get pending to confirm user info.
     * @param userName
     *
     * @return UserInfo
     */
    @Query(
            "select user from UserInfo user where user.userName = ?1 and user.isPendingToConfirm = 1")
    UserInfo getPendingToConfirmUser(String userName);


    /**
     * @return count of total records in table
     */
    @Query ("select count(user) from UserInfo user")
    Integer getUsersCountByType();

    /**
     * @param userName
     *
     * @return
     */
    @Query ("select user from UserInfo user where user.userName = ?1 and user.isPendingToConfirm = false")
    UserInfo getUser (String userName);

    /**
     *
     * @param email
     * @param statusId
     * @param userType
     * @return
     */
    @Query ("select user from UserInfo user where user.email = ?1 and user.statusBean.id!=?2 and user.userTypeBean.description <> ?3 ")
    List<UserInfo> getUserNameFromEmailWithoutUserType (String email, Integer statusId, String userType);


    /**
     * @param userName
     *
     * @return
     */
    @Query ("select user.userName from UserInfo user where user.userName = ?1")
    String getUserName (String userName);

    /**
     * @param userName
     *
     * @return
     */
    @Query ("select user.email from UserInfo user where user.userName = ?1")
    String getEmail (String userName);

    /**
     * @return
     */
    @Query ("select user from UserInfo user where user.userTypeBean.type = 'distributor'")
    List<UserInfo> getDistributorList ();

    /**
     * @param email
     *
     * This method will return only signed-up users not social media users
     *
     * @return
     */
    @Query ("select user from UserInfo user where (user.email = ?1 or user.userName = ?1) " +
            " and user.isPendingToConfirm=0 and user.statusBean.id=?2 and user.userTypeBean.description <> ?3 ")
    List<UserInfo> getVTBUserFromEmail (String email, Integer statusId,String userType);


    @Query ("select user from UserInfo user where user.email = ?1 and user.statusBean.id!=?2 and user.userTypeBean.description = ?3")
    List<UserInfo> getUserNameFromEmailWithUserType (String email, Integer statusId,String userType);

    /**
     *
     * @param userNameOrEmail
     * @param userType
     * @return
     */
    @Query("select user from UserInfo user where (user.userName = ?1 or user.email=?1) and user.userTypeBean.type!=?2")
    List<UserInfo> getNonGuestUserByUserNameOrEmail(String userNameOrEmail, String userType);

    /**
     *
     * @param email
     * @param userType
     * @param statusId
     * @return
     */
    @Query("select user from UserInfo user where (user.email=?1 or user.userName=?1) and user.userTypeBean.type<>?2 and user.statusBean.id<>?3")
    UserInfo getUserByEmailAndUserType(String email, String userType, Integer statusId);

    /**
     *
     * @param userName
     * @param status
     * @param isDefault
     * @return
     */
    @Query ("select address from UserInfoAddress address where address.userInfo.userName = ?1 and " +
                    " address.addressTypeBean.name='shipping' and address.statusBean.id=?2 and address.addressTypeBean.status=?2 and address.defaultAddress=?3 ")
    List<UserInfoAddress> getUserShippingAddress(String userName, Integer status, boolean isDefault);
}

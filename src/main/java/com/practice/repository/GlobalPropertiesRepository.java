package com.practice.repository;


import com.practice.domain.GlobalProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GlobalPropertiesRepository extends JpaRepository<GlobalProperty,Integer> {

    /**
     *
     * @param name
     * @return
     */
    @Query("select globalProperties.value from GlobalProperty globalProperties where globalProperties.name=?1")
    String getPropertyValue (String name);


}

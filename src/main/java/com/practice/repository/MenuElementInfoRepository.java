package com.practice.repository;


import com.practice.domain.MenuElementInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuElementInfoRepository extends JpaRepository<MenuElementInfo, Integer> {

    /**
     * @param menuId
     * @param idTranslation
     *
     * @return
     */
    @Query("select info.menuName from MenuElementInfo info where info.menuElement.id=?1 and info.translation" +
            ".idTranslation=?2")
    String getElementName (Integer menuId, Integer idTranslation);

    /**
     * @param idTranslation
     * @param idStatus
     *
     * @return
     */
    @Query ("select info from MenuElementInfo info where info.menuElement.menu.type='FOOTER'" +
            " and info.translation.idTranslation=?1 and info.menuElement.menu.statusBean.id=?2")
    List<MenuElementInfo> getFooterMenus (Integer idTranslation, Integer idStatus);
}

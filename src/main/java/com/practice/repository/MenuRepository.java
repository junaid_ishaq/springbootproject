package com.practice.repository;

import com.practice.domain.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu,Integer> {

    /**
     * @param idStatus
     *
     * @return
     */
    @Query("select menu from Menu menu where menu.type='HEADER' and menu.statusBean.id=?1")
    Menu getHeader (Integer idStatus);

    /**
     * @param idStatus
     *
     * @return
     */
    @Query ("select menu from Menu menu where menu.type='FOOTER' and menu.statusBean.id=?1")
    Menu getFooter (Integer idStatus);
}

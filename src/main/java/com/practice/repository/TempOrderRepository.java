package com.practice.repository;

import com.practice.domain.CarrierService;
import com.practice.domain.TempOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface TempOrderRepository extends JpaRepository<TempOrder,Integer> {

    /**
     * @param userName
     *
     * @return
     */
    @Query (value = "select * from temp_order where user_name=?1 order by id desc", nativeQuery = true)
    List<TempOrder> findAllByUserName (String userName);

    /**
     * @param deviceId
     * @param sizeId
     *
     * @return
     */
    @Query ("select tempOrders from TempOrder tempOrders where tempOrders.deviceId = ?1 and tempOrders.stockProductSize3" +
                    ".id=?2")
    List<TempOrder> findByDeviceId_Product (String deviceId, Integer sizeId);

    /**
     * @param userName
     * @param sizeId
     *
     * @return
     */
    @Query ("select tempOrders from TempOrder tempOrders where tempOrders.userinfo.userName = ?1 and tempOrders" +
                    ".stockProductSize3.id=?2")
    TempOrder findByUserName_Product (String userName, Integer sizeId);

    /**
     * @param id
     */
    @Modifying
    @Transactional
    @Query ("delete from TempOrder temp where temp.stockProductSize3.id=?1 and temp.userinfo.userName=?2")
    void deleteFromTempWebOrder (Integer id, String userName);

    /**
     * @param userName
     */
    @Modifying
    @Transactional
    @Query ("delete from TempOrder temp where temp.userinfo.userName=?1")
    void deleteByUserName (String userName);

    /**
     * @param deviceId
     *
     * @return
     */
    @Query ("select tempOrders from TempOrder tempOrders where tempOrders.deviceId = ?1 order by tempOrders.id desc")
    List<TempOrder> findAllByDeviceId (String deviceId);

    @Modifying
    @Transactional
    @Query ("update TempOrder temp set temp.carrierService=?1 where temp.userinfo.userName=?2")
    void updateCarrierService (CarrierService service, String userName);



    @Query ("select tempOrders from TempOrder tempOrders where tempOrders.userinfo.userName = ?1" +
            " and tempOrders.promotions.id=?2")
    List<TempOrder> findByUserName_Promotion (String userName, Integer promotionId);

    @Query ("select tempOrders from TempOrder tempOrders where tempOrders.userinfo.userName = ?1" +
            " and tempOrders.promotionVisibility.id=?2 and tempOrders.idSubPromotion=?3")
    TempOrder findByUserName_Promotion (String userName, Integer promotionId, Integer idSubPromotion);

    @Modifying
    @Transactional
    @Query ("delete from TempOrder temp where temp.promotions.id=?1 and temp.idSubPromotion=?2" +
            " and temp.userinfo.userName = ?3")
    void deletePromotionFromTempWebOrder (Integer id, Integer subId, String userName);

    @Modifying
    @Transactional
    @Query ("update TempOrder temp set temp.quantity=?1 where temp.id=?2")
    void updateTempWebOrder (Integer quantity, Integer id);


    @Modifying
    @Transactional
    @Query ("update TempOrder temp set temp.productStatus=?1 where temp.id=?2")
    void updateProductStatusTempWebOrder (String status, Integer id);


    @Query("select temp from TempOrder temp where temp.promotions.id=?1 and temp.userinfo.userName = ?2 " +
            " and temp.idSubPromotion=?3")
    TempOrder getTempOrderByPromotionId(Integer id,String userName, Integer subId);


    @Query(value = "select max(id_subpromotion) from temp_order where user_name=?1 and id_promotion=?2",
            nativeQuery = true)
    Integer getMaxSubPromotionID(String userName, Integer idPromotion);

    @Query (value = "select freeze_for_payment from temp_order where user_name = ?1 limit 1", nativeQuery = true)
    Boolean checkFreezeForPayment (String userName);

    @Modifying
    @Transactional(noRollbackFor = RuntimeException.class, propagation = Propagation.REQUIRES_NEW)
    @Query (value = "update TempOrder temp set temp.freezeForPayment=?1 where temp.userinfo.userName=?2")
    void freezeUnfreezeOrder (Boolean isFreeze, String userName);

    @Modifying
    @Transactional
    @Query (value = "update TempOrder temp set temp.freezeForPayment=?1 where temp.createdAt < ?2")
    void unfreezeAllFreezeOrders (Boolean unFreeze, Timestamp oldTime);

    @Query(value = "select stock_product, id_promotion from temp_order where user_name = ?1", nativeQuery = true)
    List<Object[]> getAddedProuctIds(String user);
}

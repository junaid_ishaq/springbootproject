package com.practice.repository;


import com.practice.domain.StaticLabelInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StaticLabelInfoRepository extends JpaRepository<StaticLabelInfo,Integer> {


    /**
     * @param name
     * @param idTranslation
     *
     * @return
     */
    @Query("select label from StaticLabelInfo label where label.staticLabel.property = ?1  " +
            " and label.translation.idTranslation = ?2")
    List<StaticLabelInfo> getStaticLabel (String name, Integer idTranslation);

    /**
     * @param name
     * @param idTranslation
     *
     * @return
     */
    @Query ("select label from StaticLabelInfo label where label.staticLabel.property = ?1 " +
            " and label.translation.idTranslation = ?2")
    StaticLabelInfo getStaticLabelInfo (String name, Integer idTranslation);
}

package com.practice.repository;


import com.practice.domain.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Integer> {

    @Query("select brand from Brand brand where brand.statusBean.id=?1")
    List<Brand> getAllActiveBrands (Integer statusId);

    @Query ("select brand from Brand brand where (brand.brandName in (?1) OR brand.slugName in (?1)) and brand.statusBean.id=?2")
    List<Brand> getBrandsByName (List<String> brandNames,Integer statusId);

    @Query(value = "select id from brand where id_folder is not null limit 1", nativeQuery = true)
    Integer getFirstHavingDocumentCabinet();

    @Query("select brand from Brand brand where brand.id = ?1 and brand.statusBean.id=1")
    Brand getBrandById(Integer brandId);

}

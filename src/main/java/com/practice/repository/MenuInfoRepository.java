package com.practice.repository;


import com.practice.domain.MenuInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuInfoRepository extends JpaRepository<MenuInfo, Integer> {

    /**
     * @param idMenu
     *
     * @return
     */
    @Query("select info from MenuInfo info where info.menu.id=?1 and info.translation.idTranslation=?2 and info.status.id=1")
    MenuInfo getMenuInfo (Integer idMenu, Integer idTranslation);

    /**
     * @param idTranslation
     *
     * @return
     */
    @Query ("select info.email from MenuInfo info where info.translation.idTranslation=?1 and info.status.id=1")
    String getEmail (Integer idTranslation);

}

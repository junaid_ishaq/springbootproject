package com.practice.exceptionHandling;

public class RecordNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -3042686055658047285L;

    private final String exceptionMessage;

    public RecordNotFoundException(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    public String getExceptionMessage(){
        return exceptionMessage;
    }

}
